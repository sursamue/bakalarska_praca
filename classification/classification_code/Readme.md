# Welcome to classification code

## Requirements

The program expects the [files](https://www.kaggle.com/datasets/cdeotte/jpeg-isic2019-512x512) in path 
```bash 
/mnt/datagrid/Medical/skin/data/
```
Install the requirements:
```bash
pip3 install -r requirements.txt
```


## How to run:
The working directory must include 'stats' in the same directory.
### Train a model

```bash
python3 main.py --train True --model [model_name] --lr [learning_rate] --batch_size [batch_size] --cuda [index_of_gpu]
```

list currently supported model_name: efficientnet-b0, efficientnet-b1, efficientnet-b2, efficientnet-b3, efficientnet-b4, efficientnet-b5, efficientnet-b6, efficientnet-b7, resnet50, densenet121

### Test a model
If you want to test model, without experiments, currently,
you need to change the 'thresholds' variable in get_test_augmentations function to [256].
(We will fix this problem in the future.)
#### Test a single model


```bash
python3 main.py --test_aug True --model [model_name] --batch_size [batch_size] --cuda [index_of_gpu]
```
The model_name must be in a form of model_[model_name]

#### Test an ensemble model

```bash
python3 main.py --test_aug True --ensemble [model_name] --batch_size [batch_size] --cuda [index_of_gpu]
```
The model_name must be in a form of model_[model_name],model_[model_name],model_[model_name],...



