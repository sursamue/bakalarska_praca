# Script that combines skin lesion detection and classification
# the user will be able to upload an image and the script will
# detect the lesion and classify it

# import logging and disable it
import logging
# disable logging completely
logging.disable(logging.CRITICAL)
logging.getLogger('yolov7').setLevel(logging.CRITICAL)

# import the necessary packages
import argparse
import cv2
import torch
from PIL import Image, ImageOps
# torch nn
import torch.nn as nn
import torch.nn.functional as F
# dataset
from torch.utils.data import Dataset, DataLoader
# albumentations
import albumentations as A
# tqdm
from tqdm import tqdm

import os
import piq
import sys

import numpy as np
import pandas as pd

# import custom_loader.py from utils folder
# it is used as a nice visualisation so user will know that
# program is running
from utils.custom_loader import Loader as Loading_screen


def chceck_if_file_exists(file_path):
    """ Check if the file exists
    Args:
        file_path (str): path to the file
    Returns:
        bool: whether the file exists or not
    """
    if os.path.isfile(file_path):
        return True
    else:
        return False


def check_if_is_supported_image(image_path):
    """ Check if the image is supported
    Args:
        image_path (str): path to the image
    Returns:
        bool: whether the image is supported or not
    """
    # get the image extension
    image_extension = image_path.split('.')[-1]
    # check if the extension is supported
    if image_extension in ['jpg', 'jpeg', 'JPG', 'JPEG']:
        return True
    else:
        return False


def embed_box_borders(img, x, y, w, h, color, pad):
    """ Embed the box borders
    This is a helper function for drawing the box borders from skin3d 
    by Jeremy Kawahara.
    However, it does not work properly, as it does not draw full rectangle
    if the rectangle is on the edge of the image.
    Args:
        img (np.array): image
        x (int): x coordinate
        y (int): y coordinate
        w (int): width
        h (int): height
        color (tuple): color
        pad (int): padding
    Returns:
        img (np.array): image with embedded borders
    """

    p = pad

    img[y:y + h, x - p:x, :] = color
    img[y:y + h, x + w:x + w + p, :] = color
    img[y - p:y, x - p:x + w + p, :] = color
    img[y + h:y + h + p, x - p:x + w + p, :] = color


class HiddenPrints:
    """ Class that is used to hide the prints
    We use this class, because we want to hide the prints from other libraries
    """
    def __enter__(self):
        self._original_stdout = sys.stdout
        sys.stdout = open(os.devnull, 'w')

    def __exit__(self, exc_type, exc_val, exc_tb):
        sys.stdout.close()
        sys.stdout = self._original_stdout


def get_args():
    """Function that gets the arguments from the user
    Returns:
        args (dict): dictionary of arguments
    """
    ap = argparse.ArgumentParser()
    ap.add_argument("-i", "--image", required=True,
                    help="path to input image")
    # add argument for gt boxes
    ap.add_argument("-gt", "--groundtruth", required=False, default=None, type=str, help="path to groundtruth file")
    # ap.add_argument("-m", "--model", required=True,
    #                 help="path to trained model model")
    # ap.add_argument("-l", "--labelbin", required=True,
    #                 help="path to label binarizer")
    ap.add_argument("-c", "--confidence", type=float, default=0.2,
                    help="minimum probability to filter weak detections")
    args = vars(ap.parse_args()) # vars is used to convert the arguments to a dictionary
    return args

def load_yolo_model():
    """ Load the trained YOLO model
    Returns:
        model (torch model): YOLO model
    """
    # load the model
    model = create_yolov7_model('yolov7', num_classes=1, pretrained=False)
    checkpoint_path = 'models/detection/best_model.pt'
    model.load_state_dict(torch.load(checkpoint_path, map_location="cpu")["model_state_dict"])
    # return the model
    return model

class Ensemble(nn.Module):
    """
    Class Ensemble defines our strategy for combining the models.
    Current strategy is to average the outputs of the models.
    """

    def __init__(self, models):
        super(Ensemble, self).__init__()
        self.models = models
        # model1 = models[0]
        # model2 = models[1]
        # model3 = models[2]
        # self.output = nn.Linear(8*len(models), 8)
            
    def forward(self, inputs):

        x, meta = inputs
        # for model in self.models:
        #     cnn_features = model(inputs)
        #     features.append(cnn_features)
        outputs = []
        # is able to return multiple outputs
        for model in self.models:
            outputs.append(model(inputs))
        # out is a sum of all outputs
        out = sum(outputs)
        out = out/len(outputs)
        return out

def form_ensemble_model(models, device):
    """ Form the ensemble model
    Args:
        models (str): models to read
        device (torch.device): device
    Returns:
        model (torch model): ensemble model
    """

    models_list = []
    for model in read_models(models):
        models_list.append(torch.load(model, map_location=device))
    # create the ensemble model
    model = Ensemble(models_list)
    model.to(device)
    return model



def read_models(models):
    """ Read the models, 
    Args:
        models (str): models to read
    Returns:
        models (list): list of models
    """

    # they are divided ' ' space
    models = models.split(' ')
    # add the .pth extension and model_prefix and model_ prefix
    models = ['models/classification/model_{}.pth'.format(model) for model in models]
    return models

def load_classification_model(args, model_names, is_ensemble=False, device = torch.device('cpu')):
    """ Load the classification model
    Args:
        args (dict): arguments
        model_names (list): name of the model/s
        is_ensemble (bool): whether to load the ensemble model or not
    Returns:
        model (model): classification model
    """

    # load the model
    model_path_list = []
    
    if not is_ensemble:
        for model_name in model_names:
            model_path = 'models/classification/model_{}.pth'.format(model_name)
            model_path_list.append(model_path)
        model = torch.load(model_path_list[0], map_location=device)
    elif is_ensemble:
        model = form_ensemble_model(model_path_list, device)

    return model

def check_image_quality(image):
    """ Check the image quality
    Args:
        image (image): image to check the quality
    Returns:
        brisque_bool (bool): whether the image is good or not in terms of brisque score
        mp_bool (bool): whether the image is good or not in terms of Mega Pixels
    """
    print('checking image quality...')
    brisque_bool = None
    mp_bool = None
    # check how many MP the image has
    # get the image size
    image_size = image.shape
    # calculate the image size in MP
    image_size = image_size[0] * image_size[1] / 1000000
    # print the image size
    #print in same line
    # if the image size is less than 24 MP, then the image is not good
    if image_size < 24:
        mp_bool = False
    else:
        mp_bool = True

    print('image size: {:.0f} MP...{}'.format(image_size, 'ok' if mp_bool else 'not ok'))
    # convert the image to tensor
    image = image_to_tensor(image)[None, ...]
    # calculate the brisque score
    brisque_index = np.nan
    loader = Loading_screen("brisque score: ", '', 0.05).start()
    brisque_index = piq.brisque(image, data_range=1., reduction='none')
    if brisque_index > 60:
        brisque_bool = False
    else:
        brisque_bool = True
    brisque_index = brisque_index.detach().numpy()
    print('\rbrisque score {:.0f}...{}'.format(brisque_index[0], 'ok' if brisque_bool else 'not ok'), flush=True)

    loader.stop()


    return mp_bool, brisque_bool

# load the image func
def load_image(image_path):
    """Check if supported and oad the image and convert it to tensor
    Args:
        image_path (str): path to the image
    Returns:
        image (tensor): image as a tensor
    """
    file_exits = chceck_if_file_exists(image_path)
    if not file_exits:
        raise FileNotFoundError('File {} does not exist'.format(image_path))

    is_supported_file_format = check_if_is_supported_image(image_path)
    if not is_supported_file_format:
        raise ValueError('File format {} is not supported'.format(image_path.split('.')[-1]))
    
    # load the image
    image = cv2.imread(image_path)
    # convert the image to RGB, because cv reads images in BGR format
    image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)

    # image = Image.open(image_path).convert('RGB')
    # image = ImageOps.exif_transpose(image)

    # return the image
    return image

def get_test_transform():
    """ Simple transform that resizes input to 256x256 and normalizes it
    Returns:
        transform (transform): transform
    """
    return A.Compose([
        A.Resize(256, 256),
    A.Normalize()
    ])

def print_brisque_score(image):
    """ Print the brisque score of the image
    Args:
        image (image): image to calculate the brisque score
    """
    # convert the image to tensor
    image = image_to_tensor(image)[None, ...]
    # calculate the brisque score
    brisque_index = piq.brisque(image, data_range=1., reduction='none')
    # print the brisque score
    print('brisque score: {}'.format(brisque_index))


def image_to_tensor(image):
    """ Convert the image to tensor and normalize it to [0, 1]
    [0,1] is essential, as the program is faster
    Args:
        image (image): image to convert
    Returns:
        image (tensor): image as a tensor
    """
    # # convert the image to tensor
    # image = torch.from_numpy(image).permute(2, 0, 1).float()
    # # normalize the image
    # image = image / 255.0
    # # return the image

    image = image.astype(np.float32) / 255.0
    # transpose the image
    image = image.transpose(2, 0, 1)
    # image to tensor
    image = torch.from_numpy(image).float()
    
    return image

def model_make_preds(args, model, image_tensor):
    """ Make predictions using the model
    Args:
        model (model): model to use for predictions
        image_tensor (tensor): image as a tensor
    Returns:
        boxes (list): list of boxes
        scores (list): list of scores
        class_ids (list): list of class ids
    """
    model.eval()
    with torch.no_grad():
        model_outputs = model(image_tensor[None])
        preds = model.postprocess(model_outputs, conf_thres=args['confidence'], multiple_labels_per_box=False)
        nms_predictions = filter_eval_predictions(preds, confidence_threshold=0, nms_threshold=0.1)
        boxes = nms_predictions[0][:, :4]
        scores = nms_predictions[0][:, 4]
        # scores to numpy
        scores = scores.numpy()
        # boxes = boxes.numpy()
        # convert to list
        scores = scores.tolist()
        class_ids = nms_predictions[0][:, -1]
    return boxes, scores, class_ids

def draw_bounding_boxes(image, boxes, scores):
    """ Draw bounding boxes on the image
    Args:
        image (image): image to draw on
        boxes (list): list of boxes
        scores (list): list of scores
    Returns:
        image (image): image with bounding boxes
    """
    
    # enumerate boxes and scores

    for index, (box, score) in enumerate(zip(boxes, scores)):

        color = [0,0,255] # blue
        pad = 10
        x = int(box[0])
        y = int(box[1])
        width = int(box[2]-box[0])
        height = int(box[3]-box[1])

        cv2.rectangle(image, (x, y), (x+width, y+height), color, pad)
        # draw index on the top of the box, use color black
        # put text inside the box top left corner
        cv2.putText(image, str(index), (x+pad, y+pad*3), cv2.FONT_HERSHEY_SIMPLEX, 1, (0,0,0), 2, cv2.LINE_AA)
        # put text inside the box bottom left corner
        cv2.putText(image, str(int(score*100))+'%', (x+pad, y+height-pad), cv2.FONT_HERSHEY_SIMPLEX, 1, (0,0,0), 2, cv2.LINE_AA)
    return image


def boxes_to_df(boxes, scores):
    """ Convert the boxes and scores to dataframe
    Args:
        boxes (list): list of boxes
        scores (list): list of scores
    Returns:
        boxes_df (dataframe): dataframe of boxes and scores
    """
    boxes_df = pd.DataFrame(boxes, columns=['x1', 'y1', 'x2', 'y2'])
    boxes_df['score'] = scores
    return boxes_df

def cutout_square_boxes(image, boxes_df):
    """ Cut out the boxes from the image, but make them square with the same size as the bigger side,
    if the box overflows the image, then make the box as big as possible and fill the rest with black
    Args:
        image (image): image to cut out the boxes from
        boxes_df (dataframe): dataframe of boxes and scores
    Returns:

        boxes (list): list of boxes
    """
    # get the boxes
    boxes = boxes_df[['x1', 'y1', 'x2', 'y2']].values
    # make the boxes square and check if they overflow the image
    for i, box in enumerate(boxes):
        # get the width and height of the box
        width = box[2] - box[0]
        height = box[3] - box[1]
        # get the bigger side
        bigger_side = max(width, height)
        # get the new width and height
        new_width = bigger_side
        new_height = bigger_side
        # get the difference between the new width and height and the old width and height
        width_diff = new_width - width
        height_diff = new_height - height
        # get the new coordinates
        x1 = int(box[0] - width_diff / 2)
        y1 = int(box[1] - height_diff / 2)
        x2 = int(box[2] + width_diff / 2)
        y2 = int(box[3] + height_diff / 2)
        # check if the box overflows the image
        if x1 < 0:
            x1 = 0
        if y1 < 0:
            y1 = 0
        if x2 > image.shape[1]:
            x2 = image.shape[1]
        if y2 > image.shape[0]:
            y2 = image.shape[0]
        # update the boxes
        boxes[i] = [x1, y1, x2, y2]
    # cut out the boxes
    boxes = [image[int(box[1]):int(box[3]), int(box[0]):int(box[2])] for box in boxes]
    # resize all the boxes to 256x256
    boxes = [cv2.resize(box, (256, 256)) for box in boxes]

    
    
    return boxes
    

def cutout_boxes(image, boxes_df):
    """ Cut out the boxes from the image
    Args:
        image (image): image to cut out the boxes from
        boxes_df (dataframe): dataframe of boxes and scores
    Returns:
        boxes (list): list of boxes
    """
    # get the boxes
    boxes = boxes_df[['x1', 'y1', 'x2', 'y2']].values
    # cut out the boxes
    boxes = [image[int(box[1]):int(box[3]), int(box[0]):int(box[2])] for box in boxes]
    return boxes


def save_boxes(boxes):
    """ Save the boxes
    Args:
        boxes (list): list of boxes
    Returns:
        None
    """
    # save boxes from bgr to rgb
    for i, box in enumerate(boxes):

        box = cv2.cvtColor(box, cv2.COLOR_BGR2RGB)
        cv2.imwrite(f"results/box_{i}.jpg", box)


def square_boxes(boxes, image_shape):
    """function that will take a list of boxes and make them square with the size x size pixels, where size = max(width, height)
    if the box overflows the image,it will, still make it square, but the box will be filled with zeros
    Args:
        boxes (list): list of boxes
        image_shape (tuple): shape of the image, (height, width)
    Returns:
        boxes (list): list of boxes
        each box is a list of 4 elements: x1, y1, x2, y2
    """
    # get the height and width of the image
    height, width = image_shape
    # loop over the boxes
    for i, box in enumerate(boxes):
        # get the width and height of the box
        box_width = box[2] - box[0]
        box_height = box[3] - box[1]
        # get the max size
        max_size = max(box_width, box_height)
        # get the difference between the max size and the width and height
        diff_width = max_size - box_width
        diff_height = max_size - box_height
        # get the new x1 and y1
        new_x1 = box[0] - diff_width // 2
        new_y1 = box[1] - diff_height // 2
        # get the new x2 and y2
        new_x2 = box[2] + diff_width // 2
        new_y2 = box[3] + diff_height // 2
        # check if the new x1 and y1 are less than 0
        if new_x1 < 0:
            new_x1 = 0
        if new_y1 < 0:
            new_y1 = 0
        # check if the new x2 and y2 are greater than the width and height
        if new_x2 > width:
            new_x2 = width
        if new_y2 > height:
            new_y2 = height
        # update the boxes
        boxes[i] = [new_x1, new_y1, new_x2, new_y2]
    return boxes

# divide image into 2 parts
# this function is not used, we leave it here for possible future use
def divide_image(image):
    """function that will divide an image into 2 parts,
    if the widht is greater than the height, it will divide it vertically,
    if the height is greater than the width, it will divide it horizontally
    Args:

        image (image): image to divide
    Returns:
        top_im (image): top image
        bottom_im (image): bottom image
    """
    # get the height and width of the image
    height, width = image.shape[:2]
    # check if the width is greater than the height
    if width > height:
        # divide the image vertically
        top_im = image[:, :width // 2]
        bottom_im = image[:, width // 2:]
    else:
        # divide the image horizontally
        top_im = image[:height // 2, :]
        bottom_im = image[height // 2:, :]
    return top_im, bottom_im



def detection(args):
    """function that will detect skin lesions in an image
    Args:
        args (dict): dictionary of arguments
    Returns:
        skin_lesions (list): list of images of skin lesions
        bboxes_df (dataframe): dataframe of bounding boxes and scores
    """
    # load the image
    image = load_image(args["image"])
    # image, bottom_im = divide_image(image)

    iqa_mp, iqa_brisque = check_image_quality(image)
    if iqa_mp == False:
        message = get_MP_message()
        print(message)

    if iqa_brisque == False:
        message = get_brisque_message()
        print(message)

    if iqa_mp == False or iqa_brisque == False:
        print("Exiting program due to bad image quality.")
        # exit the program
        sys.exit(0)
    else:
        print("Image quality is good.")

    original_image_size = image.shape[:2]
    original_image = image.copy()

    
    # save resized image
    # resize image to have max size of 800, save aspect ratio
    original_width = image.shape[1]
    original_height = image.shape[0]
    if original_width > original_height:
    # image = cv2.resize(image, (800, 800))
        image = image_resize(image, width = 1600)
        # pad the image to have size 800x800
        new_dims = image.shape[:2]
        image = cv2.copyMakeBorder(image, 0, 1600 - image.shape[0], 0, 1600 - image.shape[1], cv2.BORDER_CONSTANT, value=[0,0,0])
    else:
        image = image_resize(image, height = 1600)
        # pad the image to have size 800x800
        new_dims = image.shape[:2]
        image = cv2.copyMakeBorder(image, 0, 1600 - image.shape[0], 0, 1600 - image.shape[1], cv2.BORDER_CONSTANT, value=[0,0,0])
    
    # print("new dims: ", new_dims)

    # save resized image
    # resized_image = image.copy()
    # from bgr to rgb
    # resized_image = cv2.cvtColor(resized_image, cv2.COLOR_BGR2RGB)

    # cv2.imwrite("results/resized_image.jpg", resized_image)
    # convert the image to tensor
    
    image_tensor = image_to_tensor(image)
    
    # load the model
    model = load_yolo_model()

    
    # make predictions
    boxes, scores, class_ids = model_make_preds(args, model, image_tensor)
    boxes = scale_bboxes_to_original_image_size(
        boxes,
        new_dims,
        (original_image_size[0], original_image_size[1]),
        is_padded=False,
    )
    boxes = boxes.numpy()

    # draw the bounding boxes
    og_image_copy = original_image.copy()
    og_image_copy = draw_bounding_boxes(og_image_copy, boxes, scores)

    if args["groundtruth"] is not None:
        gt_boxes = pd.read_csv(args["groundtruth"])
        og_image_copy = draw_gt_boxes(og_image_copy, gt_boxes, args["image"])
    # og image convert to rgb
    og_image_copy = cv2.cvtColor(og_image_copy, cv2.COLOR_BGR2RGB)
    cv2.imwrite("tmp/detection_res.jpg", og_image_copy)
    # boxes = square_boxes(boxes, original_image.shape[:2]) 


    # convert the boxes and scores to dataframe
    boxes_df = boxes_to_df(boxes, scores) # boxes_df currently has the boxes yielded by the model, they are resized to fit the original image size, but they do not include squared_boxes

    # cut out the boxes
    # skin_lesion_images = cutout_square_boxes(original_image, boxes_df) 
    skin_lesion_images = cutout_boxes(original_image, boxes_df)
    # iterate over the boxes and print shape 


    # save the boxes
    save_boxes(skin_lesion_images)

    return skin_lesion_images, boxes_df, og_image_copy


def image_resize(image, width = None, height = None, inter = cv2.INTER_AREA):
    # initialize the dimensions of the image to be resized and
    # grab the image size
    dim = None
    (h, w) = image.shape[:2]

    # if both the width and height are None, then return the
    # original image
    if width is None and height is None:
        return image

    # check to see if the width is None
    if width is None:
        # calculate the ratio of the height and construct the
        # dimensions
        r = height / float(h)
        dim = (int(w * r), height)

    # otherwise, the height is None
    else:
        # calculate the ratio of the width and construct the
        # dimensions
        r = width / float(w)
        dim = (width, int(h * r))

    # resize the image
    resized = cv2.resize(image, dim, interpolation = inter)
    # return the resized image
    return resized





def draw_gt_boxes(image, gt_df, image_name):
    """function that will draw the ground truth boxes on the image
    Args:
        image (np.array): image
        gt_df (dataframe): dataframe with ground truth boxes
        image_name (str): name of the image
    Returns:
        image (np.array): image with ground truth boxes
    """
    # get image_name.jpg from the image_name which is path to the image
    image_name = image_name.split("/")[-1]
    #filter out everything except the image_name
    gt_df = gt_df[gt_df["image_name"] == image_name]
    # convert boxes to list
    boxes = gt_df[["bbox_x", "bbox_y", "bbox_width", "bbox_height"]].values.tolist()

    # get the boxes
    for index, box in enumerate(boxes):

        color = [0,255,0]
        pad = 10
        x = int(box[0])
        y = int(box[1])
        width = int(box[2])
        height = int(box[3])
        embed_box_borders(image, x, y, width, height, color, pad)
    return image



class MelanomaDataset(Dataset):
    # the code of this class is based on https://www.kaggle.com/code/nroman/melanoma-pytorch-starter-efficientnet
    def __init__(self, df: pd.DataFrame, imfolder: str, train: bool = True, transforms = None, meta_features = None):
        """
        Class initialization
        Args:
            df (pd.DataFrame): DataFrame with data description
            imfolder (str): folder with images
            train (bool): flag of whether a training dataset is being initialized or testing one
            transforms: image transformation method to be applied
            meta_features (list): list of features with meta information, such as sex and age
            
        """
        self.df = df
        self.imfolder = imfolder
        self.transforms = transforms
        self.train = train
        self.meta_features = meta_features
        
    def __getitem__(self, index):
        im_path = os.path.join(self.imfolder, self.df.iloc[index]['image_name'] + '.jpg')
        x = cv2.imread(im_path)
        x = cv2.cvtColor(x, cv2.COLOR_BGR2RGB)
        
        if self.meta_features is not None:
            meta = np.array(self.df.iloc[index][self.meta_features].values, dtype=np.float32)
        else:
            meta = np.zeros(0)

        if self.transforms:

            
            x = self.transforms(image = x)
            x = x['image'].astype(np.float32)
            x = x.transpose(2, 0, 1)
            # x to float tensor
            x = torch.from_numpy(x)

                    
            
        if self.train:
            y = self.df.iloc[index]['target']
            return (x, meta), y
        else:
            return (x, meta)
    
    def __len__(self):
        return len(self.df)

def get_class_names_convertor():
    # nv, mel, bcc, bkl, ak, scc, df , vasc
    return {'NV': 0, 'MEL': 1, 'BCC': 2, 'BKL': 3, 'AK': 4, 'SCC': 5, 'DF': 6, 'VASC': 7}

class Net_without_meta(nn.Module):
    def __init__(self, arch):
        super(Net_without_meta, self).__init__()
        self.arch = arch
        # if 'ResNet' in str(arch.__class__):
        #     self.arch.fc = nn.Linear(in_features=512, out_features=500, bias=True)

        if args.model == 'efficientnet-b0':
            self.arch._fc = nn.Linear(in_features=1280, out_features=500, bias=True)
        elif args.model == 'efficientnet-b1':
            self.arch._fc = nn.Linear(in_features=1280, out_features=500, bias=True)
        if args.model == 'efficientnet-b2':
            self.arch._fc = nn.Linear(in_features=1408, out_features=500, bias=True)
        elif args.model == 'efficientnet-b3':
            self.arch._fc = nn.Linear(in_features=1536, out_features=500, bias=True)
        elif args.model == 'efficientnet-b4':
            self.arch._fc = nn.Linear(in_features=1792, out_features=500, bias=True)
        elif args.model == 'efficientnet-b5':
            self.arch._fc = nn.Linear(in_features=2048, out_features=500, bias=True)
        elif args.model == 'efficientnet-b6':
            self.arch._fc = nn.Linear(in_features=2304, out_features=500, bias=True)
        elif args.model == 'efficientnet-b7':
            self.arch._fc = nn.Linear(in_features=2560, out_features=500, bias=True)
        elif args.model =='resnet50':
            self.arch.fc = nn.Linear(in_features=2048, out_features=500, bias=True)
        elif args.model =='densenet121':
            self.arch.classifier = nn.Linear(in_features=1024, out_features=500, bias=True)
        self.ouput = nn.Linear(500, 8)
        


    def forward(self, inputs):

        x, meta = inputs


        cnn_features = self.arch(x)

        output = self.ouput(cnn_features)
        return output


def draw_classification_results(image, boxes_df, preds_df, pad=10):
    """Function that will draw the classification results such as label name and probability
    Args:
        image (np.array): image to draw the results on
        boxes_df (pd.DataFrame): dataframe of boxes, format:x1,y1,x2,y2,scores
        pad (int): padding for the boxes
    Returns:
        image (np.array): image with drawn results
    """
    # iterate over boxes_df and preds_df
    for index, row in boxes_df.iterrows():
        # get the label and probability
        label = preds_df.loc[index, 'preds']
        prob = preds_df.loc[index, 'probs']
        # get the box coordinates
        x, y, width, height = row['x1'], row['y1'], row['x2'] - row['x1'], row['y2'] - row['y1']

        
        # draw the label and probability
        if label == 'Unknown':
            cv2.putText(image, f'{label}', (int(x), int(y - pad)), cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 0, 0), 2)
        else:
            cv2.putText(image, f'{label} {prob*100:.0f}'+'%', (int(x), int(y - pad)), cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 0, 0), 2)
    # save the image
    cv2.imwrite('results/results.jpg', image)


def get_MP_message():
    return ('Sorry, the image quality is not good enough for the model to make predictions\nPlease take a picture with at least 24MP camera.')

def get_brisque_message():
    return ('Sorry, the image quality is not good enough for the model to make predictions\nPlease make sure to take your picture in well lit environment, with no blur or noisy background.')


def classification(args, image, skin_lesion_images, boxes_df, device = torch.device('cpu')):
    """Function that will classify the skin lesions
    Args:
        args (dict): dictionary of arguments
        skin_lesion_images (list): list of images of skin lesions
        boxes_df (pd.DataFrame): dataframe of boxes (currently not resized, original yielded by detection model)
        device (torch.device): device to run the model on
    Returns:
        skin_lesion_df (pd.DataFrame): dataframe of skin lesions with classification results
    """
    # load the model
    model_names = ['efficientnet-b7'] # default model, may be changed in the future, in plan to add possibility to choose the model from the command line
    model = load_classification_model(args, model_names)
    # create dataframe for the skin lesions
    skin_lesion_df = pd.DataFrame()
    transform = get_test_transform()
    # save skin lesion images' names to the dataframe as a box_'id', based on the position in the list
    skin_lesion_df['image_name'] = ['box_' + str(i) for i in range(len(skin_lesion_images))]
    dataset = MelanomaDataset(skin_lesion_df, 'results/', train = False, transforms = transform)
    test_loader = DataLoader(dataset, batch_size = 1, shuffle = False, num_workers = 2)
    detection_scores = boxes_df.score.values.tolist()
    skin_lesion_df['detection_scores'] = detection_scores
    # skin_lesion_tensors = []
    # meta = np.zeros(0)
    # # iterate over the skin lesion images
    # for image in skin_lesion_images:
    #     # convert the image to tensor
    #     image = transform(image = image)
    #     image = image['image'].astype(np.float32)
    #     image = image.transpose(2, 0, 1)
    #     # x to float tensor
    #     image = torch.from_numpy(image)
    #     # append the tensor to the list
    #     skin_lesion_tensors.append(image)

    # get predictions for each image in skin_lesion_images
    model.eval()
    # predictions = torch.zeros((len(skin_lesion_tensors), 8), dtype=torch.float32, device=device)
    # with torch.no_grad():
    #     for j, image in enumerate(skin_lesion_tensors):
    #         # put image in format that the model expects
    #         # get the prediction
    #         x = [image, meta]
            

    #         x[0] = x[0].to(device)
    #         # x[1] = x[1].to(device)
    #         z = model(x)
    #         # append the prediction to the list
    #         pred = (F.softmax(z, dim=1)) 
    #         predictions[j:j+ x_test[0].shape[0]] = test_pred

    test_preds = torch.zeros((len(skin_lesion_df), 8), dtype=torch.float32, device=device)
    test_preds_non_softmax = torch.zeros((len(skin_lesion_df), 8), dtype=torch.float32, device=device)
    with torch.no_grad():  # Do not calculate gradient since we are only predicting
        # Predicting on validation set
        for j, x_test in tqdm(enumerate(test_loader), total=len(test_loader)):
            x_test[0] = x_test[0].to(device)
            x_test[1] = x_test[1].to(device)
            # print out shape of the x_test
            z_test = model(x_test)
            # this is used for probabilities
            test_pred = (F.softmax(z_test, dim=1))
            test_preds[j*test_loader.batch_size:j*test_loader.batch_size + x_test[0].shape[0]] = test_pred 
            test_preds_non_softmax[j*test_loader.batch_size:j*test_loader.batch_size + x_test[0].shape[0]] = z_test
    

    dict_lesion_types = {'NV': 0, 'MEL': 1, 'BCC': 2, 'BKL': 3, 'AK': 4, 'SCC': 5, 'DF': 6, 'VASC': 7}
    # flip the dictionary
    dict_lesion_types = {v: k for k, v in dict_lesion_types.items()}
    # convert numpy array of predictions to dataframe
    for i in range(8):
        skin_lesion_df['' + str(i)] = test_preds[:, i].numpy()
    # convert column names of 0, 1, 2, 3, 4, 5, 6, 7 to NV, MEL, BCC, BKL, AK, SCC, DF, VASC (in this order)
    skin_lesion_df.columns = ['image_name', 'detection_scores', 'NV', 'MEL', 'BCC', 'BKL', 'AK', 'SCC', 'DF', 'VASC']

    # get max number from test_preds and if it is < 0.5, then set the prediction to 8
    skin_lesion_df['probs'] = test_preds.max(1)[0].cpu().numpy()
    skin_lesion_df['unknown'] = skin_lesion_df['probs'] < 0.5
    
    skin_lesion_df['preds'] = (torch.max(test_preds_non_softmax.data, 1)[1].cpu()).numpy()
    # convert the predictions to the names of the skin lesion types
    skin_lesion_df['preds'] = skin_lesion_df['preds'].map(dict_lesion_types)
    # but if unknown is True, then set the prediction to 8
    skin_lesion_df.loc[skin_lesion_df['unknown'] == True, 'preds'] = 'Unknown'
    
    draw_classification_results(image, boxes_df, skin_lesion_df)

    # print(test_preds)
    print(skin_lesion_df)
    return skin_lesion_df
    


def draw_classification_class_to_boxes(df):
    # read the image according to the image_name +'jpg'
    # draw the class and probability on the image
    # save the image

    for index, row in df.iterrows():
        # get the label and probability
        label = row['preds']
        prob = row['probs']
        # get the box coordinates
        # read image from results foder
        image = cv2.imread('results/' + row['image_name'] + '.jpg')

        # draw the label and probability to left bottom corner
        if label == 'Unknown':
            cv2.putText(image, f'{label}', (10, image.shape[0] - 10), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (255, 0, 0), 2) # the number for size is
        else:
            cv2.putText(image, f'{label} {prob*100:.0f}'+'%', (10, image.shape[0] - 10), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (255, 0, 0), 2)
        # save the image
        cv2.imwrite('results/' + row['image_name'] + '.jpg', image)
    return None


def check_result_folder():
    if not os.path.exists('results'):
        os.makedirs('results')
    # check if the results folder is empty, if not, then delete all the files in the folder but ask the user first
    if len(os.listdir('results')) != 0:
        # ask the user if he wants to delete the files in the results folder
        answer = input('The results folder is not empty. Do you want to delete the files in the results folder? (y/n) ')
        if answer == 'y':
            # delete all the files in the results folder
            for file in os.listdir('results'):
                os.remove('results/' + file)
        else:
            print('Please delete the files in the results folder and run the program again.')
            # exit the program without error mesaage
            sys.exit(0)


    return None


if __name__ == '__main__':
    # get the arguments
    args = get_args()
    
    with HiddenPrints():
        from yolov7 import create_yolov7_model
        from yolov7.trainer import filter_eval_predictions
        from yolov7.models.yolo import scale_bboxes_to_original_image_size
    check_result_folder()
    # detect the skin lesions
    skin_lesion_images, boxes_df, image = detection(args)
    # # classify the skin lesions
    output_df = classification(args, image, skin_lesion_images, boxes_df)

    # # save the dataframe to a csv file
    output_df.to_csv('results/output.csv', index=False)

    # # draw the classification results on the images
    draw_classification_class_to_boxes(output_df)


    # we are considering adding the IQA for every skin lesion image in future as
    # not all detected lesions are of a good quality
    # however we need to find the threshold empirically
    # therefore we are not using it for now

    # # image quality assessment using briaque
    # for image in skin_lesion_images:
    #     # get the image quality score
    #     image = image.astype(np.float32) 
    #     image = image.transpose(2, 0, 1)[None, ...] / 255.0 # [None, ...] is used to add a batch dimension
    #     # image to tensor
    #     image = torch.from_numpy(image).float()
    #     # get the score
    #     brisque_index = piq.brisque(image, data_range=1., reduction='none')
    #     # save the score to the dataframe
    #     print(brisque_index)




    
    








    








    