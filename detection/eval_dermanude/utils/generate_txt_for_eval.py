# generate imports
import pandas as pd
import sys
import os
import argparse



# check if output folder has subfolders named 'groundtruths' and 'detections'
# if not, create them
def get_gt_det_folder(output_folder):
    if not os.path.exists(output_folder + 'groundtruths/'):
        os.makedirs(output_folder + 'groundtruths/')
    if not os.path.exists(output_folder + 'detections/'):
        os.makedirs(output_folder + 'detections/')
    # return gt folder and det folder
    return output_folder + 'groundtruths/', output_folder + 'detections/'

# chceck if output folders empty
def check_if_output_folder_empty(gt_folder, det_folder):
    is_empty_gt_folder = True
    is_empty_det_folder = True
    if len(os.listdir(gt_folder)) != 0:
        print('The ground truth folder is not empty.')
        is_empty_gt_folder = False
    if len(os.listdir(det_folder)) != 0:
        print('The detection folder is not empty.')
        is_empty_det_folder = False
    return is_empty_gt_folder, is_empty_det_folder
    
# function to delete all files in the otuput folders but ask the user first
def delete_all_files_in_output_folder(gt_folder, det_folder):
    # ask user if he wants to delete all files in the output folder

    answer = input('Do you want to delete all files in the output folder? (y/n)')
    if answer == 'y':
        # delete all files in the output folder
        for file in os.listdir(gt_folder):
            os.remove(gt_folder + file)
        for file in os.listdir(det_folder):
            os.remove(det_folder + file)
        print('All files have been deleted from the output folder.')
    else:
        print('No files have been deleted from the output folder.')
        print('Exiting the program.')
        sys.exit()

    print('The output folders are now empty.')


def image_id_to_txt_file_name(image_id):
    return str(int(image_id)) + '.txt'

# function to convert ground truth csv file to txt files 
def convert_gt_csv_to_txt(gt_csv, gt_folder, gt_format):
    """ The structure of the ground truth csv file is:
    subject_id,sex,age,image_name,camera,image_id,image_width,image_height,label_name,bbox_x,bbox_y,bbox_width,bbox_height
    Convert csv to txt files. Each image_id has its own txt file.
    The structure of txt files is following:
    label_name bbox_x bbox_y bbox_width bbox_height
    save txt files as image_id.txt in the groundtruths folder
    label_name is always 0, so write skin_lesion
    """
    # read csv file
    df = pd.read_csv(gt_csv)
    # if gt_format is 'xyxy', convert bbox coordinates to 'xywh'
    if gt_format == 'xyxy':
        df = convert_df_xyxy_xywh(df)
    # get unique image ids
    image_ids = df['image_id'].unique()
    # iterate over image ids
    for image_id in image_ids:
        # get rows with image_id
        df_image_id = df[df['image_id'] == image_id]

        # get bbox coordinates
        bbox_x = df_image_id['bbox_x'].values
        bbox_y = df_image_id['bbox_y'].values
        bbox_width = df_image_id['bbox_width'].values
        bbox_height = df_image_id['bbox_height'].values
        # create txt file name
        txt_file_name = image_id_to_txt_file_name(image_id)
        # create txt file
        with open(gt_folder + txt_file_name, 'w') as f:
            # iterate over rows
            for i in range(len(df_image_id)):
                # write label name, bbox coordinates to the txt file
                f.write('skin_lesion' + ' ' + str(bbox_x[i]) + ' ' + str(bbox_y[i]) + ' ' + str(bbox_width[i]) + ' ' + str(bbox_height[i]) + '\n')

# function to convert prediction csv file to txt files
def convert_pred_csv_to_txt(pred_csv, det_folder, pred_format):
    """ The structure of the prediction csv file is:
    image_id,image_width,image_height,bbox_x,bbox_y,bbox_width,bbox_height,score
    Convert csv to txt files. Each image_id has its own txt file.
    The structure of txt files is following:
    label_name bbox_x bbox_y bbox_width bbox_height score
    save txt files as image_id.txt in the detections folder
    label_name is always 0, so write skin_lesion
    """
    # read csv file
    df = pd.read_csv(pred_csv)
    # if gt_format is xyxy, convert bbox coordinates to xywh
    if pred_format == 'xyxy':
        df = convert_df_xyxy_xywh(df)
    # get unique image ids
    image_ids = df['image_id'].unique()
    # iterate over image ids
    for image_id in image_ids:
        # get rows with image_id
        df_image_id = df[df['image_id'] == image_id]

        # get bbox coordinates
        bbox_x = df_image_id['bbox_x'].values
        bbox_y = df_image_id['bbox_y'].values
        bbox_width = df_image_id['bbox_width'].values
        bbox_height = df_image_id['bbox_height'].values
        # get scores
        scores = df_image_id['score'].values
        # create txt file name
        txt_file_name = image_id_to_txt_file_name(image_id)
        # create txt file
        with open(det_folder + txt_file_name, 'w') as f:
            # iterate over rows
            for i in range(len(df_image_id)):
                # write label name, bbox coordinates to the txt file
                f.write('skin_lesion'+ ' ' + str(scores[i]) + ' ' + str(bbox_x[i]) + ' ' + str(bbox_y[i]) + ' ' + str(bbox_width[i]) + ' ' + str(bbox_height[i]) + ' ' + '\n')


def main(gt_csv, pred_csv, output_folder, gt_format, pred_format):
    """ Main function.
    """
    # create output folder if it does not exist
    if not os.path.exists(output_folder):
        os.makedirs(output_folder)
    # create groundtruths folder if it does not exist
    gt_folder = output_folder + '/groundtruths/'
    if not os.path.exists(gt_folder):
        os.makedirs(gt_folder)
    # create detections folder if it does not exist
    det_folder = output_folder + '/detections/'
    if not os.path.exists(det_folder):
        os.makedirs(det_folder)

    # check if the output folders are empty
    is_empty_gt_folder, is_empty_det_folder = check_if_output_folder_empty(gt_folder, det_folder)
    # if the output folders are not empty, ask the user if he wants to delete all files in the output folder
    if not is_empty_gt_folder or not is_empty_det_folder:
        delete_all_files_in_output_folder(gt_folder, det_folder)

    # convert ground truth csv file to txt files
    convert_gt_csv_to_txt(gt_csv, gt_folder, gt_format)
    # convert prediction csv file to txt files
    convert_pred_csv_to_txt(pred_csv, det_folder, pred_format)

    print('Done converting csv files to txt files.')
    print('The txt files are saved in the groundtruths and detections folders.')
    

def convert_df_xyxy_xywh(df):
    """ Convert bbox coordinates from xyxy to xywh format.
    The structure of the csv file is:
    image_id,image_width,image_height,bbox_x,bbox_y,bbox_width,bbox_height,score
    or 
    image_id, image_width, image_height, bbox_xmin, bbox_ymin, bbox_xmax, bbox_ymax
    in case of ground truth file
    """
    # get bbox coordinates
    bbox_xmin = df['xmin'].values
    bbox_ymin = df['ymin'].values
    bbox_xmax = df['xmax'].values
    bbox_ymax = df['ymax'].values
    # convert bbox coordinates from xyxy to xywh format
    bbox_x = bbox_xmin
    bbox_y = bbox_ymin
    bbox_width = bbox_xmax - bbox_xmin
    bbox_height = bbox_ymax - bbox_ymin
    # add bbox coordinates to the dataframe
    df['bbox_x'] = bbox_x
    df['bbox_y'] = bbox_y
    df['bbox_width'] = bbox_width
    df['bbox_height'] = bbox_height
    # return dataframe
    return df

# make the file to be callable from the command line
if __name__ == '__main__':
    # crate argparser with arguments as -gt, -pred, -out -h for help and optional -int for converting bbox coordinates to integers
    parser = argparse.ArgumentParser(description='Convert ground truth csv file and prediction csv file to txt files which can be used for evaluation.')
    parser.add_argument('-gt', '--ground_truth', help='The ground truth csv file.', required=True)
    parser.add_argument('-pred', '--prediction', help='The prediction csv file.', required=True)
    parser.add_argument('-out', '--output', help='The output path where the txt files will be saved.', required=True)
    parser.add_argument('-fgt', '--format_gt', help='The input format of the ground truth csv file. The default is xywh.', default='xywh')
    parser.add_argument('-fpred', '--format_pred', help='The input format of the prediction csv file. The default is xywh.', default='xywh')
    # parser.add_argument('-int', '--int', help='Convert bbox coordinates to integers.', action='store_true')
    # parse arguments
    args = parser.parse_args()

    # run main function but int is optional
    main(args.ground_truth, args.prediction, args.output, args.format_gt, args.format_pred)

#  python3 generate_txt_for_eval.py -gt /mnt/datagrid/Medical/skin/DermaNude/my_images/camera/annotations/main.csv -pred tmp/rescaled_pred_bboxes.csv -out temp_results/