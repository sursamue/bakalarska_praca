import numpy as np
import pandas as pd 
import os

import datetime
import torch
import torchvision
from efficientnet_pytorch import EfficientNet
from sklearn.model_selection import StratifiedKFold, GroupKFold, KFold


from sklearn.metrics import accuracy_score, roc_auc_score, balanced_accuracy_score
from sklearn.metrics import confusion_matrix
from sklearn.metrics import classification_report
from sklearn.metrics import roc_curve
from sklearn.metrics import precision_recall_curve
from sklearn.metrics import average_precision_score
from sklearn.metrics import f1_score
from sklearn.metrics import precision_score
from sklearn.metrics import recall_score
# from sklearn specificity_score



from torch.utils.data import DataLoader#,WeightedRandomSampler
from torchvision import transforms 
import torchvision.models as M

from torch.utils.data import Dataset
from torch import nn
import albumentations as A
from albumentations.pytorch import ToTensorV2
import argparse

from tqdm import tqdm
import time
import cv2
import torch.nn.functional as F

# plt
import matplotlib.pyplot as plt
import seaborn as sns




def save_image(image, path):
    image = image.cpu().numpy()
    image = np.transpose(image, (1, 2, 0))
    image = image * np.array([0.229, 0.224, 0.225]) + np.array([0.485, 0.456, 0.406])
    image = np.clip(image, 0, 1)
    cv2.imwrite(path, (image * 255).astype(np.uint8))





def get_train_transform():

    return A.Compose([
        A.Transpose(p=0.5),
    A.VerticalFlip(p=0.5),
    A.HorizontalFlip(p=0.5),
    A.RandomBrightness(limit=0.2, p=0.75),
    A.RandomContrast(limit=0.2, p=0.75),
    A.OneOf([
        A.MotionBlur(blur_limit=5),
        A.MedianBlur(blur_limit=5),
        A.GaussianBlur(blur_limit=5),
        A.GaussNoise(var_limit=(5.0, 30.0)),
    ], p=0.7),

    A.OneOf([
        A.OpticalDistortion(distort_limit=1.0),
        A.GridDistortion(num_steps=5, distort_limit=1.),
        A.ElasticTransform(alpha=3),
    ], p=0.7),

    A.CLAHE(clip_limit=4.0, p=0.7),
    A.HueSaturationValue(hue_shift_limit=10, sat_shift_limit=20, val_shift_limit=10, p=0.5),
    A.ShiftScaleRotate(shift_limit=0.1, scale_limit=0.1, rotate_limit=15, border_mode=0, p=0.85),
    A.Resize(256, 256),
    A.Cutout(max_h_size=int(256 * 0.375), max_w_size=int(256 * 0.375), num_holes=1, p=0.7), #256 is image size
    
    A.Normalize()
    ])


def get_test_transform():
    return A.Compose([
        A.Resize(256, 256),
    A.Normalize()
])

class MelanomaDataset(Dataset):
    def __init__(self, df: pd.DataFrame, imfolder: str, train: bool = True, transforms = None, meta_features = None):
        """`
        Class initialization
        the code of this class is based on https://www.kaggle.com/code/nroman/melanoma-pytorch-starter-efficientnet
        It is ready to for metadata to be added to the model, however it is not used in this project.
        Args:
            df (pd.DataFrame): DataFrame with data description
            imfolder (str): folder with images
            train (bool): flag of whether a training dataset is being initialized or testing one
            transforms: image transformation method to be applied
            meta_features (list): list of features with meta information, such as sex and age   
        """
        self.df = df
        self.imfolder = imfolder
        self.transforms = transforms
        self.train = train
        self.meta_features = meta_features
        
    def __getitem__(self, index):
        im_path = os.path.join(self.imfolder, self.df.iloc[index]['image_name'] + '.jpg')
        x = cv2.imread(im_path)
        x = cv2.cvtColor(x, cv2.COLOR_BGR2RGB)
        
        if self.meta_features is not None:
            meta = np.array(self.df.iloc[index][self.meta_features].values, dtype=np.float32)
        else:
            meta = np.zeros(0)

        if self.transforms:

            
            x = self.transforms(image = x)
            x = x['image'].astype(np.float32)
            x = x.transpose(2, 0, 1)
            # x to float tensor
            x = torch.from_numpy(x)

                    
            
        if self.train:
            y = self.df.iloc[index]['target']
            return (x, meta), y
        else:
            return (x, meta), -1
    
    def __len__(self):
        return len(self.df)

class Net(nn.Module):
    def __init__(self, arch, n_meta_features: int):
        super(Net, self).__init__()
        self.arch = arch
        if args.model == 'efficientnet-b0':
            self.arch._fc = nn.Linear(in_features=1280, out_features=500, bias=True)
        elif args.model == 'efficientnet-b1':
            self.arch._fc = nn.Linear(in_features=1280, out_features=500, bias=True)
        if args.model == 'efficientnet-b2':
            self.arch._fc = nn.Linear(in_features=1408, out_features=500, bias=True)
        elif args.model == 'efficientnet-b3':
            self.arch._fc = nn.Linear(in_features=1536, out_features=500, bias=True)
        elif args.model == 'efficientnet-b4':
            self.arch._fc = nn.Linear(in_features=1792, out_features=500, bias=True)
        elif args.model == 'efficientnet-b5':
            self.arch._fc = nn.Linear(in_features=2048, out_features=500, bias=True)
        elif args.model == 'efficientnet-b6':
            self.arch._fc = nn.Linear(in_features=2304, out_features=500, bias=True)
        elif args.model == 'efficientnet-b7':
            self.arch._fc = nn.Linear(in_features=2560, out_features=500, bias=True)
        elif args.model =='resnet50':
            self.arch.fc = nn.Linear(in_features=2048, out_features=500, bias=True)
        elif args.model =='densenet121':
            self.arch.classifier = nn.Linear(in_features=1024, out_features=500, bias=True)

        self.meta = nn.Sequential(nn.Linear(n_meta_features, 500),
                                  nn.BatchNorm1d(500),
                                  nn.ReLU(),
                                  nn.Dropout(p=0.2),
                                  nn.Linear(500, 250),  # FC layer output will have 250 features
                                  nn.BatchNorm1d(250),
                                  nn.ReLU(),
                                  nn.Dropout(p=0.2))
        self.ouput = nn.Linear(500 + 250, 8)
        


    def forward(self, inputs):

        x, meta = inputs


        cnn_features = self.arch(x)

        meta_features = self.meta(meta)

        features = torch.cat((cnn_features, meta_features), dim=1)
        output = self.ouput(features)
        return output

class Net_without_meta(nn.Module):
    def __init__(self, arch):
        super(Net_without_meta, self).__init__()
        self.arch = arch

        if args.model == 'efficientnet-b0':
            self.arch._fc = nn.Linear(in_features=1280, out_features=500, bias=True)
        elif args.model == 'efficientnet-b1':
            self.arch._fc = nn.Linear(in_features=1280, out_features=500, bias=True)
        if args.model == 'efficientnet-b2':
            self.arch._fc = nn.Linear(in_features=1408, out_features=500, bias=True)
        elif args.model == 'efficientnet-b3':
            self.arch._fc = nn.Linear(in_features=1536, out_features=500, bias=True)
        elif args.model == 'efficientnet-b4':
            self.arch._fc = nn.Linear(in_features=1792, out_features=500, bias=True)
        elif args.model == 'efficientnet-b5':
            self.arch._fc = nn.Linear(in_features=2048, out_features=500, bias=True)
        elif args.model == 'efficientnet-b6':
            self.arch._fc = nn.Linear(in_features=2304, out_features=500, bias=True)
        elif args.model == 'efficientnet-b7':
            self.arch._fc = nn.Linear(in_features=2560, out_features=500, bias=True)
        elif args.model =='resnet50':
            self.arch.fc = nn.Linear(in_features=2048, out_features=500, bias=True)
        elif args.model =='densenet121':
            self.arch.classifier = nn.Linear(in_features=1024, out_features=500, bias=True)
        self.ouput = nn.Linear(500, 8)
        


    def forward(self, inputs):

        x, meta = inputs


        cnn_features = self.arch(x)

        output = self.ouput(cnn_features)
        return output


# create ensemble model class
class Ensemble(nn.Module):
    """Class for averaging multiple models
    aka Ensemble"""
    def __init__(self, models):
        super(Ensemble, self).__init__()
        self.models = models
        # model1 = models[0]
        # model2 = models[1]
        # model3 = models[2]
        # self.output = nn.Linear(8*len(models), 8)
            
    def forward(self, inputs):

        x, meta = inputs
        # for model in self.models:
        #     cnn_features = model(inputs)
        #     features.append(cnn_features)
        outputs = []
        # is able to return multiple outputs
        for model in self.models:
            outputs.append(model(inputs))
        # out is a sum of all outputs
        out = sum(outputs)
        out = out/len(outputs)
        return out




def get_sampler(df):
    # create weights for the sampler because 8 classes are imbalanced
    # we will use these weights to create a weighted sampler
    # this will help the model to learn better

    # get the counts of each class
    counts = df['target'].value_counts().sort_index().values

    # get the weights for each class
    weights = 1.0 / torch.tensor(counts, dtype=torch.float)
    # get the weight for each sample
    samples_weights = weights[df['target'].values]
    # create a sampler

    
    return samples_weights
    
class metricsLogger():
    """ 
    This class is used to log the metrics during training and validation
    such as train loss, train accuracy, valid loss, valid accuracy, balanced val accuracy, etc
    """
    def __init__(self, args):
        self.args = args
        self.train_loss = []
        self.train_acc = []
        self.valid_loss = []
        self.valid_acc = [] 
        self.lr = []
        self.valid_balanced_acc = []
        self.epoch = []

    def log_train(self, loss, acc):
        """ 
        This function is used to log the train loss and train accuracy
        """
        self.train_loss.append(loss)
        self.train_acc.append(acc)

    def log_valid(self, loss, acc, balanced_acc):
        """
        This function is used to log the valid loss, valid accuracy and balanced valid accuracy
        """        
        self.valid_loss.append(loss)
        self.valid_acc.append(acc)
        self.valid_balanced_acc.append(balanced_acc)

    def log_lr(self, lr):
        """
        This function is used to log the learning rate
        """
        self.lr.append(lr)
    
    def log_epoch(self, epoch):
        """
        This function is used to log the epoch
        """
        self.epoch.append(epoch)

    def update(self, epoch, train_loss, train_acc, valid_loss, valid_acc, valid_balanced_acc, lr):
        """
        This function is used to update all the metrics
        - epoch, train loss, train acc, valid loss, valid acc, valid balanced acc, lr
        """
        self.log_epoch(epoch)
        self.log_train(train_loss, train_acc)
        self.log_valid(valid_loss, valid_acc, valid_balanced_acc)
        self.log_lr(lr)
    
    

    def save(self):
        """
        This function is used to save the metrics in a csv file
        the csv file will be saved in the current directory
        with the name in format <model_name>_metrics.csv
        """
        df = pd.DataFrame({ 'epoch': self.epoch,
                            'train_loss': self.train_loss,
                           'train_acc': self.train_acc,
                           'valid_loss': self.valid_loss,
                           'valid_acc': self.valid_acc,
                           'valid_balanced_acc': self.valid_balanced_acc,
                           'lr': self.lr
                           })
        df.to_csv(f'{self.args.model}_metrics.csv', index=False)


        


# func to train single model add args to the function, train_loader, valid_loader, 
def train_model(args, train_loader, val_loader, test_loader, train_df, valid_df, test_df, device):
    """ 
    Function to train single model and args to the function, train_loader, valid_loader
    The user shall specify the model in Command Line Arguments.
    ARGS:
    - args: arguments passed to the function
    - train_loader: train dataloader
    - valid_loader: valid dataloader
    - test_loader: test dataloader
    - train_df: train dataframe
    - valid_df: valid dataframe
    - test_df: test dataframe
    - device: device to train the model on
    Returns:
    - model: trained model
    """    
    if args.model == 'efficientnet-b0':
        arch = EfficientNet.from_name('efficientnet-b0') # from_name is used to load model without pretrained weights
    elif args.model == 'efficientnet-b1':
        arch  = EfficientNet.from_pretrained('efficientnet-b1') # from_pretrained is used to load model with pretrained weights
    elif args.model == 'efficientnet-b2':
        arch = EfficientNet.from_pretrained('efficientnet-b2')
    elif args.model == 'efficientnet-b3':
        arch  = EfficientNet.from_pretrained('efficientnet-b3')
    elif args.model == 'efficientnet-b4':
        arch  = EfficientNet.from_pretrained('efficientnet-b4')
    elif args.model == 'efficientnet-b5':
        arch  = EfficientNet.from_pretrained('efficientnet-b5')
    elif args.model == 'efficientnet-b6':
        arch  = EfficientNet.from_pretrained('efficientnet-b6')
    elif args.model == 'efficientnet-b7':
        arch  = EfficientNet.from_pretrained('efficientnet-b7')
    elif args.model =='resnet50':
        arch = torchvision.M.get_model('resnet50', pretrained=False)
    elif args.model =='densenet121':
        arch = torchvision.M.get_model('densenet121', pretrained=True)
    else:
        raise ValueError('Invalid model name')


    model_path = f'model_{args.model}.pth' # model file to save the model
    log_path = f'log_{args.model}.txt'     # log file to save the logs
    epochs = 300                           # number of epochs to train the model

    es_patience = 10                       # early stopping patience
    criterion = nn.CrossEntropyLoss()      # loss function (might add weights in future)





    best_val = 0               # best validation accuracy
    patience = es_patience     # init patience counter

    # in future when smartphone app will be made, we will explore possibility of using meta features
    # and their effect on the model
    # model = Net(arch = arch, n_meta_features = len(meta_features))
    # for now we will use only the image features
    model = Net_without_meta(arch = arch)
    
    # move the model to the device
    # in newer versions of pytorch, this is done differently!! so errors may occur
    model.to(device)
    # set the optimizer and learning_rate
    optimizer = torch.optim.Adam(model.parameters(), lr=args.lr)
    # scheduler = torch.optim.lr_scheduler.ReduceLROnPlateau(optimizer = optimizer, mode='max', factor=0.2, patience=1, verbose=True)

    # create cosine lr scheduler with warm restarts i want to train from scratch lr = 1e-3
    scheduler = torch.optim.lr_scheduler.CosineAnnealingWarmRestarts(optimizer, T_0=10, T_mult=2, eta_min=1e-6, last_epoch=-1)

    # create a metrics logger
    metrics_logger = metricsLogger(args)

    # start training
    for epoch in tqdm(range(epochs)):
       

        start_time = time.time()
        correct = 0
        train_epoch_loss = 0
        val_epoch_loss = 0
        model.train()

        for x, y in tqdm(train_loader, total=len(train_loader)):
            x[0] = x[0].to(device)
            x[1] = x[1].to(device)
            y = y.to(device)
            # x[0] = torch.tensor(x[0], dtype=torch.float32).to(device)
            # x[1] = torch.tensor(x[1], dtype=torch.float32).to(device)
            # y = y.type(torch.LongTensor).to(device)

            optimizer.zero_grad()
            z = model(x)
            # detach and convert to numpy array
            loss = criterion(z, y)
            loss.backward()
            optimizer.step()
            pred = (F.softmax(z, dim=1))
           
            # the label with the highest energy willbe our prediction
            _, predicted = torch.max(z.data, 1)
            correct += (predicted == y).sum().item()
            train_epoch_loss += loss.item()
        train_acc = correct/len(train)

        model.eval()  # switch model to the evaluation mode
        # create emptytorch tensor to store predictions
        val_preds = torch.zeros((len(valid_df), 8), dtype=torch.float32, device=device)
        val_preds_non_softmax = torch.zeros((len(valid_df), 8), dtype=torch.float32, device=device)
        with torch.no_grad():  # Do not calculate gradient since we are only predicting
            # Predicting on validation set
            for j, (x_val, y_val) in enumerate(val_loader):
                x_val[0] = x_val[0].to(device)
                x_val[1] = x_val[1].to(device)
                y_val = y_val.to(device)
                z_val = model(x_val)
                val_epoch_loss += criterion(z_val, y_val).item()
            
                val_pred = (F.softmax(z_val, dim=1))

                val_preds[j*val_loader.batch_size:j*val_loader.batch_size + x_val[0].shape[0]] = val_pred
                val_preds_non_softmax[j*val_loader.batch_size:j*val_loader.batch_size + x_val[0].shape[0]] = z_val

  
            val_acc = accuracy_score(valid_df['target'].values, (torch.max(val_preds_non_softmax.data, 1)[1].cpu()))
            bal_acc = balanced_accuracy_score(valid_df['target'].values, (torch.max(val_preds_non_softmax.data, 1)[1].cpu()))
        
            val_roc = roc_auc_score(valid_df['target'].values, val_preds.cpu(), multi_class='ovr')

            train_epoch_loss = train_epoch_loss / len(train_loader)
            val_epoch_loss = val_epoch_loss / len(val_loader)
            print('Epoch {:03}: | Train Loss: {:.3f} | Val loss {:.3f} | Train acc: {:.3f} | Val acc: {:.3f} | Balanced val acc: {:.3f} | Val roc_auc: {:.3f} | Training time: {}'.format(
            epoch + 1, 
            train_epoch_loss, 
            val_epoch_loss,
            train_acc, 
            val_acc, 
            bal_acc,
            val_roc, 
            str(datetime.timedelta(seconds=time.time() - start_time))[:7]))
            with open(log_path, 'a') as f:
                f.write('Epoch {:03}: | Train Loss: {:.3f} | Val loss {:.3f} | Train acc: {:.3f} | Val acc: {:.3f} | Balanced val acc: {:.3f} | Val roc_auc: {:.3f} | Training time: {}'.format(
            epoch + 1, 
            train_epoch_loss, 
            val_epoch_loss,
            train_acc, 
            val_acc, 
            bal_acc,
            val_roc, 
            str(datetime.timedelta(seconds=time.time() - start_time))[:7]))
            
            # update all metrics - epoch, all losses, all accuracies and learning rate
            metrics_logger.update(epoch = epoch, train_loss = train_epoch_loss,valid_loss =  val_epoch_loss, train_acc = train_acc, valid_acc =  val_acc, valid_balanced_acc = bal_acc, lr = optimizer.param_groups[0]['lr'])
           


            scheduler.step(bal_acc)

            
                
            if bal_acc >= best_val:
                best_val = bal_acc
                patience = es_patience  # Resetting patience since we have new best validation accuracy
                torch.save(model, model_path)  # Saving current best model
                metrics_logger.save()
                
            else:
                patience -= 1
                if patience == 0:
                    print('Early stopping. Best bal accuracy: {:.3f}'.format(best_val))
                    break
    # save all metrics
    metrics_logger.save()


# function toread models from the current directory
def read_models(models):
    # they are divided by comma and and .pth extension
    models = models.split(',')
    # add the .pth extension
    models = [model + '.pth' for model in models]
    # add 'model_' prefix
    # models = ['model_' + model for model in models]
    return models


# func to train ensemble model, however, it is not used as we average the predictions of the models
def train_ensemble(args, train_loader, val_loader, test_loader, train_df, valid_df, test_df, device):
    # rewritten train_model function to fit the ensemble model
    # input the models to train

    # load the models from the current directory
    models = []

    for model in read_models(args.ensemble):
        models.append(torch.load(model,map_location=device))
        #models.append(torch.load(model).to(device))
        
    # create the ensemble model
    model = Ensemble(models)
    model.to(device)

    # define the loss function
    criterion = nn.CrossEntropyLoss()
    # define the optimizer
    optimizer = torch.optim.Adam(model.parameters(), lr=args.lr)
    # define the scheduler
    scheduler = torch.optim.lr_scheduler.ReduceLROnPlateau(optimizer = optimizer, mode='max', factor=0.2, patience=1, verbose=True)
    # define the early stopping
    es_patience = 5
    patience = es_patience
    best_val = 0
    # define the path to save the model
    # as a name accept the models that are used in the ensemble each one separated by comma change to _
    model_path = 'ensemble_' + args.ensemble.replace(',', '_') + '.pth'
    # define the path to save the log
    log_path = 'ensemble_' + args.ensemble.replace(',', '_') + '.txt'
    # define the number of epochs
    epochs = 12

    for epoch in range(epochs):
        start_time = time.time()
        correct = 0
        epoch_loss = 0
        model.train()

        for x, y in tqdm(train_loader, total=len(train_loader)):
            x[0] = x[0].to(device)
            x[1] = x[1].to(device)
            y = y.to(device)
            # x[0] = torch.tensor(x[0], dtype=torch.float32).to(device)
            # x[1] = torch.tensor(x[1], dtype=torch.float32).to(device)
            # y = y.type(torch.LongTensor).to(device)

            optimizer.zero_grad()
            z = model(x)
            # detach and convert to numpy array
            loss = criterion(z, y)
            loss.backward()
            optimizer.step()
            pred = (F.softmax(z, dim=1))
           
            # the label with the highest energy willbe our prediction
            _, predicted = torch.max(z.data, 1)
            correct += (predicted == y).sum().item()
            epoch_loss += loss.item()
        train_acc = correct/len(train)

        model.eval()  # switch model to the evaluation mode
        # create emptytorch tensor to store predictions
        val_preds = torch.zeros((len(valid_df), 8), dtype=torch.float32, device=device)
        val_preds_non_softmax = torch.zeros((len(valid_df), 8), dtype=torch.float32, device=device)
        with torch.no_grad():  # Do not calculate gradient since we are only predicting
            # Predicting on validation set
            for j, (x_val, y_val) in enumerate(val_loader):
                x_val[0] = x_val[0].to(device)
                x_val[1] = x_val[1].to(device)
                y_val = y_val.to(device)
                z_val = model(x_val)
            
                val_pred = (F.softmax(z_val, dim=1))

                val_preds[j*val_loader.batch_size:j*val_loader.batch_size + x_val[0].shape[0]] = val_pred
                val_preds_non_softmax[j*val_loader.batch_size:j*val_loader.batch_size + x_val[0].shape[0]] = z_val

  
            val_acc = accuracy_score(valid_df['target'].values, (torch.max(val_preds_non_softmax.data, 1)[1].cpu()))
            bal_acc = balanced_accuracy_score(valid_df['target'].values, (torch.max(val_preds_non_softmax.data, 1)[1].cpu()))
        
            val_roc = roc_auc_score(valid_df['target'].values, val_preds.cpu(), multi_class='ovr')

            
            print('Epoch {:03}: | Loss: {:.3f} | Train acc: {:.3f} | Val acc: {:.3f} | Balanced val acc: {:.3f} | Val roc_auc: {:.3f} | Training time: {}'.format(
            epoch + 1, 
            epoch_loss, 
            train_acc, 
            val_acc, 
            bal_acc,
            val_roc, 
            str(datetime.timedelta(seconds=time.time() - start_time))[:7]))
            with open(log_path, 'a') as f:
                f.write('\n\nEpoch {:03}: | Loss: {:.3f} | Train acc: {:.3f} | Val acc: {:.3f} | Balanced val acc: {:.3f} | Val roc_auc: {:.3f} | Training time: {}\n\n'.format(
                epoch + 1, 
                epoch_loss, 
                train_acc, 
                val_acc, 
                bal_acc,
                val_roc, 
                str(datetime.timedelta(seconds=time.time() - start_time))[:7]))
            
            scheduler.step(val_roc)
                
            if val_roc >= best_val:
                best_val = val_roc
                patience = es_patience  # Resetting patience since we have new best validation accuracy
                torch.save(model, model_path)  # Saving current best model
                
            else:
                patience -= 1
                if patience == 0:
                    print('Early stopping. Best Val roc_auc: {:.3f}'.format(best_val))
                    break
                    start_time = time.time()
        correct = 0
        epoch_loss = 0
        model.train()

        for x, y in tqdm(train_loader, total=len(train_loader)):
            x[0] = x[0].to(device)
            x[1] = x[1].to(device)
            y = y.to(device)
            # x[0] = torch.tensor(x[0], dtype=torch.float32).to(device)
            # x[1] = torch.tensor(x[1], dtype=torch.float32).to(device)
            # y = y.type(torch.LongTensor).to(device)

            optimizer.zero_grad()
            z = model(x)
            # detach and convert to numpy array
            loss = criterion(z, y)
            loss.backward()
            optimizer.step()
            pred = (F.softmax(z, dim=1))
           
            # the label with the highest energy willbe our prediction
            _, predicted = torch.max(z.data, 1)
            correct += (predicted == y).sum().item()
            epoch_loss += loss.item()
        train_acc = correct/len(train)

        model.eval()  # switch model to the evaluation mode
        # create emptytorch tensor to store predictions
        val_preds = torch.zeros((len(valid_df), 8), dtype=torch.float32, device=device)
        val_preds_non_softmax = torch.zeros((len(valid_df), 8), dtype=torch.float32, device=device)
        with torch.no_grad():  # Do not calculate gradient since we are only predicting
            # Predicting on validation set
            for j, (x_val, y_val) in enumerate(val_loader):
                x_val[0] = x_val[0].to(device)
                x_val[1] = x_val[1].to(device)
                y_val = y_val.to(device)
                z_val = model(x_val)
            
                val_pred = (F.softmax(z_val, dim=1))

                val_preds[j*val_loader.batch_size:j*val_loader.batch_size + x_val[0].shape[0]] = val_pred
                val_preds_non_softmax[j*val_loader.batch_size:j*val_loader.batch_size + x_val[0].shape[0]] = z_val

  
            val_acc = accuracy_score(valid_df['target'].values, (torch.max(val_preds_non_softmax.data, 1)[1].cpu()))
            bal_acc = balanced_accuracy_score(valid_df['target'].values, (torch.max(val_preds_non_softmax.data, 1)[1].cpu()))
        
            val_roc = roc_auc_score(valid_df['target'].values, val_preds.cpu(), multi_class='ovr')

            
            print('Epoch {:03}: | Loss: {:.3f} | Train acc: {:.3f} | Val acc: {:.3f} | Balanced val acc: {:.3f} | Val roc_auc: {:.3f} | Training time: {}'.format(
            epoch + 1, 
            epoch_loss, 
            train_acc, 
            val_acc, 
            bal_acc,
            val_roc, 
            str(datetime.timedelta(seconds=time.time() - start_time))[:7]))
            with open(log_path, 'a') as f:
                f.write('\n\nEpoch {:03}: | Loss: {:.3f} | Train acc: {:.3f} | Val acc: {:.3f} | Balanced val acc: {:.3f} | Val roc_auc: {:.3f} | Training time: {}\n\n'.format(
                epoch + 1, 
                epoch_loss, 
                train_acc, 
                val_acc, 
                bal_acc,
                val_roc, 
                str(datetime.timedelta(seconds=time.time() - start_time))[:7]))
            
            scheduler.step(val_roc)
                
            if val_roc >= best_val:
                best_val = val_roc
                patience = es_patience  # Resetting patience since we have new best validation accuracy
                torch.save(model, model_path)  # Saving current best model
                
            else:
                patience -= 1
                if patience == 0:
                    print('Early stopping. Best Val roc_auc: {:.3f}'.format(best_val))
                    break

#func to form ensemble models from saved models in current dir, return model
def form_ensemble_model(models, device):
    

    models_list = []
    for model in read_models(models):
        # models_list.append(torch.load(model).to(device))

        models_list.append(torch.load(model,map_location=device))
    # create the ensemble model
    model = Ensemble(models_list)
    model.to(device)
    return model




def master_test(args, test_loader, test_df, device):
    """
    Function to test model or ensemble of models
    """
    # log path as name of ensamble or model, if ensamble then each particular model is divided by '_' instead of comma and start with test_model_
    log_path = 'test_model_' + args.model + '.txt' if args.model is not None else 'test_ensemble_' + '_'.join(args.ensemble.split(',')) + '.txt'
    if args.model is not None:
        # arg model is without pth extension and model_ prefix, add it
        # model = torch.load(args.model + '.pth').to(device)
        model= torch.load(args.model+'.pth',map_location=device)
        acc, bal_acc, auc, dict_all_metrics = test_model(model, test_loader, test_df, device, log_path)
    elif args.ensemble is not None:
        model = form_ensemble_model(args.ensemble,  device)
        acc, bal_acc, auc, dict_all_metrics = test_model(model, test_loader, test_df, device, log_path)

    return acc, bal_acc, auc, dict_all_metrics


# create same function as test_model but without the gt dataframe
def test_model_no_df(model_name,model, test_loader, test_df, device, log_path):
    """
    Function to test model without ground-truth dataframe
    """
    model.eval()
    
    test_preds = torch.zeros((len(test_df), 8), dtype=torch.float32, device=device)
    test_preds_non_softmax = torch.zeros((len(test_df), 8), dtype=torch.float32, device=device)
    with torch.no_grad():  # Do not calculate gradient since we are only predicting
        # Predicting on validation set
        for j, (x_test, y_test) in tqdm(enumerate(test_loader), total=len(test_loader)):
            x_test[0] = x_test[0].to(device)
            x_test[1] = x_test[1].to(device)
            y_test = y_test.to(device)
            # print out shape of the x_test
            z_test = model(x_test)
            # this is used for probabilities
            test_pred = (F.softmax(z_test, dim=1))
            test_preds[j*test_loader.batch_size:j*test_loader.batch_size + x_test[0].shape[0]] = test_pred
            test_preds_non_softmax[j*test_loader.batch_size:j*test_loader.batch_size + x_test[0].shape[0]] = z_test



    
        # save predictions to csv called predictions_{model_name}.csv but if ensamble than predictions_ensemble_{model1}_{model2}_{model3}.csv, split (,) to get models
        test_df['preds'] = (torch.max(test_preds_non_softmax.data, 1)[1].cpu()).numpy()
        # get highest probability for each row
        test_df['probs'] = test_preds.max(1)[0].cpu().numpy()
        # set unknown to true if highest probability is less than 0.5
        test_df['unknown'] = test_df['probs'] < 0.5

        # save also probabilities for each class create columns for each class and fill them with probabilitieskeep order of classes specified in class_names
            
        for i in range(8):
            test_df['prob_' + str(i)] = test_preds[:, i].cpu().numpy()
        # write info about augmentations used which can be later got from csv by df.info()
        # if augmentations_used is not None:
        #     test_df.info = augmentations_used
        # test_df.to_csv('stats/predictions_' + args.model + '.csv' if args.model is not None else 'predictions_ensemble_' + '_'.join(args.ensemble.split(',')) + '.csv', index=False)
        # basic name
        test_df.to_csv(f'stats/{model_name}_empty_preds.csv', index=False)
    return None


# function to run test set on a model
def test_model( model, test_loader, test_df, device, log_path, augmentations_used=None):
    """
    Function to test model
    """
    model.eval()
    
    test_preds = torch.zeros((len(test_df), 8), dtype=torch.float32, device=device)
    test_preds_non_softmax = torch.zeros((len(test_df), 8), dtype=torch.float32, device=device)
    with torch.no_grad():  # Do not calculate gradient since we are only predicting
        # Predicting on validation set
        for j, (x_test, y_test) in tqdm(enumerate(test_loader), total=len(test_loader)):
            x_test[0] = x_test[0].to(device)
            x_test[1] = x_test[1].to(device)
            y_test = y_test.to(device)
            # print out shape of the x_test
            z_test = model(x_test)
            # this is used for probabilities
            test_pred = (F.softmax(z_test, dim=1))
            test_preds[j*test_loader.batch_size:j*test_loader.batch_size + x_test[0].shape[0]] = test_pred
            test_preds_non_softmax[j*test_loader.batch_size:j*test_loader.batch_size + x_test[0].shape[0]] = z_test



    
        # save predictions to csv called predictions_{model_name}.csv but if ensamble than predictions_ensemble_{model1}_{model2}_{model3}.csv, split (,) to get models
        test_df['preds'] = (torch.max(test_preds_non_softmax.data, 1)[1].cpu()).numpy()
        # get highest probability for each row
        test_df['probs'] = test_preds.max(1)[0].cpu().numpy()
        # set unknown to true if highest probability is less than 0.5
        test_df['unknown'] = test_df['probs'] < 0.5

        # save also probabilities for each class create columns for each class and fill them with probabilitieskeep order of classes specified in class_names
            
        for i in range(8):
            test_df['prob_' + str(i)] = test_preds[:, i].cpu().numpy()
        # write info about augmentations used which can be later got from csv by df.info()
        # if augmentations_used is not None:
        #     test_df.info = augmentations_used
        # test_df.to_csv('stats/predictions_' + args.model + '.csv' if args.model is not None else 'predictions_ensemble_' + '_'.join(args.ensemble.split(',')) + '.csv', index=False)
        # basic name
        test_df.to_csv('stats/preds.csv', index=False)



        test_acc = accuracy_score(test_df['target'].values, (torch.max(test_preds_non_softmax.data, 1)[1].cpu()))
        bal_acc = balanced_accuracy_score(test_df['target'].values, (torch.max(test_preds_non_softmax.data, 1)[1].cpu()))
        
        test_roc = roc_auc_score(test_df['target'].values, test_preds.cpu(), multi_class='ovr')
        test_f1_score = f1_score(test_df['target'].values, (torch.max(test_preds_non_softmax.data, 1)[1].cpu()), average='macro')
        test_precision = precision_score(test_df['target'].values, (torch.max(test_preds_non_softmax.data, 1)[1].cpu()), average='macro')
        test_recall = recall_score(test_df['target'].values, (torch.max(test_preds_non_softmax.data, 1)[1].cpu()), average='macro')

        
        print(' Test acc: {:.3f} | Balanced val acc: {:.3f} | Test roc_auc: {:.3f} '.format(


        test_acc, 
        bal_acc,
        test_roc, )
        )
        with open(log_path, 'a') as f:
            f.write('{} \n\ Test acc: {:.3f} | Balanced test acc: {:.3f} | Val roc_auc: {:.3f}\n '.format(
        args.size,
        test_acc, 
        bal_acc,
        test_roc, )
        )
    dict_all_metrics = {'test_acc': test_acc, 'bal_acc': bal_acc, 'test_roc': test_roc, 'test_f1_score': test_f1_score, 'test_precision': test_precision, 'test_recall': test_recall}
    return test_acc, bal_acc, test_roc, dict_all_metrics

# not used currently
def test_model_TTA(TTA, model, test_loader, test_df, device, log_path, augmentations_used=None):
    """
    Function to test model in a TTA style
    """
    model.eval()
    
    tta_preds = torch.zeros((len(test_df), 8), dtype=torch.float32, device=device)
    tta_preds_non_softmax = torch.zeros((len(test_df), 8), dtype=torch.float32, device=device)
    for _ in range(TTA):
        test_preds = torch.zeros((len(test_df), 8), dtype=torch.float32, device=device)
        test_preds_non_softmax = torch.zeros((len(test_df), 8), dtype=torch.float32, device=device)
        with torch.no_grad():  # Do not calculate gradient since we are only predicting
            # Predicting on validation set
            for j, (x_test, y_test) in tqdm(enumerate(test_loader), total=len(test_loader)):
                x_test[0] = x_test[0].to(device)
                x_test[1] = x_test[1].to(device)
                y_test = y_test.to(device)
                # print out shape of the x_test
                z_test = model(x_test)
                # this is used for probabilities
                test_pred = (F.softmax(z_test, dim=1))
                test_preds[j*test_loader.batch_size:j*test_loader.batch_size + x_test[0].shape[0]] = test_pred
                test_preds_non_softmax[j*test_loader.batch_size:j*test_loader.batch_size + x_test[0].shape[0]] = z_test
        tta_preds+=test_preds
        tta_preds_non_softmax+=test_preds_non_softmax
    test_preds = tta_preds/TTA
    test_preds_non_softmax = tta_preds_non_softmax/TTA

    
    # save predictions to csv called predictions_{model_name}.csv but if ensamble than predictions_ensemble_{model1}_{model2}_{model3}.csv, split (,) to get models
    test_df['preds'] = (torch.max(test_preds_non_softmax.data, 1)[1].cpu()).numpy()
    # save also probabilities for each class create columns for each class and fill them with probabilitieskeep order of classes specified in class_names
        
    for i in range(8):
        test_df['prob_' + str(i)] = test_preds[:, i].cpu().numpy()
    # write info about augmentations used which can be later got from csv by df.info()
    # if augmentations_used is not None:
    #     test_df.info = augmentations_used
    # test_df.to_csv('stats/predictions_' + args.model + '.csv' if args.model is not None else 'predictions_ensemble_' + '_'.join(args.ensemble.split(',')) + '.csv', index=False)
    # basic name
    test_df.to_csv('stats/preds.csv', index=False)



    test_acc = accuracy_score(test_df['target'].values, (torch.max(test_preds_non_softmax.data, 1)[1].cpu()))
    bal_acc = balanced_accuracy_score(test_df['target'].values, (torch.max(test_preds_non_softmax.data, 1)[1].cpu()))
    
    test_roc = roc_auc_score(test_df['target'].values, test_preds.cpu(), multi_class='ovr')
      
    print(' Test acc: {:.3f} | Balanced val acc: {:.3f} | Test roc_auc: {:.3f} '.format(


    test_acc, 
    bal_acc,
    test_roc, )
    )
    with open(log_path, 'a') as f:
        f.write('{} \n\ Test acc: {:.3f} | Balanced test acc: {:.3f} | Val roc_auc: {:.3f}\n '.format(
    args.size,
    test_acc, 
    bal_acc,
    test_roc, )
    )

def iterate_and_save(dataset): # this is a helper function for visualisations
    # for i, data in enumerate(dataset):
    #     print(i)
    #     (image, meta), target = data
    #     save_image(image, 'tmp/image_tmp.png')
    #     input('Press enter to generate next image')
    data = dataset[206]
    (image, meta), target = data
    save_image(image, 'tmp/color_50_0_50_image.png')
    






def get_test_augmentations(args, custom_threshold):
    """
    This function is used to simulate 
    real world data.
    Feel free to uncomment prepared augmentations
    according to your needs.
    Args:
        args: arguments from cli
        custom_threshold: threshold for custom augmentations
                        if you want to test a range of thresholds
                        you can pass a list of thresholds
                        if not, pass a single value in a list
    Returns:
        A.Compose object with augmentations
    """
    return A.Compose([
    #      A.Transpose(p=0.5),
    # A.VerticalFlip(p=0.5),
    # A.HorizontalFlip(p=0.5),
    # A.RandomBrightness(limit=0.2, p=0.75),
    # A.RandomContrast(limit=0.2, p=0.75),
    # A.OneOf([
    #     A.MotionBlur(blur_limit=5),
    #     A.MedianBlur(blur_limit=5),
    #     A.GaussianBlur(blur_limit=5),
    #     A.GaussNoise(var_limit=(5.0, 30.0)),
    # ], p=0.7),

    # A.OneOf([
    #     A.OpticalDistortion(distort_limit=1.0),
    #     A.GridDistortion(num_steps=5, distort_limit=1.),
    #     A.ElasticTransform(alpha=3),
    # # ], p=0.7),

    # A.CLAHE(clip_limit=4.0, p=0.7),
    # A.HueSaturationValue(hue_shift_limit=10, sat_shift_limit=20, val_shift_limit=10, p=0.5),
    # A.ShiftScaleRotate(shift_limit=0.1, scale_limit=0.1, rotate_limit=15, border_mode=0, p=0.85),
    # random brightness contrast but contrast is 0, only change brightness
    # A.RandomBrightnessContrast(brightness_limit=(custom_threshold,custom_threshold), contrast_limit=0, p=1),
    A.Resize(custom_threshold, custom_threshold, p=1),
    # A.Resize(256, 256, p=1),
    # A.Blur(blur_limit=(10,10), p=1),
    # color shift
    # A.RGBShift(r_shift_limit=(50, 50), g_shift_limit=(0,0), b_shift_limit=(50,50), p=1),
    # A.RandomBrightnessContrast(brightness_limit=(0.2,0.2), contrast_limit=0, p=1),
    # A.Downscale(scale_min=custom_threshold, scale_max=custom_threshold, p=1),
    # A.Downscale(scale_min=0.4, scale_max=0.4, p=1),
    # A.Cutout(max_h_size=int(256 * 0.375), max_w_size=int(256 * 0.375), num_holes=1, p=0.7), #256 is image size
    
    A.Normalize()
    ])

def plot_test_augmentations(acc_list, bal_acc_list, roc_auc_list, thresholds):
    # this function will plot the results of the augmentation test
    # all three will be saved into one figure with different colors
    # acc_list - list of accuracies
    # bal_acc_list - list of balanced accuracies
    # roc_auc_list - list of roc auc scores
    # thresholds - list of thresholds at which the augmentation was applied
    plt.figure(figsize=(10, 10))
    plt.plot(thresholds, acc_list, color='red', label='Accuracy')
    plt.plot(thresholds, bal_acc_list, color='blue', label='Balanced accuracy')
    plt.plot(thresholds, roc_auc_list, color='green', label='ROC AUC')
    plt.xlabel('Size')
    plt.ylabel('Score')
    plt.legend()
    plt.grid()
    plt.savefig('stats/augmentation_test.pdf')

    plt.close()




# test time augmentations
def test_with_augmentations(args, test_df, device):
    """ Function to test model on simulated real world data
    Args:
        args: arguments from cli
        test_df: dataframe with test data
        device: device to use for testing
    Returns:
        acc_list: list of accuracies
        bal_acc_list: list of balanced accuracies
        roc_auc_list: list of roc auc scores
        dict_all_metrics: dictionary with all metrics
    """
    # get_data from func get_data()
    print('Testing with augmentations...')
    print('Image size: {}x{}'.format(args.size, args.size))
    # get_model name from args, it is either specified as model or ensemble
    # in case of ensemble the name is more models separated by comma, separate them by _ insead of comma
    model_name = args.model if args.model else args.ensemble.replace(',', '_')


    _, _, test_df, meta_features = get_data()
    acc_list = []
    bal_acc_list = []
    roc_auc_list = []
    f1_list = []
    precision_list = []
    recall_list = [] 

    # we provide a list of thresholds at which the augmentation will be applied
    #     feel free to uncomment the one you want to use
    #     or crate your own

    # thresholds = np.arange(0.01, 1.05, 0.05)
    # create thresholds from 0.01 to 0.99 inclucing 
    # thresholds = np.arange(0.01, 1, 0.03)
    thresholds = [256]

    # thresholds = np.arange(256, 66, -10)
    # thresholds = np.arange(256,394,10)
    #thresholds = np.arange(256)
    # threhs for blur from 1 to 25 with step 1
    # thresholds = np.arange(1, 26, 1)
    # for brightness from -1, to 1, with step 0.05 including 1
    # thresholds = np.arange(-1, 1.05, 0.05)
    # define threshold list with 3 values for each color channel, there are 3 possible values for each channel 0,-50,50, define each combination
    # thresholds = [[0,0,0], [0,0,50], [0,0,-50], [0,50,0], [0,50,50], [0,50,-50], [0,-50,0], [0,-50,50], [0,-50,-50], [50,0,0], [50,0,50], [50,0,-50], [50,50,0], [50,50,50], [50,50,-50], [50,-50,0], [50,-50,50], [50,-50,-50], [-50,0,0], [-50,0,50], [-50,0,-50], [-50,50,0], [-50,50,50], [-50,50,-50], [-50,-50,0], [-50,-50,50], [-50,-50,-50]]


    # thresholds= [256]

        
    # iterate over thresholds
    for threshold in tqdm(thresholds):
        print('Threshold: {}'.format(threshold))
        # create new test loader and apply augmentations
        test = MelanomaDataset(df=test_df,
                                imfolder='/mnt/datagrid/Medical/skin/data/jpeg-isic2019-512x512/train/',
                                train=True, # tihs is when we have ground truth
                                transforms=get_test_augmentations(args, threshold),
                                meta_features=None)
        test_loader = torch.utils.data.DataLoader(
            dataset=test, batch_size=args.batch_size, shuffle=False, num_workers=2
            )


        # run test
        acc, bal_acc, auc, dict_all_metrics = master_test(args, test_loader, test_df, device)
        acc_list.append(acc)
        bal_acc_list.append(bal_acc)
        roc_auc_list.append(auc)
        #     dict_all_metrics = {'test_acc': test_acc, 'bal_acc': bal_acc, 'test_roc': test_roc, 'test_f1_score': test_f1_score, 'test_specificity': test_specificity, 'test_precision': test_precision, 'test_recall': test_recall}
        f1_list.append(dict_all_metrics['test_f1_score'])
        precision_list.append(dict_all_metrics['test_precision'])
        recall_list.append(dict_all_metrics['test_recall'])

    # run test
    # acc, bal_acc, auc = master_test(args, test_loader, test_df, device)
    # plot_test_augmentations(acc_list, 
    ploted_metrics_df = pd.DataFrame({'thresholds': thresholds, 'acc_list': acc_list, 'bal_acc_list': bal_acc_list, 'roc_auc_list': roc_auc_list, 'f1_list': f1_list, 'precision_list': precision_list, 'recall_list': recall_list})
    ploted_metrics_df.to_csv(f'stats/augmentation_test_{model_name}.csv', index=False)
    return acc_list, bal_acc_list, roc_auc_list, dict_all_metrics

        
def get_class_names_convertor():
    # function to return dictionary of class names and their corresponding numbers
    # nv, mel, bcc, bkl, ak, scc, df , vasc
    return {'NV': 0, 'MEL': 1, 'BCC': 2, 'BKL': 3, 'AK': 4, 'SCC': 5, 'DF': 6, 'VASC': 7}

def get_data():
    """ Function to get all necessary data for training, validation and testing
    Args:
        None
    Returns:
        train_df: dataframe with training data
        valid_df: dataframe with validation data
        test_df: dataframe with testing data
        meta_features: list of meta features
    """
    data_pth = '/mnt/datagrid/Medical/skin/data/jpeg-isic2019-512x512'

     # nv, mel, bcc, bkl, ak, scc, df , vasc
    class_names = get_class_names_convertor()
    # map class names to numbers
    main_df = pd.read_csv(data_pth + '/train.csv')
    main_df['diagnosis'] = main_df['diagnosis'].map(class_names)

    # create assert to check the order of the old_main_df and main_df except the diagnosis column

    # drop target col
    main_df.drop('target', axis=1, inplace=True)
    # rename columns diagnosis to target
    main_df.rename(columns={'diagnosis': 'target'}, inplace=True)


    train_df, valid_df, test_df = \
                np.split(main_df.sample(frac=1, random_state=42), 
                        [int(.6*len(main_df)), int(.8*len(main_df))])

    train_df.reset_index(drop=False, inplace=True)
    valid_df.reset_index(drop=False, inplace=True)
    test_df.reset_index(drop=False, inplace=True)



    train_df.to_csv('/mnt/datagrid/Medical/skin/data/custom_train.csv', index=False)
    valid_df.to_csv('/mnt/datagrid/Medical/skin/data/custom_valid.csv', index=False)
    test_df.to_csv('/mnt/datagrid/Medical/skin/data/custom_test.csv', index=False)




    

    # One-hot encoding of anatom_site_general_challenge feature
    concat = pd.concat([train_df['anatom_site_general_challenge'], valid_df['anatom_site_general_challenge'], test_df['anatom_site_general_challenge']], ignore_index=True)
    
    dummies = pd.get_dummies(concat, dummy_na=True, dtype=np.uint8, prefix='site')
    # the following line is important, because it ensures that the order of columns is the same in train, valid and test datasets
    train_df = pd.concat([train_df, dummies.iloc[:train_df.shape[0]]], axis=1)
    valid_df = pd.concat([valid_df, dummies.iloc[train_df.shape[0]:train_df.shape[0] + valid_df.shape[0]].reset_index(drop=True)], axis=1)
    test_df = pd.concat([test_df, dummies.iloc[train_df.shape[0]+valid_df.shape[0]:].reset_index(drop=True)], axis=1)
    


    

    # Sex features
    train_df['sex'] = train_df['sex'].map({'male': 1, 'female': 0})
    valid_df['sex'] = valid_df['sex'].map({'male': 1, 'female': 0})
    test_df['sex'] = test_df['sex'].map({'male': 1, 'female': 0})
    train_df['sex'] = train_df['sex'].fillna(-1)
    valid_df['sex'] = valid_df['sex'].fillna(-1)
    test_df['sex'] = test_df['sex'].fillna(-1)

    # Age features
    train_df['age_approx'] /= train_df['age_approx'].max()
    valid_df['age_approx'] /= valid_df['age_approx'].max()
    test_df['age_approx'] /= test_df['age_approx'].max()
    train_df['age_approx'] = train_df['age_approx'].fillna(0)
    valid_df['age_approx'] = valid_df['age_approx'].fillna(0)
    test_df['age_approx'] = test_df['age_approx'].fillna(0)

    train_df['patient_id'] = train_df['patient_id'].fillna(0)


    meta_features = ['sex', 'age_approx'] + [col for col in train_df.columns if 'site_' in col]

    
    meta_features.remove('anatom_site_general_challenge')


    return train_df, valid_df, test_df, meta_features

# crete function to run evaluation metrics from scikit learn
# read csv file with predictions and ground truth

# master evaluate metrics function which combines evaluation metrics functions created 
def master_evaluate_metrics(args, eval_df):
    """ Function to evaluate metrics
    Args:
        args: arguments from cli
        eval_df: dataframe with predictions and ground truth
    Returns:
        None
    """

    metrics = evaluate_metrics(eval_df)
    # save metrics to csv file
    save_metrics(metrics, path='stats/metrics.txt')







def evaluate_metrics(master_df):
    """ Function to evaluate metrics
    Args:
        master_df: dataframe with predictions and ground truth
    Returns:
        None
    """
    # read csv file with predictions and ground truth
    df = master_df

    # map values to keys
    # df.target = df.target.map(class_names)
    # df.preds = df.preds.map(class_names)

    

    # get predictions from csv file
    y_pred = df['preds']
    # get ground truth from csv file
    y_true = df['target']

   
   # probs_0, probs_1, ..., probs_7
    y_probs = df[['prob_0', 'prob_1', 'prob_2', 'prob_3', 'prob_4', 'prob_5', 'prob_6', 'prob_7']]
    # make list of lists per row
    # y_probs = y_probs.values.tolist()



    # calculate accuracy
    accuracy = accuracy_score(y_true, y_pred)
    # balanced accuracy
    balanced_accuracy = balanced_accuracy_score(y_true, y_pred)
    # calculate precision
    precision = precision_score(y_true, y_pred, average='macro')
    # calculate recall
    recall = recall_score(y_true, y_pred, average='macro')
    # calculate f1 score
    f1 = f1_score(y_true, y_pred, average='macro')
    # calculate roc auc score
    # y_true to numpy array
    # y_true_np = y_true.to_numpy()
    # # y_probs to numpy array each row are probs for one sample
    # y_probs = y_probs.to_numpy()


    roc_auc = roc_auc_score(y_true, y_probs, multi_class='ovr')

    # calculate confusion matrix
    conf_matrix = confusion_matrix(y_true, y_pred)
    # calculate classification report
    # class report targets convert to names
    class_names_dict = get_class_names_convertor()
    # reverse dict
    class_names = {v: k for k, v in class_names_dict.items()}

    y_true = y_true.map(class_names)
    y_pred = y_pred.map(class_names)    

    class_report = classification_report(y_true, y_pred)
    
    # create roc auc curve for each class and save it to roc_auc.pdf

    # plot_roc_curve(y_true, y_probs)
    # create confusion matrix heatmap and save it to conf_matrix.pdf
    plot_confusion_matrix(conf_matrix)
    # create metrics dictionary
    metrics = {'accuracy': accuracy,
               'balanced_accuracy': balanced_accuracy,
               'precision': precision,
               'recall': recall,
               'f1': f1,
               'roc_auc': roc_auc,
               'conf_matrix': conf_matrix,
               'class_report': class_report}

    return metrics

# save metrics to txt file each one to new line
def save_metrics(metrics, path):
    # save metrics to txt file
    with open(path, 'w') as f:
        for key, value in metrics.items():
            f.write(f'{key}: {value} \n')



def plot_confusion_matrix(conf_matrix):
    """ Function to save confusion matrix
    Args:
        conf_matrix: confusion matrix
    Returns:
        None
    """
    conf_matrix = conf_matrix.astype('float') / conf_matrix.sum(axis=1)[:, np.newaxis]

    class_names = get_class_names_convertor()
    # reverse dict
    class_names = {v: k for k, v in class_names.items()}
    # get class names
    class_names = list(class_names.values())
    # create figure
    fig = plt.figure(figsize=(10, 10))
    # create axis
    ax = fig.add_subplot(111)
    # plot confusion matrix
    sns.heatmap(conf_matrix, annot=True, fmt='.2f', cmap='Blues', cbar=False, xticklabels=class_names, yticklabels=class_names)
    # set title
    ax.set_title('Confusion Matrix')
    # set x label
    ax.set_xlabel('Predicted')
    # set y label
    ax.set_ylabel('Ground Truth')
    # save figure
    plt.savefig('stats/conf_matrix.pdf')
    # close figure
    plt.close()
    




def check_empty_dir(path):
    # check if directory is empty
    if os.listdir(path):
        return False
    else:
        return True

def delete_files_in_dir(path):
    # delete all files in directory
    for file in os.listdir(path):
        os.remove(os.path.join(path, file))

# function to delete files in stats directory but firstly ask the user if he wants to delete them if not end the program
def delete_stats():
    # check if stats directory is empty
    if check_empty_dir('stats'):
        print('Stats directory is empty')
    else:
        # ask user if he wants to delete files in stats directory
        answer = input('Stats directory is not empty, do you want to delete all files in stats directory? (y/n): ')
        # if answer is yes, delete all files in stats directory
        if answer == 'y':
            delete_files_in_dir('stats')
            print('All files in stats directory were deleted')
        # if answer is no, end the program
        elif answer == 'n':
            print('Program was ended')
            sys.exit()
        # if answer is not yes or no, ask user again
        else:
            print('Please enter y or n')
            delete_stats()



if __name__ == '__main__':

    #argparse to train the model #input name of the efficient model
    parser = argparse.ArgumentParser()
    parser.add_argument('--train', type=bool, default=False)
    parser.add_argument('--test', type=bool, default=False)
    # argument for running test with augmentations
    parser.add_argument('--test_aug', type=bool, default=False)
    # arg to run evaluation metrics
    parser.add_argument('--evaluate', type=bool, default=False)
    # argument to run image saver
    # parser.add_argument('--save_images', type=bool, default=False)
    # add arg one_model, it will be true, if user will write it in command line, if not, it will be false
    # parser.add_argument('--single_model', type=bool, default=False) 
    # parser.add_argument('--ensemble_model', type=bool, default=False)
    
    # add argument -m or --model to specify the model to train or test
    parser.add_argument('--model', type=str, default=None)
    # add argument -e or --ensemble to specify the ensemble of models to train or test
    parser.add_argument('--ensemble', type=str, default=None)
    
    parser.add_argument('--batch_size', type=int, default=8)
    # learning rate
    parser.add_argument('--lr', type=float, default=0.001)
    parser.add_argument('--size', type=int, default=256)
    # add arg to read csv for evaluation
    # parser.add_argument('--eval_csv', type=str, default=None)
    parser.add_argument('--cuda', type=int, default=4)
    # parser.add_argument('--TTA', type=int, default=1)
    # add argument to run test without gt
    parser.add_argument('--test_no_gt', type=bool, default=False)
    
    args = parser.parse_args()


    device = torch.device(f"cuda:{args.cuda}" if torch.cuda.is_available() else "cpu")
    # load data
    train_df, valid_df, test_df, meta_features = get_data()
    # create datasets
    train = MelanomaDataset(df=train_df,
                            imfolder='/mnt/datagrid/Medical/skin/data/jpeg-isic2019-512x512/train/', 
                            train=True, 
                            transforms=get_train_transform(),
                            meta_features=None)
    val = MelanomaDataset(df=valid_df,
                            imfolder='/mnt/datagrid/Medical/skin/data/jpeg-isic2019-512x512/train/',
                            train=True, # this is when we have ground truth
                            transforms=get_test_transform(),
                            meta_features=None)

    test = MelanomaDataset(df=test_df,
                            imfolder='/mnt/datagrid/Medical/skin/data/jpeg-isic2019-512x512/train/',
                            train=True, # tihs is when we have ground truth
                            transforms=get_test_augmentations(args, [256]),
                            meta_features=None)

    empty_df = pd.read_csv('/home.stud/sursamue/bakalarka/detection/yolov8/empty_bboxes_dataset.csv')
    empty = MelanomaDataset(df=empty_df,
                            imfolder='/home.stud/sursamue/bakalarka/detection/yolov8/empty_bboxes_dataset/',
                            train=False,
                            transforms=get_test_transform(),
                            meta_features=None)
    empty_phone_df = pd.read_csv('/home.stud/sursamue/bakalarka/detection/eval_dermanude/empty_images_phone/phone_df.csv')
    empty_phone_ds = MelanomaDataset(df=empty_phone_df,
                            imfolder='/home.stud/sursamue/bakalarka/detection/eval_dermanude/empty_images_phone/',
                            train=False,
                            transforms=get_test_transform(),
                            meta_features=None)
    empty_camera_df = pd.read_csv('/home.stud/sursamue/bakalarka/detection/eval_dermanude/empty_images_camera/camera_df.csv')
    empty_camera_ds = MelanomaDataset(df=empty_camera_df,
                            imfolder='/home.stud/sursamue/bakalarka/detection/eval_dermanude/empty_images_camera/',
                            train=False,
                            transforms=get_test_transform(),
                            meta_features=None)

    

    # create dataloaders with weighted sampler
    samples_weights = get_sampler(train_df)
    weighted_sampler = torch.utils.data.sampler.WeightedRandomSampler(samples_weights, len(samples_weights))
    train_loader = DataLoader(dataset=train, batch_size = args.batch_size, shuffle=False, num_workers=4, sampler = weighted_sampler)
    val_loader = DataLoader(dataset=val, batch_size=args.batch_size, shuffle=False, num_workers=4)
    test_loader = DataLoader(dataset=test, batch_size=args.batch_size, shuffle=False, num_workers=4)
    empty_loader = DataLoader(dataset=empty, batch_size=args.batch_size, shuffle=False, num_workers=4)
    empty_phone_loader = DataLoader(dataset=empty_phone_ds, batch_size=args.batch_size, shuffle=False, num_workers=4)
    empty_camera_loader = DataLoader(dataset=empty_camera_ds, batch_size=args.batch_size, shuffle=False, num_workers=4)

    # run program depending on the arguments
    if args.train == True:
        if args.model is not None:
            train_model(args, train_loader, val_loader, test_loader, train_df, valid_df, test_df, device)
        elif args.ensemble is not None:
            train_ensemble(args, train_loader, val_loader, test_loader, train_df, valid_df, test_df, device)
        else:
            print("No model specified")
    elif args.test == True:
        delete_stats()
        master_test(args, test_loader, test_df, device)
    elif args.test_aug == True:
        delete_stats()
        test_with_augmentations(args, test_df, device)
        eval_df = pd.read_csv('stats/preds.csv')
        master_evaluate_metrics(args, eval_df)
    # elif args.save_images == True:
    #     iterate_and_save(test)
    elif args.test_no_gt == True:
        # delete_stats()

        # log path as name of ensamble or model, if ensamble then each particular model is divided by '_' instead of comma and start with test_model_
        log_path = 'test_model_' + args.model + '.txt' if args.model is not None else 'test_ensemble_' + '_'.join(args.ensemble.split(',')) + '.txt'
        if args.model is not None:
            # arg model is without pth extension and model_ prefix, add it
            # model = torch.load(args.model + '.pth').to(device)
            model= torch.load(args.model+'.pth',map_location=device)
            #(model, test_loader, test_df, device, log_path
            model_name = args.model
            test_model_no_df(model_name, model, empty_loader, empty_df, device, log_path)
        elif args.ensemble is not None:
            model = form_ensemble_model(args.ensemble,  device)
            model_name = args.ensemble.replace(',', '_')
            test_model_no_df(model_name, model, empty_loader, empty_df, device, log_path)
        
    
    # elif args.evaluate == True:
    #     # run master evaluate function

        # eval_df = pd.read_csv(args.eval_csv)
        # master_evaluate_metrics(args, eval_df):
        
    


