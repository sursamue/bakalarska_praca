# Welcome to this repository
This repo represents the code for Classification and Detection of skin lesions.
# The structure of this repository

- [Classification](classification/classification_code) code can be found here, it contains the requirements and how-to for running the code.
- [Detection](detection) folder contains code and results for the detection task.
    - [Yolov7](detection/yolov7) contains the code for the detection task.
    - [Dermanude eval](detection/eval_dermanude) contains the code for the evaluation of the code on our dataset
- [CLI app](cli_app) contains code for the detection/classification system.
- [main.pdf](main.pdf) contains the report for this project.

