# CLI app for skin diagnosis

We provide a CLI app for skin diagnosis. You can use it to test your skin condition.
It will detect and classify the skin lesion in the image you provide.

## Usage
<!-- usage -->
```bash
python3 main.py -i <image_path> -c <confidence_threshold>
```
The app supports .jpg files.


The program will make an Image quality assessment (IQA). If the image is not of good quality, it will printout an ERROR message with the reason, and suggestion to improve the image quality, and exit.

If the image passes the IQA, it will printout a dataframe with the diagnosis result.
The image with the results such as type of the lesion and confidence score will be saved in the `./results` folder.



