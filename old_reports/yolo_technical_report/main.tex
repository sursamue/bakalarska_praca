\documentclass[conference]{IEEEtran}
\IEEEoverridecommandlockouts
% The preceding line is only needed to identify funding in the first footnote. If that is unneeded, please comment it out.


\ifCLASSOPTIONcompsoc
    \usepackage[caption=false, font=normalsize, labelfont=sf, textfont=sf]{subfig}
\else
\usepackage[caption=false, font=footnotesize]{subfig}
\fi

\usepackage{multirow}
\usepackage{todonotes}
\usepackage{float}
\usepackage[utf8]{inputenc}
% \usepackage[slovak, shorthands=off]{babel}

\usepackage{csquotes}

\usepackage[T1]{fontenc}
% \usepackage{cite}
\usepackage{amsmath,amssymb,amsfonts}
\usepackage{algorithmic}
\usepackage{graphicx}
\usepackage{textcomp}
\usepackage{xcolor}

%\usepackage{csquotes}
\usepackage[style=ieee]{biblatex}
\addbibresource{report_yolo.bib}

\pagestyle{plain} % page numbering


\usepackage[unicode=true, bookmarks=true,bookmarksnumbered=true,
bookmarksopen=false, breaklinks=false,pdfborder={0 0 0},
pdfpagemode=UseNone,backref=false,colorlinks=true] {hyperref}




\begin{document}

\title{Detection of pigmented skin lesions - technical report}

\author{\IEEEauthorblockN{Samuel Šúr}
}

\maketitle

\section{Introduction}

The purpose of this technical document is to outline the progress we have made 
in detecting pigmented skin lesions using the 3dBodyTex.v1 dataset. Detection of 
skin lesions on whole body images is a challenging task. The main reason for this
is the fact that the skin lesions are often small, they may be located in places
that are difficult to take a picture of, and they may be covered by clothing. 
The next problem is the quality of the whole-body images, and the availability of 
the datasets which are suitable for our task. 

Detection is a first step in the process of diagnosing skin cancer using machine learning,
after the images have been taken. Image segmentation (skin lesion border detection) is more accurate
than detection, which is better for this step.
After the detection, the next steps is
classification,
to determine the type of skin lesions. The last step to fully understand the type 
of skin lesion is to take longitudinal images of the lesion and compare them in time.


\section{Object detection}
The object detection is a computer vision task of localizing objects in images by predicting 
bounding boxes around them. The problem has been studied for many years and many Different
algorithms have been developed. The object detection in last years can be split into two main
periods: the first period before 2014 -  up until 2014, the object detection was based on methods 
not using neural networks; the second period after 2014 -  after 2014, the object detection was
based on methods using neural networks.

\subsection{Traditional detectors}

One of the most notable algorithms for object detection from the first period is the
Viola-Jones detector \cite{viola_rapid_2001}. % explain viola jones in simple words
The Viola-Jones detector consist of three main stages: the first stage is the Haar feature selection, 
the second stage is the AdaBoost training and the third stage is the cascade classifier.
The Haar-like feature selection is a method for extracting features from images. The Haar-like features
are rectangular patterns that capture the constrast between the pixels withing the feature area.
The algorithm applies these features to every possible location in the image and calculates the
difference between the sum of the pixels in the white area and the sum of the pixels in the black area. The 
next step is to train the classifier using the AdaBoost algorithm. The AdaBoost algorithm is used to select 
the most important features and to distinguish between object and non-object regions. The last step is to
combine the classifiers into a cascade of classifiers. The cascade of classifiers is used to reduce the
number of false positives.

The next algorithm for object detection from the first period is the HOG (Histogram of Oriented Gradients)
detector \cite{dalal_histograms_2005}. The HOG detector is based on the HOG descriptor. The HOG descriptor
is a feature vector that describes the shape of an object. The HOG descriptor is calculated by dividing the
image into cells and calculating the gradient histogram for each cell. The local gradient historgrams are 
then normalized and concatenated into a single feature vector. The HOG detector is trained using the SVM
(Support Vector Machine) algorithm. The SVM algorithm is used to find the optimal hyperplane that separates
the object and non-object regions.

The next algorithm for object detection from the first period is the DPM (Deformable Part Models) detector
The DPM detector was firstly introduced by Felzenszwalb et al. in 2009 \cite{felzenszwalb_discriminatively_2008}. 
It was developed as a successor of the HOG detector. It is based on method of breaking downt the objects into 
smaller parts and analyzing them separately. 

\subsection{Neural network detectors}

Neural network detectors can be divided into two groups: the first group is the one-stage detectors and the
secont group is the two-stage detectors. The one-stage detectors use a single CNN stage to directly predict
the objects in an mage. The two-stage detectors have an initial stage to propose regions of interest and 
a second stage to classify the regions of interest.
\subsection{Two stage detectors}

In 2014, R. Girshick et al. \cite{girshick_rich_2014} introduced RCNN (Regions with CNNs). They achieved 
state-of-the-art results on the PASCAL VOC 2007 dataset - 58.5\% mAP compared to 33.7\% mAP by previous 
state-of-the-art method \cite{felzenszwalb_discriminatively_2008}. 
The RCNN firstly uses selective search algorithm to generate regions proposals. 
For each region proposal a feature vector is extracted using the CNN. The feature 
vectors are then passed through a linear SVM classifier to determine the 
presence of an object as well as the class of the object it belongs to.
The RCNN's main drawback is the speed. The RCNN is very slow because it uses 2000
region proposals per image. RCNN is not suitable for real-time applications.

In next years, the new models were introduced improving the speed and accuracy of the object detection.
In 2014, K.He et al. \cite{he_spatial_2014} introduced the SPPNet (Single Shot Multibox Detector).
SPPNet is 24 - 102 times faster than RCNN. The mAP of SPPNet is 59.2\% on the PASCAL VOC 2007 dataset.
In 2015, R. Girshick \cite{girshick_fast_2015} introduced the Fast R-CNN followed by the Faster R-CNN 
\cite{ren_faster_2016}. The Faster R-CNN increased both, the speed  and the accuracy - 73.2\% mAP on the 
Pascal VOC 2007 dataset.

In 2017, T. Lin et al. \cite{lin_feature_2017} introduced the FPN (Feature Pyramid Networks). The FPN
is a top-down architecture with lateral connections. It generates a pyramid of 
feature maps with strong semantics at all levels from a single input image. 
The FPN serves as a backbone network for region proposal networks (RPNs).
FPN provides multi-scale feature pyramids where each level corresponds to a different scale of the input image,
allowing for better detection of objects at different sizes. The FPN is used as 
a backbone network for many object detection algorithms.


\subsection{One stage detectors}
\subsubsection{YOLO}
In 2016, J. Redmon et al. \cite{redmon_you_2016} introduced the YOLO (You Only Look Once) algorithm as 
a first one-stage detector. The YOLO model processes images at speed of 45 frames per second (fps).
The mAP of YOLO is 63.4\% on Pascal VOC 2007 dataset.
During the following years many improvements of the YOLO algorithm were introduced,
improving speed, accuracy, changing the architecture of the network.

\subsubsection{SSD}
In 2015, W. Liu et al. \cite{liu_ssd_2016} introduced the SSD (Single Shot MultiBox Detector) network.
The algorithm generates a set of default boxes over different aspect ratios and scales per feature map location.
The SSD network then predicts the class probabilities and bounding box offsets for each of the default boxes.

\subsubsection{RetinaNet}
In 2017, F. Lin et al. \cite{lin_focal_2018} introduced the RetinaNet network. The paper 
introduces a new loss function for object detection, the Focal Loss. The Focal Loss is a 
generalization of the cross-entropy loss. The Focal Loss is used to address the problem of
the class imbalance between the foreground and background classes. 

\subsubsection{CornerNet}
In 2019, H. Law et al. \cite{law_cornernet_2019} introduced the CornerNet network. The CornerNet network
brings a new approach to object detection. The CornerNet detects objects as a set of keypoints - top left and
bottom right corners of the bounding box. By this method there is no need to use anchor boxes. CornerNet
achieves state-of-the-art results on the COCO dataset.

\subsubsection{CenterNet}
In 2019, X. Zhou et al. \cite{zhou_objects_2019} introduced the CenterNet network. The CenterNet network considers
objects to be the center points of the bounding boxes. The network then regresses other parameters such as size, orientation, 
pose, 3D location.

\subsubsection{DETR}
In 2020, M. Carion et al. \cite{carion_end--end_2020} introduced the DETR network (DEtection TRansformer). 
The DETR network uses transformer architecture which is a new state-of-the-art type of neural network architecture.
Their approach does not use Non-Maximum Suppression (NMS) and anchor boxes. 

\section{YOLO}
As mentioned in the previous section, YOLO network has been changed and improved during the years.
In this section we shall describe various iterations of the YOLO network.

\subsection{YOLOv1}
Yolov \cite{redmon_you_2016} process of detection starts with dividing the input 
image into $S\times S $ grid cells. Each grid cell predicsts $B$ bounding boxes and confidence 
scores for each of the bounding boxes. It also predicts $C$ class probabilities for each bounding box.
These predictions form a tensor described as $S \times S \times(B \times 5 + C)$. Each bounding box
is described by 5 variables: $x, y, w, h, c$. The variables $x, y$ are the coordinates of the center of the
bounding box. The variables $w, h$ are the width and height of the bounding box. The variable $c$ is the
confidence score of the bounding box. Low confidence score bounding boxes are filtered out using the predefined 
confidence threshold. The bounding boxes are then sorted by their confidence score and the non-maximum suppression
algorithm is applied to remove overlapping bounding boxes.
Yolov1 network consists of 2 parts - the backbone network and the head. The backbone network is 
a convolutional neural network that extracts features from the input image. The head of the network
is a fully connected layer that turns the features extracted by the backbone network into the prediction tensor.
The backbone network is a 24-layer convolutional neural network. 

The authors of the YOLOv1 introduced also the YOLOv2 (Yolov9000) \cite{redmon_yolo9000_2016}  and YOLOv3 networks \cite{redmon_yolov3_2018-1}.


\subsection{Yolov3}
For a backbone network of the YOLOv3 algorithm the authors used the decided to use the Darknet-53, while YOLOv2 uses the Darknet-19. The latter uses 19 convolutional
layers, while the former uses 53 convolutional layers. YOLOv3 further distinguishes itself from YOLOv2 and YOLOv1 by using  similar architecture to the FPN. The
format of the predictions is also different. YOLOv3 predicts three bounding boxes per grid cell at different scales. The prediction tensors are described as 
$ S \times S\times (3 \times 5+ C)$, then $(2 \cdot S) \times (2 \cdot S) \times (3 \times 5 + C)$ and finally $(4 \cdot S) \times (4 \cdot S) \times (3 \times 5 + C)$.
This can be combined and written as $ N  \times N  \times (3 \times 5 + C)$.
The YOLOv3 increased detection accuracy for small objects. \cite{redmon_yolov3_2018}

\subsection{YOLOv4}
In 2020, Alexey Bochkovskiy et al. introduced the YOLOv4 algorithm. It is the first 
YOLO version that is not crated by the authors of the original YOLO algorithm. The YOLOv4 algorithm uses the CSPDarknet53 backbone network. The CSPDarknet53
is a modified version of the Darknet53 network. The second part of the YOLOv4 network is Neck. The Neck consists of 
the SPP (Spatial Pyramid Pooling) block and the PAN (Path Aggregation Network) block. The SPP block is used to
extract features from the input image at different scales. The PAN block is used for parameter aggregation from 
different backbone levels. The last part - the YOLOv3 head. \cite{bochkovskiy_yolov4_2020}

\subsection{YOLOv7}
In 2022, Alexey Bochkovskiy et al. upgraded the YOLOv4 which yielded the YOLOv7 algorithm.
The YOLOv7 introduce Extended Efficient Layer Aggregation Network (EELAN).
This paper also introduces the concept of Trainable Bag of Freebies (TBoF) which is a reparameterization algorithm. \cite{wang_yolov7_2022}

\section{Pigmented skin lesion detection}

Most of the current research in the field of computerized diagnosis of skin lesions
uses images of single skin lesion rather than whole body images \cite{celebi_dermoscopy_2019}.
Tim K. Lee \cite{lee_counting_2005} proposed a method for counting the skin lesions of the 
wide area photo of the back. The method involves pre-processing the image, 
applying mean shift filtering and region-growing techniques to identify candidate moles.
\todo[inline]{[poznamka pre seba]todo, dopisat chybajucu literaturu}

In 2014, K. Korotkov \cite{korotkov_automatic_2014} used the MSER (Maximally Stable Extremal Regions) algorithm
for detection of PSLs. MSER \cite{matas_robust_nodate} is a feature extraction technique which works by identifying
regions of an image which are stable under small intensity variations.
The method requires a lot of manual tuning of parameters and preprocessing of the images.

M. Zhao \cite{zhao_skin3d_2022} uses the Faster RCNN network for detection of PSLs using
the 3dBodyTex.v1 dataset which consists of whole body textures.

Ahmedt-Aristizabal et al. \cite{ahmedt-aristizabal_monitoring_2022} uses Scaled-YOLOv4 network for detection of PSLs.
It is a modification of the YOLOv4 network. The authors of the paper use the YOLOv4 network for detection of PSLs.







\section{Problem analysis}
\subsection{Dataset}


We acquired the 3dBodyTex.v1 dataset \cite{saint_3dbodytex_2018} which consists of 400 scans
of 200 people (100 males, 100 females) in in close-fitting clothing in
various position. Moreover, each scan is accompanied by high-quality
texture which are suitable for our task of detection of pigmented skin
lesions on whole body images.

In this work we use the annotations Skin3D\footnote{ https://github.com/jeremykawahara/skin3d
} introduced by Mengliu Zhao,
Jeremy Kawahara et al. \cite{zhao_skin3d_2022}. Their work provides over 25 000 manually
annotated skin lesions which are visible on textures of 3dBodyTex.v1
dataset. Their work divided the 3dBodyTex.v1 dataset into 3 groups -
train set, validation set and test set. Test has been simultaneously
annotated by 3 different annotators and is divided into 3 subsets
accordingly.

\begin{figure}[htbp]
    \centering
  \subfloat[Texture image from test dataset with bounding boxes by 3 annotators distinguished by colors.\label{fig:1a}]{%
       \includegraphics[width=0.8\linewidth]{figures/061_bbox.png}}
    \\
  \subfloat[Close up on image texture from  \ref{fig:1a}\label{fig:1b}]{%
        \includegraphics[width=0.4\linewidth]{figures/061_bbox_part.png}}
  \caption{Texture images from 3dBodyTex.v1 dataset with bounding boxes from Skin3D dataset.}
  \label{fig:1}
\end{figure}









%\begin{figure}[htbp]
%    \includegraphics[width=1\linewidth]{figures/performance.png}
%    \caption{State of art YOLO object detectors, their performance and inference time}
%    \label{fig:yolo_performance}
%  \end{figure}


\section{Our approach}
\subsection{Dataset preprocessing}

Each texture image has 4096x4096 pixels. As you can see from ``Fig.~\ref{fig:1}'',
the quality of individual skin lesions is already low, therefore
rather than resizing image, we have choosen an approach to divide each
texture image into 49 overlapping patches of size 800x800.

The method of overlapping patches is a commonly used technique in
neural networks for processing large images or datasets, and it offers
several advantages over alternative methods. One major advantage is
that it allows for better utilization of available computational
resources by breaking down the input data into smaller, overlapping
patches, which reduces the memory and computation requirements of the
network. Using overlapping patches rather than just slicing the image
into non-overlapping patches can also help reduce the impact of crops
on boundaries that can arise when processing large images. This can
lead to more accurate and reliable results.

\subsection{Training parameters}\label{sec:training}
For training of neural network on our dataset, we use Yolov7 with
official pretrained weights.
In this work we rely on implementation of YOLOv7 by Chris Hughes\footnote{ https://github.com/Chris-hughes10/Yolov7-training
}. His
implementation is far more adjustable on custom datasets than original version\footnote{ https://github.com/WongKinYiu/yolov7
}. It is
easier to use with custom datasets.
To optimize the training process, we utilized Adam optimizer (we did not explore training process with other optimizers) with a
base learning rate of 0.001 and a weight decay of 0.0005 (we did not explore other weight decays). We tried different 
learning rates, both above and below 0.001, but we found that learning rate ~0.001 
works best for our dataset.
We implemented a method of Non-maximum suppression with a Intersectoin over union (IoU) threshold of
0.01 to filter out overlapping bounding boxes.
Additionally,
we evaluated the model's performance using mean Average Precision
(mAP) or Average Precision (AP), as we are detecting only one class, with an IoU threshold of 0.5, as
described in the COCO competition \footnote{https://cocodataset.org/\#detection-eval}. 
We applied early stopping with a
patience of 5 epochs, and the training would stop if the mAP on the
validation dataset did not improve by 0.0001 for five consecutive
epochs. Lastly, we utilized a Cosine annealing learning rate scheduler, 
as described in this paper \cite{loshchilov_sgdr_2017},
but without restarts and with a warm-up period of 2 epochs.
We tried to use also stable learning rate, as well as Cosine learnign rate 
without warm-up period, but the results were better with warm up period in Cosine learning rate.
The changes of 
learning rate during training are shown in ``Fig.~\ref{fig:lr}''
\begin{figure}[htbp]
  \includegraphics[width=1\linewidth]{figures/lr.pdf}
  \caption{Learning rate during training}
  \label{fig:lr}
\end{figure}


\subsection{Results}
After the best model was trained using the parameters described in
``Sec.~\ref{sec:training}'', we evaluated its performance on the test dataset.
The model stopped training in epoch 10 after 5 epochs with no improvement in mAP.
The evaluation metrics used on the test dataset are different 
from the ones used during training. We used Non-maximum suppression with IoU threshold
of 0.01 to filter out overlapping predictions. 
On ``Fig.~\ref{fig:precisionxrecall}'' we can see the Precision x Recall curves of skin lesions predicted by our model for test datasets
by each annotator. The Precision x Recall curves are calculated in Pascal VOC 2012 manner, which means that
the Precision x Recall curve is calculated for each confidence threshold with IoU threhsold of 0.5.

We calculate AP and AR with 2 different confidence thresholds \textit{c}, which removes low confidence predictions as may be seen 
in ``Tab. ~\ref{tab:results1}''. We also provide the results computed using Pascal VOC 2012 metric - AP (IoU = 0.5) through all confidence thresholds ``Tab. ~\ref{tab:results2}''.

The Average Precision (AP)  in Pascal VOC 2012 manner
is computed as the Area under the Precision x Recall curve \cite{padilla_survey_2020}\cite{padilla_comparative_2021}. 


\begin{figure}[htbp]
  \centering
\subfloat[Precision x Recall curve for annotations A1\label{fig:1a}]{%
     \includegraphics[width=1\linewidth]{figures/graphs_03_09/graphs/A1/results/skin_lesion.pdf}}
  \\
  \subfloat[Precision x Recall curve for annotations A2\label{fig:1b}]{%
       \includegraphics[width=1\linewidth]{figures/graphs_03_09/graphs/A2/results/skin_lesion.pdf}}
  \\
  \subfloat[Precision x Recall curve of for annotations A3\label{fig:1c}]{%
       \includegraphics[width=1\linewidth]{figures/graphs_03_09/graphs/A3/results/skin_lesion.pdf}}
\caption{Precision x Recall curves for each annotator.}
\label{fig:precisionxrecall}
\end{figure}


% generate table in a form like this
% annotator ||  \multirow(c = 0.25 & iou = 0.5 )| \multirow(c = 0.5 & iou = 0.5) | pascal voc 2012 AP (iou = 0.5)

\begin{table}[htbp]
  \centering
  
  \caption{Results of the model on test dataset using COCO metrics (AP and AR (Average Precision and Average Recall).}
  \begin{tabular}{cccccc}
  \hline
  \multicolumn{1}{c||}{\multirow{2}{*}{Annotator}}         &        \multicolumn{2}{c|}{$AP_{50}$ }         &        \multicolumn{2}{c}{AR}         \\  \cline{2-5}
  \multicolumn{1}{c||}{{}}                                   & \multicolumn{1}{c|}{$c = 0.25$} & \multicolumn{1}{c|}{$c = 0.5$} & \multicolumn{1}{c|}{$c = 0.25$} & \multicolumn{1}{c}{$c = 0.5$}  \\ \hline
  \multicolumn{1}{c||}{A1}                  & \multicolumn{1}{c|}{0.26\%} & \multicolumn{1}{c|}{0.14\%} & \multicolumn{1}{c|}{12.65\%} & \multicolumn{1}{c}{4.34\%}  \\ \hline
  \multicolumn{1}{c||}{A2}     & \multicolumn{1}{c|}{47.07\%} & \multicolumn{1}{c|}{17.94\%}               & \multicolumn{1}{c|}{45.78\%}  & \multicolumn{1}{c}{16.19\%} \\ \hline

  \multicolumn{1}{c||}{A3}                  & \multicolumn{1}{c|}{26.68\%} & \multicolumn{1}{c|}{16.02\%} & \multicolumn{1}{c|}{43.27\%} & \multicolumn{1}{c}{21.10\%} \\ 
  \end{tabular}
  \label{tab:results1}
  \end{table}


\begin{table}[htbp]
  \centering
  \caption{Average Precision (AP) (Pascal VOC 2012)} 
  \begin{tabular}{c|c}
  \hline
  Annotator & Average Precision (AP) \\ \hline
  A1 & 0.65\% \\
  A2 & 63.10\% \\
  A3 &  33.32\% \\
    
  \end{tabular}
  \label{tab:results2}

  \end{table}
% \begin{table}[htbp]
%   \caption{Average Precision (AP) for each annotations}

% \centering
% \begin{tabular}{c|c|c|c}
% \hline
% \multirow{2}{*}{Annotator} & \multicolumn{3}{c}{Average Precision (AP)} & \multicolumn{Average Recall (AR)}{c}{Average Recall (AR)}\\
% %  & AP_{50}^{25}& AR_{25} &AP_{50}^{50} & AP_{50}^{VOC2012}\\
% % \hline
% % A1 & 0.52\% \\
% % A2 & 57.49\% \\
% % A3 & 21.32\% \\
  
% \end{tabular}
% \label{tab:ap}
% \end{table}

\begin{figure}[htbp]
  \includegraphics[width=1\linewidth]{figures/A2_pred/000_6.png}
  \caption{Prediction (red) and ground truth (blue) bounding boxes for annotations A2, with confidence scores (red text)}
  \label{fig:pred_vs_gt_A2}
\end{figure}

On the ``Fig.~\ref{fig:pred_vs_gt_A2}'' we can see the predictions of the model and ground truth bounding boxes of annotator A2.


\section{Comparison with state-of-the-art methods}\label{sec:comparison}

\subsection{}
The most similar work to ours is the work of J. Kawahara et al. \cite{zhao_skin3d_2022}.
Their work uses the same dataset as ours. For detecting the skin lesions, they use Faster RCNN. They do not 
use overlapping pathches, but they use a sliding window approach. We must note that we 
use overlapping pathces but then we do not consider the overlapping regions as one, but as two different regions
Average Precision's, as stated in paper by J. Kawahara et al. are following:
\begin{itemize}
    \item A1: 8\%
    \item A2: 59\%
    \item A3: 56\%
\end{itemize}
Their work uses the same metric for evaluation on test dataset as ours. We can see
that our work reaches better score in case of annotator A2 but worse in case of annotator A3 and A1.
Their work
proposes another metric for evaluation, which is called overlapping centroid. This metric
considers bounding box to be True Positive if the predicted bounding box overlaps with the center 
of the ground truth bounding box. We do not use this metric.


\subsection{}
Another work similar to ours is the paper by Ahmed-Aristizabal et al. \cite{ahmedt-aristizabal_monitoring_2022}.
As mentioned earlier, they use similar model to ours - Scaled YOLOv4. They also use
3dBodyTex.v1 dataset among others with custom made ground-truth data. They split the dataset images into overlapping patches
of a size 608x608. They use Soft-NMS to suppress bounding boxes \cite{bodla_soft-nms_2017}. The use of confidence
score to filter out bounding boxes is mentioned, but not specified in the paper. Their results are $AP_{50}\approx$ 92\%. Although
this method differs from our approach in terms of ground-truth data, it introduces high precision results.




\section{Conclusion}\label{sec:conclusion}
In this work, we proposed a method for detecting skin lesions in textures of skin lesions using 3dBodyTex.v1 dataset.
We used Yolov7 as a model, which is the latest addition to the YOLO family of object detectors.
We used overlapping patches to train the model.
We used the same dataset as the work of J. Kawahara et al. \cite{zhao_skin3d_2022}. Our work 
reaches comparable score in some cases with the state-of-the-art methods,
 but predicts bounding boxes with low confidence scores compared to
the other state-of-the-art methods.





\printbibliography

\end{document}