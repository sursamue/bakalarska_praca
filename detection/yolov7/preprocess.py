import pandas as pd
import numpy as np
import cv2
import os
import re
import ast
import sys

from timeit import default_timer

from pathlib import Path

from PIL import Image
from torch.utils.data import Dataset


from functools import partial

import albumentations as A
from albumentations.pytorch.transforms import ToTensorV2

import torch
import torchvision

from torchvision.models.detection.faster_rcnn import FastRCNNPredictor
from torchvision.models.detection import FasterRCNN
# from torchvision.models.detection.rpn import AnchorGenerator

from torch.utils.data import DataLoader, Dataset
from torch.utils.data.sampler import SequentialSampler

from matplotlib import pyplot as plt
from matplotlib import patches

from tqdm import tqdm

from multiprocessing import Pool
from typing import List

import pickle



# DIR_DATASET = '/kaggle/input/airbus-aircrafts-sample-dataset/images'
# DIR_ANNOTATIONS = '/kaggle/input/annotations-better-better/annotations_better.csv'

# PATH_DIR_DATASET = Path('/kaggle/input/airbus-aircrafts-sample-dataset/images')


sys.path.insert(0, '../vision/references/detection') # good official library, easy for evaluation
from engine import train_one_epoch, evaluate

sys.path.insert(0, os.path.abspath(os.path.join(os.getcwd(), '/ssd/temporary/sursamue/skin/skin3d')))
from skin3d.bodytex import BodyTexDataset

from skin3d.visualize import embed_annotatations
from skin3d.visualize import embed_box_borders

import dataset_wrapper as dw


def load_bodytex_dataset():
    # File path of the bodytex CSV.
    bodytex_csv = '/ssd/temporary/sursamue/skin/skin3d/data/3dbodytex-1.1-highres/bodytex.csv'
    bodytex_df = pd.read_csv(bodytex_csv, converters={'scan_id': lambda x: str(x)})
    bodytex = BodyTexDataset(
        df=bodytex_df, 
        dir_annotate='/ssd/temporary/sursamue/skin/skin3d/data/3dbodytex-1.1-highres/annotations',
        dir_multi_annotate='/ssd/temporary/sursamue/skin/skin3d/data/3dbodytex-1.1-highres/multiple_annotators',
        dir_textures='/ssd/temporary/sursamue/skin/3dbodytex-1.1-highres'
        )
    return bodytex


class BodyTexWrapper(Dataset):

    """
    This class acts as an adaptor and wrapper at the same time
    It wraps BodyTexDataset into easy to ready class
    It is and adaptor for the BodyTexSlices, which 
    is suitable for training.
    function __getitem__ returns image, annotations, index
    """

    def __init__(self, body_tex_dataset, partition="train", annotator = None, custom_len = None):

        self.custom_len = custom_len

         # define dataset
        self.body_tex_dataset = body_tex_dataset
        # list of all images in partition
        self.image_list = self.body_tex_dataset.annotated_scan_ids(partition)
        if custom_len!=None:
            self.image_list = self.image_list[:self.custom_len]
        # df for all images in partition
        self.partition_df=self.body_tex_dataset.annotated_samples_in_partition(partition)
        if custom_len!=None:
            self.image_list = self.image_list[:self.custom_len]
            self.partition_df = self.partition_df.loc[:self.custom_len,:]
        # for test partition we need to provide annotator ['A1', 'A2', 'A3']
        self.annotator = annotator


    def __len__(self):
        # get length of the dataset
        return len(self.image_list)

    def __getitem__(self, index):
        # get scan_id
        scan_id = self.image_list[index]
        # for visualisation

        image = np.asarray(self.body_tex_dataset.texture_image(scan_id), dtype=np.uint8).copy()


        # do stuff


        if self.annotator == None:
            annotations = self.body_tex_dataset.annotation(scan_id)
        else:
            annotations = self.body_tex_dataset.annotation(scan_id = scan_id, annotator = self.annotator)

        class_labels = np.ones(len(annotations), dtype='int32') # we are detecting only skin lesions, not classyfying them

        return image, annotations, class_labels, index

def calculate_slice_bboxes(
    image_height: int = 4096,
    image_width: int = 4096,
    slice_height: int = 800,
    slice_width: int = 800,
    overlap_height_ratio: float = 0.2,
    overlap_width_ratio: float = 0.2,
) -> List[List[int]]:
    """
    Given the height and width of an image, calculates how to divide the image into
    overlapping slices according to the height and width provided. These slices are returned
    as bounding boxes in xyxy format.

    :param image_height: Height of the original image.
    :param image_width: Width of the original image.
    :param slice_height: Height of each slice
    :param slice_width: Width of each slice
    :param overlap_height_ratio: Fractional overlap in height of each slice (e.g. an overlap of 0.2 for a slice of size 100 yields an overlap of 20 pixels)
    :param overlap_width_ratio: Fractional overlap in width of each slice (e.g. an overlap of 0.2 for a slice of size 100 yields an overlap of 20 pixels)
    :return: a list of bounding boxes in xyxy format
    """

    slice_bboxes = []
    y_max = y_min = 0
    y_overlap = int(overlap_height_ratio * slice_height)
    x_overlap = int(overlap_width_ratio * slice_width)
    while y_max < image_height:
        x_min = x_max = 0
        y_max = y_min + slice_height
        while x_max < image_width:
            x_max = x_min + slice_width
            if y_max > image_height or x_max > image_width:
                xmax = min(image_width, x_max)
                ymax = min(image_height, y_max)
                xmin = max(0, xmax - slice_width)
                ymin = max(0, ymax - slice_height)
                slice_bboxes.append([xmin, ymin, xmax, ymax])
            else:
                slice_bboxes.append([x_min, y_min, x_max, y_max])
            x_min = x_max - x_overlap
        y_min = y_max - y_overlap
    return slice_bboxes


def create_image_slices_df(
    wrapper_ds,
    slice_height: int = 800,
    slice_width: int = 800,
    overlap_height_ratio: float = 0.2,
    overlap_width_ratio: float = 0.2,
    min_bbox_area: float = 0.1,
    min_bbox_visibility: float = 0.4, #TODO: toto alebo minbboxarea je hovadina opravit
    ):


    
    # get df of given ds
    # TODO: what to do with test???????? you know it is a lil different
    temp_df = wrapper_ds.partition_df.copy()
    

    # temp_df["slices"] = temp_df.apply(
    #     lambda row: calculate_slice_bboxes(
    #         # row.image_height,
    #         # row.image_width, #good if various image sizes accross dataset (not needed for us), for this purpose temp_df should include also sizes of each image
    #         slice_height,
    #         slice_width,
    #         overlap_height_ratio,
    #         overlap_width_ratio,
    #     ),
    #     axis=1,
    # )
    slices_list = calculate_slice_bboxes(   
            image_height = 4096,
            image_width =4096,
            slice_height = slice_height,
            slice_width = slice_width,
            overlap_height_ratio = overlap_height_ratio,
            overlap_width_ratio = overlap_width_ratio)
    # temp_df['slices'] = temp_df.apply(lambda row:slices_list, axis=1)
    temp_df.loc[:, 'slices'] = [slices_list for i in temp_df.index]
  
    
    slices_row_df = (
        temp_df[["scan_id", "slices"]]
        .explode("slices")
        .rename(columns={"slices": "slice"})
    )

    slices_row_df = pd.DataFrame(
        slices_row_df.slice.tolist(),
        columns=["xmin", "ymin", "xmax", "ymax"],
        index=slices_row_df.scan_id,
    ).reset_index()


    
    image_slices_df = pd.merge(
        slices_row_df,
        temp_df[["scan_id"]],
        how="inner",
        on="scan_id",
    )


    image_slices_df[["bboxes","has_annotation"]] = image_slices_df.apply(
        partial(
            contains_object,
            wrapper_ds,
            min_bbox_area=min_bbox_area,
            min_bbox_visibility=min_bbox_visibility,
        ),
        axis=1,
    )
    # get slice id for every scan_id start from 0
    image_slices_df["slice_id"] = image_slices_df.groupby("scan_id").cumcount()

    image_slices_df['bboxes'] = image_slices_df['bboxes'].apply(lambda x: x if len(x) > 0 else [(np.nan, np.nan, np.nan, np.nan)])
    image_slices_df_with_expanded_bboxes = image_slices_df.explode("bboxes")

    image_slices_df_with_expanded_bboxes.reset_index(inplace=True, drop = True)
    
 
    image_slices_df_with_expanded_bboxes[["x", "y", "x2", "y2"]] = pd.DataFrame(
        image_slices_df_with_expanded_bboxes.bboxes.tolist()
    )
    image_slices_df_with_expanded_bboxes.fillna(value=np.nan, inplace=True)
    image_slices_df_with_expanded_bboxes = image_slices_df_with_expanded_bboxes.drop(
        "bboxes", axis=1
    )   
    image_slices_df_with_expanded_bboxes["class_id"] = np.where(
    image_slices_df_with_expanded_bboxes["has_annotation"], 1, np.nan
)

    


    image_slices_df_with_expanded_bboxes.reset_index(inplace=True)
    # image_slices_df_with_expanded_bboxes.rename(columns={"index": "slice_id"}, inplace=True)




    image_slices_df.reset_index(inplace=True)
    #image_slices_df.rename(columns={"index": "slice_id"}, inplace=True)


    # reorganize columns of image_slices_df and drop bbox column
    image_slices_df = image_slices_df[["index", "scan_id", "slice_id", "xmin", "ymin", "xmax", "ymax",  "has_annotation"]]
    image_slices_df_with_expanded_bboxes = image_slices_df_with_expanded_bboxes[["index", "scan_id", "slice_id", "xmin", "ymin", "xmax", "ymax", "x", "y", "x2", "y2", "class_id", "has_annotation"]]

    return image_slices_df, image_slices_df_with_expanded_bboxes


def contains_object(
    wrapper_ds, image_slice_row, min_bbox_visibility=0.4, min_bbox_area=0.1
):

    #TODO: asi prerobt bodytexwrapper aby sa dal indexovat pomocou scan_id, 
    #takze by sa to dalo napisat ako wrapper_ds[image_slice_row.scan_id]
    # totizto by to vyriesilo aj pre testovacie data
    # zatial to nechavam takto
    # taktiez je wrapper_ds zbytocny a nulitny
    body_tex_dataset = load_bodytex_dataset()

    # TODO: rewrite this for test
    if wrapper_ds.annotator != None:
        xyxy_bboxes = body_tex_dataset.annotation(scan_id = image_slice_row.scan_id, annotator = wrapper_ds.annotator)[['x', 'y', 'x2', 'y2']].values
    else:
        xyxy_bboxes = body_tex_dataset.annotation(scan_id = image_slice_row.scan_id)[['x', 'y', 'x2', 'y2']].values
    # create xyxy_boxes containing only x, y, x2, y2 columns
    #xyxy_boxes = pd.DataFrame(xyxy_bboxes, columns=['x', 'y', 'x2', 'y2'])
   


    # xyxy_bboxes = annotations_df.query("scan_id == @image_slice_row.scan_id")[
    #     ["xmin", "ymin", "xmax", "ymax"]
    # ].values
    
    
    slice_bbox = image_slice_row[["xmin", "ymin", "xmax", "ymax"]].values


    transforms = A.Compose(
        [A.Crop(*slice_bbox)],
        bbox_params=A.BboxParams(
            format="pascal_voc",
            min_visibility=min_bbox_visibility,
            min_area=min_bbox_area,
            label_fields=["labels"],
            ),
        )
    
    
    # once i had a porblem with overflow in the annotations
    # so i added try except
    # may be deleted in the future
    image_height=4096 #TODO this is hardcoded, ma be changed in future, but for now, all of our images are of this size
    image_width=4096
    transformed = transforms(
        # use dummy array to avoid loading image and labels
        image=np.ones((image_height, image_width, 3)), # originally it was like this (image_slice_row.image_height, image_slice_row.image_width, 3
        labels=np.ones(len(xyxy_bboxes)),
        bboxes=xyxy_bboxes,
    )
      
    transformed_boxes = transformed["bboxes"]



    return pd.Series([transformed_boxes, len(transformed_boxes) > 0])

def save_im(im, path):
    
    color = [0,0,255]
    pad = 2

    for i, row in boxes.iterrows():
        
        x = int(row['x'])
        y = int(row['y'])
        x2 = int(row['x2'])
        y2 = int(row['y2'])
        width = int(x2-x)
        height = int(y2-y)
    im = Image.fromarray(im).convert('RGB')
    im.save(path, 'PNG')

# function to crop and save images, as input it takes the path and the dataframe 
def crop_images_and_save(bodytex, folder_path, df):    
    """ This function crops the images and saves them in the folder_path.
    Args:
        bodytex: BodyTex dataset
        folder_path: path to the folder where the images will be saved
        df: dataframe containing the information about the images
    """

    # get list of all scan_ids
    scan_ids = df.scan_id.unique()
    # iterate over the scan_ids
    for scan_id in tqdm(scan_ids, total = len(scan_ids)):
        full_img = np.asarray(bodytex.texture_image(scan_id), dtype=np.uint8).copy()
        scan_id_df = df.loc[df['scan_id']==scan_id]

        # iterate over the rows of the dataframe
        for i, row in tqdm(scan_id_df.iterrows(), total = len(scan_id_df)):
            slice_bbox = row[["xmin", "ymin", "xmax", "ymax"]].values
            transforms= A.Crop(*slice_bbox)
            cropped_im = transforms(image=full_img)['image']
            cropped_im = Image.fromarray(cropped_im).convert('RGB')
            cropped_texture_path = os.path.join(folder_path, row['scan_id'] + '_' + str(row['slice_id']) + '.png')


            # save the image
            cropped_im.save(cropped_texture_path, 'PNG')

def texture_image(self, scan_id: str) -> Image:
        """Return the texture image corresponding to `scan_id`."""
        img_path = self.texture_filepath(scan_id=scan_id)
        img = Image.open(img_path).convert("RGB")
        return img

if __name__ == "__main__":
    bodytex = load_bodytex_dataset()
    
    train_ds = BodyTexWrapper(bodytex, partition='train')
    valid_ds = BodyTexWrapper(bodytex, partition='valid')
    test_ds_A1 = BodyTexWrapper(bodytex, partition='test', annotator= 'A1')
    test_ds_A2 = BodyTexWrapper(bodytex, partition='test', annotator= 'A2')
    test_ds_A3 = BodyTexWrapper(bodytex, partition='test', annotator= 'A3')


    print('Creating image slices')
    train_dataset_slices, train_dataset_slices_expanded= create_image_slices_df(train_ds)
    valid_dataset_slices, valid_dataset_slices_expanded = create_image_slices_df(valid_ds)
    
    test_dataset_slices_A1, test_dataset_slices_expanded_A1 = create_image_slices_df(test_ds_A1)
    test_dataset_slices_A2, test_dataset_slices_expanded_A2 = create_image_slices_df(test_ds_A2)
    test_dataset_slices_A3, test_dataset_slices_expanded_A3 = create_image_slices_df(test_ds_A3)
    print('Image slices created')


    print(test_dataset_slices_expanded_A1.head())
    print(test_dataset_slices_expanded_A2.head())
    print(test_dataset_slices_expanded_A3.head())

    input("Press Enter to continue...")

    



    # print(train_dataset_slices.head())
    # print(train_dataset_slices_expanded.head())
    print('Converting to csv')
    # train_dataset_slices.to_csv('/mnt/datagrid/Medical/skin/3dbodytex-1.1-highres_preprocessed_v2/train/train_dataset_slices.csv')
    # valid_dataset_slices.to_csv('/mnt/datagrid/Medical/skin/3dbodytex-1.1-highres_preprocessed_v2/valid/valid_dataset_slices.csv')
   
    train_dataset_slices.to_csv('/ssd/temporary/sursamue/skin/3dbodytex-1.1-highres_preprocessed_v2/train/train_dataset_slices.csv')
    valid_dataset_slices.to_csv('/ssd/temporary/sursamue/skin/3dbodytex-1.1-highres_preprocessed_v2/valid/valid_dataset_slices.csv')
    test_dataset_slices_A1.to_csv('/ssd/temporary/sursamue/skin/3dbodytex-1.1-highres_preprocessed_v2/test/A1/test_dataset_slices.csv')
    test_dataset_slices_A2.to_csv('/ssd/temporary/sursamue/skin/3dbodytex-1.1-highres_preprocessed_v2/test/A2/test_dataset_slices.csv')
    test_dataset_slices_A3.to_csv('/ssd/temporary/sursamue/skin/3dbodytex-1.1-highres_preprocessed_v2/test/A3/test_dataset_slices.csv')

    # train_dataset_slices_expanded.to_csv('/mnt/datagrid/Medical/skin/3dbodytex-1.1-highres_preprocessed_v2/train/train_dataset_slices_expanded.csv')
    # valid_dataset_slices_expanded.to_csv('/mnt/datagrid/Medical/skin/3dbodytex-1.1-highres_preprocessed_v2/valid/valid_dataset_slices_expanded.csv')
    train_dataset_slices_expanded.to_csv('/ssd/temporary/sursamue/skin/3dbodytex-1.1-highres_preprocessed_v2/train/train_dataset_slices_expanded.csv')
    valid_dataset_slices_expanded.to_csv('/ssd/temporary/sursamue/skin/3dbodytex-1.1-highres_preprocessed_v2/valid/valid_dataset_slices_expanded.csv')
    test_dataset_slices_expanded_A1.to_csv('/ssd/temporary/sursamue/skin/3dbodytex-1.1-highres_preprocessed_v2/test/A1/test_dataset_slices_expanded.csv')
    test_dataset_slices_expanded_A2.to_csv('/ssd/temporary/sursamue/skin/3dbodytex-1.1-highres_preprocessed_v2/test/A2/test_dataset_slices_expanded.csv')
    test_dataset_slices_expanded_A3.to_csv('/ssd/temporary/sursamue/skin/3dbodytex-1.1-highres_preprocessed_v2/test/A3/test_dataset_slices_expanded.csv')

    print('Slicing images')
    # crop_images_and_save(bodytex, '/mnt/datagrid/Medical/skin/3dbodytex-1.1-highres_preprocessed_v2/train', train_dataset_slices)
    # crop_images_and_save(bodytex, '/mnt/datagrid/Medical/skin/3dbodytex-1.1-highres_preprocessed_v2/valid', valid_dataset_slices)
    crop_images_and_save(bodytex, '/ssd/temporary/sursamue/skin/3dbodytex-1.1-highres_preprocessed_v2/train', train_dataset_slices)
    crop_images_and_save(bodytex, '/ssd/temporary/sursamue/skin/3dbodytex-1.1-highres_preprocessed_v2/valid', valid_dataset_slices)
    crop_images_and_save(bodytex, '/ssd/temporary/sursamue/skin/3dbodytex-1.1-highres_preprocessed_v2/test/A1', test_dataset_slices_A1)
    crop_images_and_save(bodytex, '/ssd/temporary/sursamue/skin/3dbodytex-1.1-highres_preprocessed_v2/test/A2', test_dataset_slices_A2)
    crop_images_and_save(bodytex, '/ssd/temporary/sursamue/skin/3dbodytex-1.1-highres_preprocessed_v2/test/A3', test_dataset_slices_A3)


