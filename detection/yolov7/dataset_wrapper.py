
import os
import sys
import pandas as pd

sys.path.insert(0, os.path.abspath(os.path.join(os.getcwd(), '/datagrid/Medical/skin/skin3d')))
from skin3d.bodytex import BodyTexDataset

def load_bodytex_dataset():
    # File path of the bodytex CSV.
    bodytex_csv = '/datagrid/Medical/skin/skin3d/data/3dbodytex-1.1-highres/bodytex.csv'
    bodytex_df = pd.read_csv(bodytex_csv, converters={'scan_id': lambda x: str(x)})
    bodytex = BodyTexDataset(
        df=bodytex_df, 
        dir_annotate='/datagrid/Medical/skin/skin3d/data/3dbodytex-1.1-highres/annotations',
        dir_multi_annotate='/datagrid/Medical/skin/skin3d/data/3dbodytex-1.1-highres/multiple_annotators',
        dir_textures='/datagrid/Medical/skin/3dbodytex-1.1-highres'
        )
    return bodytex

def get_train_df(bodytex):
    return bodytex.annotated_samples_in_partition('train')
def get_valid_df(bodytex):
    return bodytex.annotated_samples_in_partition('valid')
def get_test_df(bodytex):
    return bodytex.annotated_samples_in_partition('test')


if __name__ == '__main__':
    bodytex = load_bodytex_dataset()
    # to print summary of bodytex dataset: print(bodytex.summary())

    # to print train/valid/test dataframes simply uncomment the following
    # <valid>_df = bodytex.annotated_samples_in_partition('<valid>')
    # just change <valid> for train/valid/test according to your needs

    test_df = get_test_df(bodytex)
    
    print(test_df.head(30))

    # You can get the bounding boxes for scan_id '002'
    # that belongs to the training data.
    # scan_id = train_df.scan_id.values[0]
    # print(scan_id)
    # print(bodytex.annotation(scan_id).head())
    # similarly for valid

    # For testing, we evaluated against 3 annotators, 
    # where each annotator provided independent bounding boxes for each test scan.
    # For scans in the "test" partition, you can specify the annotator 
    # along with the scan_id to get the bounding boxes for a specific annotator.
    scan_id = test_df.scan_id.values[0]
    print(scan_id)
    # Here's how you specify the `annotator` for scans in the test partition.
    # (this won't work for scans that are not part of the "test" partition)
    print(bodytex.annotation(scan_id, annotator='A1').head())
 





    print('\ndone...exiting')

