import argparse
import cv2
import pandas as pd
import numpy as np
import os
from PIL import Image, ImageOps
from tqdm import tqdm

from func_to_script import script
from functools import partial
import sys
import os
import numpy as np
from typing import List
# Tensor
import torch
import torchvision
from torch import Tensor

from yolov7.models.yolo import scale_bboxes_to_original_image_size





from yolov7 import create_yolov7_model
from yolov7.dataset import Yolov7Dataset, create_yolov7_transforms, yolov7_collate_fn, create_base_transforms
from yolov7.evaluation import CalculateMeanAveragePrecisionCallback
from yolov7.loss_factory import create_yolov7_loss
from yolov7.trainer import Yolov7Trainer, filter_eval_predictions
from yolov7.mosaic import MosaicMixupDataset, create_post_mosaic_transform
from yolov7.trainer import Yolov7Trainer, filter_eval_predictions
from yolov7.utils import SaveBatchesCallback, Yolov7ModelEma

from pytorch_accelerated.callbacks import (
    EarlyStoppingCallback,
    SaveBestModelCallback,
    get_default_callbacks,
    ModelEmaCallback
)

from pytorch_accelerated.schedulers import CosineLrScheduler
sys.path.insert(0, os.path.abspath(os.path.join(os.getcwd(), '/ssd/temporary/sursamue/skin/skin3d')))
from skin3d.bodytex import BodyTexDataset

from skin3d.visualize import embed_annotatations
from skin3d.visualize import embed_box_borders



from torch.utils.data import Dataset, DataLoader
import albumentations as A


def convert_bbox_xywh_to_xyxy(df):
    """ Function to convert df from bbox_x, bbox_y, bbox_width, bbox_height format to xmin, ymin, xmax, ymax format.
    Args:
        df (pd.DataFrame): df with columns bbox_x, bbox_y, bbox_width, bbox_height
    Returns:
        pd.DataFrame: df with columns xmin, ymin, xmax, ymax
    """

    df['bbox_x'] = df['bbox_x']
    df['bbox_y'] = df['bbox_y']
    df['bbox_width'] = df['bbox_x'] + df['bbox_width']
    df['bbox_height'] = df['bbox_y'] + df['bbox_height']
    # rename columns
    df.rename(columns={'bbox_x': 'xmin', 'bbox_y': 'ymin', 'bbox_width': 'xmax', 'bbox_height': 'ymax'}, inplace=True)
    return df


def convert_df_to_size(df, size):
    """ Function to convert df from image_width, image_height to size x size.
    Args:
        df (pd.DataFrame): df with columns image_width, image_height
        size (int): size to which we want to convert
    Returns:
        pd.DataFrame: df with boxes converted to size x size
    """
    # use albumentations to resize images and bboxes
    # use _apply_transforms function
    df = convert_bbox_xywh_to_xyxy(df)
    # get unique image_ids
    image_ids = df['image_id'].unique()
    # iterate over image_ids
    for image_id in image_ids:
        # get "row" of image_id, which is a list of all bboxes for that image
        image_df = df[df['image_id'] == image_id]
        # get full image name
        image_name = image_df['image_name'].values[0]
        # get dims
        image_hw = image_df[['image_height', 'image_width']].values[0]
        # get boxes, the format is bbox_x, bbox_y, bbox_width, bbox_height
        boxes = image_df[['xmin', 'ymin', 'xmax', 'ymax']].values
        # class labels can be ommited as well as image, we will create fake image with appropriate size
        classes = np.zeros((boxes.shape[0], 1))
        # image is ommited
        image = np.zeros((image_hw[0], image_hw[1], 3))
        # apply transforms
        image, boxes, classes = _apply_transforms(image, boxes, classes, size)
        # update df    # checkpoint_path = '/home.stud/sursamue/bakalarka/detection/yolov8/best_model.pt'

        df.loc[df['image_id'] == image_id, 'image_height'] = size
        df.loc[df['image_id'] == image_id, 'image_width'] = size
        df.loc[df['image_id'] == image_id, 'xmin'] = boxes[:, 0]
        df.loc[df['image_id'] == image_id, 'ymin'] = boxes[:, 1]
        df.loc[df['image_id'] == image_id, 'xmax'] = boxes[:, 2]
        df.loc[df['image_id'] == image_id, 'ymax'] = boxes[:, 3]

    return df


def convert_df_to_size_dont_change_aspect_ratio(df, size):
    """ Function to convert df from image_width, image_height to size x size_keeping aspect ratio.
    Args:
        df (pd.DataFrame): df with columns image_width, image_height
        size (int): size to which we want to convert
    Returns:
        pd.DataFrame: df with boxes converted to size x size keeping aspect ratio
    """
    # use albumentations to resize images and bboxes
    # use _apply_transforms function
    df = convert_bbox_xywh_to_xyxy(df)
    # get unique image_ids
    image_ids = df['image_id'].unique()
    # iterate over image_ids
    for image_id in image_ids:
        # get "row" of image_id, which is a list of all bboxes for that image
        image_df = df[df['image_id'] == image_id]
        # get full image name
        image_name = image_df['image_name'].values[0]
        # get dims
        image_hw = image_df[['image_height', 'image_width']].values[0]
        # get boxes, the format is bbox_x, bbox_y, bbox_width, bbox_height
        boxes = image_df[['xmin', 'ymin', 'xmax', 'ymax']].values
        # class labels can be ommited as well as image, we will create fake image with appropriate size
        classes = np.zeros((boxes.shape[0], 1))
        # image is ommited
        image = np.zeros((image_hw[0], image_hw[1], 3))
        # apply transforms
        image, boxes, classes = _apply_transforms_keeping_aspect_ratio(image, boxes, classes, size)
        print(image.shape)
        # update df
        df.loc[df['image_id'] == image_id, 'image_height'] = image.shape[0]
        df.loc[df['image_id'] == image_id, 'image_width'] = image.shape[1]
        df.loc[df['image_id'] == image_id, 'xmin'] = boxes[:, 0]
        df.loc[df['image_id'] == image_id, 'ymin'] = boxes[:, 1]
        df.loc[df['image_id'] == image_id, 'xmax'] = boxes[:, 2]
        df.loc[df['image_id'] == image_id, 'ymax'] = boxes[:, 3]

    return df


def _apply_transforms_keeping_aspect_ratio(image, boxes, classes, size):
    transforms = A.Compose(
        [A.LongestMaxSize(max_size=size, always_apply=True, p=1.0), # resize image to size x size keeping aspect ratio
        # A.PadIfNeeded(min_height=size, min_width=size, always_apply=True, border_mode=0, value=0, p=1.0), # 
        ],

        bbox_params=A.BboxParams(
            format="pascal_voc",
            label_fields=["labels"],

        ),
    )
    transformed = transforms(image=image, bboxes=boxes, labels = classes)

    image = transformed["image"]
    boxes = np.array(transformed["bboxes"])
    classes = np.array(transformed["labels"])

    return image, boxes, classes


def convert_to_trainer_form(df, size):
    # renames label_name to class_id
    # puts has_annotations everywhere to True
    df = df.copy()
    df = convert_df_to_size(df, size)
    df.rename(columns={'label_name': 'class_id'}, inplace=True)
    df['has_annotation'] = True
    # sets class_id to 0
    df['class_id'] = 0
    return df

def convert_to_trainer_form_dont_change_aspect_ratio(df, size):
    # renames label_name to class_id
    # puts has_annotations everywhere to True
    df = df.copy()
    df = convert_df_to_size_dont_change_aspect_ratio(df, size)
    df.rename(columns={'label_name': 'class_id'}, inplace=True)
    df['has_annotation'] = True
    # sets class_id to 0
    df['class_id'] = 0
    return df

def image_resize(image, width = None, height = None, inter = cv2.INTER_AREA):
    # initialize the dimensions of the image to be resized and
    # grab the image size
    dim = None
    (h, w) = image.shape[:2]

    # if both the width and height are None, then return the
    # original image
    if width is None and height is None:
        return image

    # check to see if the width is None
    if width is None:
        # calculate the ratio of the height and construct the
        # dimensions
        r = height / float(h)
        dim = (int(w * r), height)

    # otherwise, the height is None
    else:
        # calculate the ratio of the width and construct the
        # dimensions
        r = width / float(w)
        dim = (width, int(h * r))

    # resize the image
    resized = cv2.resize(image, dim, interpolation = inter)

    # return the resized image
    return resized





# trainer expects image to be size x size but my images are bigger then max size and bboxes are also bigger
# convert bboxes to size x size using albumentations
def _apply_transforms(image, boxes, classes, size):
    transforms = A.Compose(
        [A.Resize(size, size),],

        bbox_params=A.BboxParams(
            format="pascal_voc",
            label_fields=["labels"],

        ),
    )
    transformed = transforms(image=image, bboxes=boxes, labels = classes)

    # image = transformed["image"]
    boxes = np.array(transformed["bboxes"])
    # classes = np.array(transformed["labels"])

    return image, boxes, classes


class DermaNudeDataset(Dataset):
    """ Dataset class for derma nude dataset.
    Args:
        dataframe (pd.DataFrame): the boxes in df are of a format bbox_x, bbox_y, bbox_width, bbox_height
        camera_path (str): path to camera folder where images are stored
    """

    def __init__(self, dataframe, camera_path):
        self.df = dataframe
        self.camera_path = camera_path
        # self.size = size
        self.image_idx_to_image_id = { # dict to translate image_idx to image_id
            idx: image_id
            for idx, image_id in enumerate(self.df.image_id.unique())
        }
        self.image_id_to_image_idx = { # dict to translate image_id to image_idx
            v: k for k, v, in self.image_idx_to_image_id.items()
        }

    def __len__(self):
        # len is number len of unique image values in df['image_id']]
        return len(self.df['image_id'].unique())

    def _apply_transforms(self, image, boxes, classes):
        transforms = A.Compose(
        [A.LongestMaxSize(max_size=1600, always_apply=True, p=1.0), # resize image to size x size keeping aspect ratio
        # A.PadIfNeeded(min_height=size, min_width=size, always_apply=True, border_mode=0, value=0, p=1.0), # 
        ],
        bbox_params=A.BboxParams(
            format="pascal_voc",
            label_fields=["labels"],

        ),
    )

        transformed = transforms(image=image, bboxes=boxes, labels = classes)

        image = transformed["image"]
        boxes = np.array(transformed["bboxes"])
        classes = np.array(transformed["labels"])

        return image, boxes, classes

    def __getitem__(self, idx):
        # returns image, bboxes, class_labels, image_id, image_hw
        # get item based on image_id
        image_id = self.image_idx_to_image_id[idx]
        # get "row" of image_id, which is a list of all bboxes for that image
        image_df = self.df[self.df['image_id'] == image_id]
        # get full image name
        image_name = image_df['image_name'].values[0]
        # get dims
        image_hw = image_df[['image_height', 'image_width']].values[0]
        # get boxes, the format is bbox_x, bbox_y, bbox_width, bbox_height
        # boxes = image_df[['bbox_x', 'bbox_y', 'bbox_width', 'bbox_height']].values
        # boxes from xywh to xyxy
        # boxes[:, 2] = boxes[:, 0] + boxes[:, 2]
        # boxes[:, 3] = boxes[:, 1] + boxes[:, 3]
        boxes = image_df[['xmin', 'ymin', 'xmax', 'ymax']].values



        # get class labels
        class_labels = image_df['class_label'].values
        # convert extenstion of image_name from .JPG to .jpg
        image_name = image_name.split('.')[0] + '.jpg'
        # form whole path of image
        image_path = os.path.join(self.camera_path, image_name)
        # get image
        # image = Image.open(image_path).convert('RGB')
        # image = cv2.imread(image_path)
        # image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
        # image = ImageOps.exif_transpose(image) # image ops to fix orientation caused by PIL wrongly reading photos from dslr
        image = cv2.imread(image_path)

        image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
        
        original_width = image.shape[1]
        original_height = image.shape[0]
        if original_width > original_height:
        # image = cv2.resize(image, (800, 800))
            image = image_resize(image, width = 1600)
            # pad the image to have size 800x800
            # new_dims = image.shape[:2]
            image = cv2.copyMakeBorder(image, 0, 1600 - image.shape[0], 0, 1600 - image.shape[1], cv2.BORDER_CONSTANT, value=[0,0,0])
        else:
            image = image_resize(image, height = 1600)
            # pad the image to have size 800x800
            # new_dims = image.shape[:2]
            image = cv2.copyMakeBorder(image, 0, 1600 - image.shape[0], 0, 1600 - image.shape[1], cv2.BORDER_CONSTANT, value=[0,0,0])
        
        # image = np.asarray(image, dtype=np.uint8).copy()

        # apply transforms
        # image, boxes, class_labels = self._apply_transforms(
        #     image, boxes, class_labels
        # )
        # image_height = image.shape[0]
        # image_width = image.shape[1]
        # image_hw = (image_height, image_width) 
        # image, _, _ = self._apply_transforms(
        #     image, boxes, class_labels
        # )
        
        print(image.shape)
        print(image_hw)
        image_hw = (image.shape[0], image.shape[1])
        # is image mirrored?
        return image, boxes, class_labels, image_id, image_hw

    def image_resize(image, width = None, height = None, inter = cv2.INTER_AREA):
    # initialize the dimensions of the image to be resized and
    # grab the image size
        dim = None
        (h, w) = image.shape[:2]

        # if both the width and height are None, then return the
        # original image
        if width is None and height is None:
            return image

        # check to see if the width is None
        if width is None:
            # calculate the ratio of the height and construct the
            # dimensions
            r = height / float(h)
            dim = (int(w * r), height)

        # otherwise, the height is None
        else:
            # calculate the ratio of the width and construct the
            # dimensions
            r = width / float(w)
            dim = (width, int(h * r))

        # resize the image
        resized = cv2.resize(image, dim, interpolation = inter)

        # return the resized image
        return resized


# get dataframe
def get_dataframe(dermanude_path,camera_type):
    """ Returns dataframe with annotations for dermanude dataset.
    Args:
        dermanude_path (str): path to dermanude dataset
        camera_type (str): 'phone' or 'camera'
    Returns:
        df (pd.DataFrame): dataframe with annotations
    """

    csv_file = None
    if camera_type == 'phone':
        csv_file = (dermanude_path + '/phone/train.csv')
    elif camera_type == 'camera':
        csv_file = (dermanude_path + '/camera/train.csv')
    df = pd.read_csv(csv_file)
    df = convert_labels_to_numbers(df)
    return df

# convert label_name column to numbers
def convert_labels_to_numbers(df):
    """ Converts label_name column to numbers.
    but all label names are same so this function is useless???
    Args:
        df (pd.DataFrame): dataframe with annotations
    Returns:
        df (pd.DataFrame): dataframe with annotations
    """
    # get unique label names
    label_names = df['class_id'].unique()
    # create dict to map label names to numbers
    label_name_to_number = {label_name: i for i, label_name in enumerate(label_names)}
    # create new column with numbers
    df['class_label'] = df['class_id'].map(label_name_to_number)
    return df

def get_dataframes(dermanude_path):
    """ Returns dataframes with annotations for dermanude dataset.
    Args:
        dermanude_path (str): path to dermanude dataset
    Returns:
        dermanude_phone_df (pd.DataFrame): dataframe with annotations for phone images
        dermanude_camera_df (pd.DataFrame): dataframe with annotations for camera images
    """
    # get phone and camera dataframes
    dermanude_phone_df = get_dataframe(dermanude_path, 'phone')
    dermanude_camera_df = get_dataframe(dermanude_path, 'camera')
    return dermanude_phone_df, dermanude_camera_df


def draw_gt_boxes(image, gt_df, image_id):
    """function that will draw the ground truth boxes on the image
    Args:
        image (np.array): image
        gt_df (dataframe): dataframe with ground truth boxes
        image_name (str): name of the image
    Returns:
        image (np.array): image with ground truth boxes
    """
    #filter out everything except the image_name
    gt_df = gt_df[gt_df['image_id'] == image_id]
    # convert boxes to list
    # boxes = gt_df[["bbox_x", "bbox_y", "bbox_width", "bbox_height"]].values.tolist()
    boxes = gt_df[["xmin", "ymin", "xmax", "ymax"]].values.tolist()
    # print(boxes)

    # get the boxes
    for index, box in enumerate(boxes):

        color = [0,255,0]
        pad = 4
        x = int(box[0])
        y = int(box[1])
        # convert
        width = int(box[2] - box[0])
        height = int(box[3] - box[1])

        embed_box_borders(image, x, y, width, height, color, pad)
    return image

def draw_gt_boxes_scaled_down(image, boxes):
    """ Draw bounding boxes on the image
    Args:
        image (image): image to draw on
        boxes (list): list of boxes
    Returns:
        image (image): image with bounding boxes
    """
    
    # enumerateboxes and scores
    
    for index, box in enumerate(boxes):

        color = [0,255,0]
        pad = 2
        x = int(box[0])
        y = int(box[1])
        # convert
        width = int(box[2] - box[0])
        height = int(box[3] - box[1])
                # width = int(box[2])
        # height = int(box[3])
        embed_box_borders(image, x, y, width, height, color, pad)

    return image



def draw_bounding_boxes(image, boxes, scores):
    """ Draw bounding boxes on the image
    Args:
        image (image): image to draw on
        boxes (list): list of boxes
        scores (list): list of scores
    Returns:
        image (image): image with bounding boxes
    """
    
    # enumerateboxes and scores
    for index, (box, score) in enumerate(zip(boxes, scores)):

        color = [255,0,0]
        pad = 10
        x = int(box[0])
        y = int(box[1])
        # convert
        width = int(box[2] - box[0])
        height = int(box[3] - box[1])
        # draw cv2 box
        cv2.rectangle(image, (x, y), (x+width, y+height), color, 4)
        # draw score but smaller and convert it to percentage and write only 0  decimals and convert to int, print it on the bottom
        # cv2.putText(image, str(int(score*100)), (x, y+height+10), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0,0,0), 1, cv2.LINE_AA)
        cv2.putText(image, str(int(score*100))+'%', (x, y+height+30), cv2.FONT_HERSHEY_SIMPLEX, 1, (0,0,0), 4, cv2.LINE_AA)


        # draw index on the top of the boxe, use color black
        # cv2.putText(image, str(index), (x, y-10), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0,0,0), 1, cv2.LINE_AA)
    return image

def export_rescaled_pred_bboxes(rescaled_pred_bboxes, image_id, image_width, image_height, score):
    """function that will export the rescaled predicted bounding boxes to a csv file and save it
    Args:
        rescaled_pred_bboxes (list): list of rescaled predicted bounding boxes in x1, y1, x2, y2 format
        image_id (str): image id
        image_width (int): image width
        image_height (int): image height
    Returns:
        None
    """
    # create a dataframe with the rescaled predicted bounding boxes convert xyxy to xywh
    rescaled_pred_bboxes = rescaled_pred_bboxes.copy()
    rescaled_pred_bboxes[:, 2] = rescaled_pred_bboxes[:, 2] - rescaled_pred_bboxes[:, 0]
    rescaled_pred_bboxes[:, 3] = rescaled_pred_bboxes[:, 3] - rescaled_pred_bboxes[:, 1]
    df = pd.DataFrame(rescaled_pred_bboxes, columns=['bbox_x', 'bbox_y', 'bbox_width', 'bbox_height'])
    # add header to csv file


    # add image id and image width and height
    df['image_id'] = image_id
    df['image_width'] = image_width
    df['image_height'] = image_height
    df['score'] = score
    # reorder columns
    df = df[['image_id', 'image_width', 'image_height', 'bbox_x', 'bbox_y', 'bbox_width', 'bbox_height', 'score']]
    # export to csv, if first time add header
    if not os.path.exists('tmp/rescaled_pred_bboxes_1600.csv'):
        df.to_csv('tmp/rescaled_pred_bboxes_1600.csv', mode='a', header=True, index=False)
    else:
        df.to_csv('tmp/rescaled_pred_bboxes_1600.csv', mode='a', header=False, index=False)



    # function to save rescaled bboxes to csv, the boxes per image_id will be 
    # input is a list of bboxes, image_id
    # output is a csv file with the bboxes and image_id
def save_rescaled_gt_bboxes(rescaled_bboxes, image_id):
    """function that will save the rescaled bounding boxes to a csv file for evaluation in xyxy format
    Args:
        rescaled_bboxes (list): list of rescaled bounding boxes in x1, y1, x2, y2 format
        image_id (str): image
    Returns:
        None
    """

        # create a dataframe with the rescaled predicted bounding boxes do not convert
    rescaled_bboxes = rescaled_bboxes.copy()

        # add header to csv file
    df = pd.DataFrame(rescaled_bboxes, columns=['xmin', 'ymin', 'xmax', 'ymax'])

    df['image_id'] = image_id

    # reorder columns
    df = df[['image_id', 'xmin', 'ymin', 'xmax', 'ymax']]
    # export to csv, if first time add header
    if not os.path.exists('stats_new/gt_bboxes_phone_1600.csv'):
        df.to_csv('stats_new/gt_bboxes_phone_1600.csv', mode='a', header=True, index=False)
    else:
        df.to_csv('stats_new/gt_bboxes_phone_1600.csv', mode='a', header=False, index=False)


if __name__ == '__main__':
    # global path to dermanude dataset
    dermanude_path = '/mnt/datagrid/Medical/skin/DermaNude_1600'

    # # get dataframes
    # dermanude_phone_df, dermanude_camera_df = get_dataframes(dermanude_path)
    # dermanude_phone_trainer_df_800 = convert_to_trainer_form(dermanude_phone_df, 800)
    # dermanude_phone_trainer_df_1600 = convert_to_trainer_form(dermanude_phone_df, 1600)
    # dermanude_camera_trainer_df_800 = convert_to_trainer_form(dermanude_camera_df, 800)
    # dermanude_camera_trainer_df_1600 = convert_to_trainer_form(dermanude_camera_df, 1600)
    # dermanude_camera_trainer_df_2500 = convert_to_trainer_form(dermanude_camera_df, 2500)
    # print(dermanude_camera_df.head())

    # keep aspect ratio
    # dermanude_phone_trainer_df_1600_keep_aspect_ratio = convert_to_trainer_form_dont_change_aspect_ratio(dermanude_phone_df, 1600)
    # dermanude_camera_trainer_df_1600_keep_aspect_ratio = convert_to_trainer_form_dont_change_aspect_ratio(dermanude_camera_df, 1600)
    dermanude_phone_df_1600, dermanude_camera_df_1600 = get_dataframes(dermanude_path)

    # save df to csv as gt_bboxes_phone.csv or gt_bboxes_camera.csv
    dermanude_camera_df_1600.to_csv('stats/gt_bboxes_camera.csv', index=False)
    dermanude_phone_df_1600.to_csv('stats/gt_bboxes_phone.csv', index=False)


    # define paths

    phone_path = dermanude_path + '/phone'
    camera_path = dermanude_path + '/camera'
    # phone_path = '/mnt/datagrid/Medical/skin/DermaNude/my_images/phone'
    # camera_path = '/mnt/datagrid/Medical/skin/DermaNude/my_images/camera'


    dermacamera_dataset_1600 = DermaNudeDataset(dermanude_camera_df_1600, camera_path)
    dermaphone_dataset_1600 = DermaNudeDataset(dermanude_phone_df_1600, phone_path)


    # dermaphone_dataset_800 = DermaNudeDataset(dermanude_phone_df, phone_path, size = 800)
    # dermacamera_dataset_1600 = DermaNudeDataset(dermanude_camera_df, camera_path, size = 1600)
    # dermacamera_dataset_800 = DermaNudeDataset(dermanude_camera_df, camera_path, size = 800)
    # dermacamera_dataset_2500 = DermaNudeDataset(dermanude_camera_df, camera_path, size = 2500)
    # dermaphone_dataset_1600 = DermaNudeDataset(dermanude_phone_df, phone_path, size = 1600)

    # test_camera_ds_1600 = Yolov7Dataset(
    #     dermacamera_dataset_1600,
    #     create_yolov7_transforms(training=False, image_size=(1600, 1600)),
    # )
    # test_camera_ds_800 = Yolov7Dataset(
    #     dermacamera_dataset_800,
    #     create_yolov7_transforms(training=False, image_size=(800, 800)),
    # )



    # test_phone_ds_1600 = Yolov7Dataset(
    #     dermaphone_dataset_1600,
    #     create_yolov7_transforms(training=False, image_size=(1600, 1600)),
    # )
    # test_phone_ds_800 = Yolov7Dataset(
    #     dermaphone_dataset_800,
    #     create_yolov7_transforms(training=False, image_size=(800, 800)),
    # )

    camera_ds = Yolov7Dataset(
        dermacamera_dataset_1600,
        create_yolov7_transforms(training=True, image_size=(1600, 1600)),
    )
    phone_ds = Yolov7Dataset(
        dermaphone_dataset_1600,
        create_yolov7_transforms(training=True, image_size=(1600, 1600)),
    )



    # preds = pd.read_csv('empty/final_camera.csv')
    # # preds = pd.read_csv('empty/predictions_camera_nms_iou.csv')  
      
    # if os.path.exists('tmp/rescaled_pred.csv'):
    #     os.remove('tmp/rescaled_pred.csv')



    # for i in tqdm(range(len(dermacamera_dataset_1600))):
    #     # delete tmp file rescaled_pred_bboxes.csv
        
        
    #     image, boxes, class_labels, image_id, image_hw = dermacamera_dataset_1600[i]
    #     # pred_boxes = preds[preds['image_id'] == image_id][['xmin', 'ymin', 'xmax', 'ymax']].values
    #     # # # make tensor from pred_boxes
    #     # pred_boxes = torch.tensor(pred_boxes)
    #     scores = preds[preds['image_id'] == image_id]['score'].values
        
    #     # # get original_image_size from image_height, image_width of dermanude_camera_df
    #     # original_image_size = dermanude_camera_df[dermanude_camera_df['image_id'] == image_id][['image_height', 'image_width']].values[0]
    #     # pred_boxes = scale_bboxes_to_original_image_size(
    #     #     pred_boxes,
    #     #     (1600, 1600),
    #     #     (original_image_size[0], original_image_size[1]),
    #     #     is_padded=False,
    #     # )

    #     pred_boxes = preds[preds['image_id'] == image_id][['bbox_x','bbox_y', 'bbox_width' ,'bbox_height']].values
    #     # convert to xyxy
    #     pred_boxes[:, 2] = pred_boxes[:, 0] + pred_boxes[:, 2]
    #     pred_boxes[:, 3] = pred_boxes[:, 1] + pred_boxes[:, 3]
    #     # make tensor from pred_boxes
    #     pred_boxes = torch.tensor(pred_boxes)

    #     pred_boxes = pred_boxes.numpy()

        
    #     # export_rescaled_pred_bboxes(pred_boxes, image_id, original_image_size[1], original_image_size[0], scores)
    #     # save image to tmp/temp_image.jpg
    #     # load original image load it using the image_id and dermacamera_dataframe
    #     image_name = dermanude_camera_df[dermanude_camera_df['image_id'] == image_id]['image_name'].values[0]
    #     image = cv2.imread(os.path.join(camera_path, image_name))
    #     # image = cv2.cvtColor(image, cv2.COLOR_RGB2BGR)


    #     image = draw_bounding_boxes(image, pred_boxes, scores)
    #     # image = draw_gt_boxes_scaled_down(image, boxes)

    #     image = draw_gt_boxes(image, dermanude_camera_df, image_id)

        
    #     # gt_800_df = save_rescaled_gt_bboxes(boxes, image_id)
    #     # print(image_id)
        
    #     cv2.imwrite('tmp/temp_image.jpg', image)
    #     input()
    #     # if image_id ==10:
    #     #     cv2.imwrite('tmp/temp_image.jpg', image)

    #     #     input()

    # print('Done')
    # quit()


    # preds = pd.read_csv('stats_camera/predictions_cv2_nms.csv')
    

    # for i in tqdm(range(len(dermacamera_dataset_1600))):
    #     # delete tmp file rescaled_pred_bboxes.csv
        
        
    #     image, boxes, class_labels, image_id, image_hw = dermacamera_dataset_1600[i]

    #     # pred_boxes = preds[preds['image_id'] == image_id][['bbox_x','bbox_y', 'bbox_width' ,'bbox_height']].values
    #     # xyxy
    #     pred_boxes = preds[preds['image_id'] == image_id][['xmin','ymin', 'xmax' ,'ymax']].values
    #     # mirror the boxes
    #     # pred_boxes[:, 0] = image_hw[1] - pred_boxes[:, 0]
    #     # pred_boxes[:, 2] = image_hw[1] - pred_boxes[:, 2] 

    #     # make tensor from pred_boxes
    #     scores = preds[preds['image_id'] == image_id]['score'].values

        
    #     # get original_image_size from image_height, image_width of dermanude_camera_df
    #     # original_image_size = dermanude_camera_df[dermanude_camera_df['image_id'] == image_id][['image_height', 'image_width']].values[0]

        
    #     # pred_boxes = pred_boxes.numpy()
    #     # save image to tmp/temp_image.jpg
    #     # load original image load it using the image_id and dermacamera_dataframe
    #     # image_name = dermanude_camera_df[dermanude_camera_df['image_id'] == image_id]['image_name'].values[0]
    #     image_name = dermanude_camera_df_1600[dermanude_camera_df_1600['image_id'] == image_id]['image_name'].values[0]
    #     # lower the .JPG to .jpg
    #     image_name = image_name.split('.')[0] + '.jpg'
    #     print(image_name)

    #     # image = cv2.imread(os.path.join(camera_path, image_name))
        
    #     # image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
    #     # image = cv2.cvtColor(image, cv2.COLOR_RGB2BGR)
    #     # read as pil image
    #     image = Image.open(os.path.join(camera_path, image_name)).convert('RGB')
    #     image = ImageOps.exif_transpose(image)
    #     image = np.asarray(image, dtype=np.uint8).copy()


    #     image = draw_bounding_boxes(image, pred_boxes, scores)
    #     image = draw_gt_boxes(image, dermanude_camera_df_1600, image_id)

    #     cv2.imwrite('tmp/temp_image.jpg', image)
    #     print(image_id)
    #     input()
    # print('Done')
    # quit()



    model = create_yolov7_model('yolov7', num_classes=1)
    loss_func = create_yolov7_loss(model, image_size=1600)

    checkpoint_path = "models/detection/best_model.pt"
    # checkpoint_path = '/home.stud/sursamue/temporary/yolov7x_04_10/best_model.pt'
    # checkpoint_path = '/home.stud/sursamue/bakalarka/detection/yolov8/best_model.pt'
    trainer = Yolov7Trainer( 
        model=model,
        optimizer=None,
        loss_func=loss_func,
        # filter_eval_predictions_fn=partial(
        #     filter_eval_predictions, confidence_threshold=0.0, nms_threshold=0.01
        # ),
        callbacks=[
            CalculateMeanAveragePrecisionCallback.create_from_targets_df(
                targets_df=dermanude_camera_df_1600.query("has_annotation == True")[
                    ["image_id", "xmin", "ymin", "xmax", "ymax", "class_id"]
                ],# just ids with bboxes                            
                image_ids=set(dermanude_camera_df_1600.image_id.unique()), # all img ids
                iou_threshold=0.10,
                save_predictions_output_dir_path="./stats_camera",
                verbose = False
            ),
            SaveBestModelCallback(watch_metric="map", greater_is_better=True),
            EarlyStoppingCallback(
                early_stopping_patience=100,
                watch_metric="map",
                greater_is_better=True,
                early_stopping_threshold=0.0001,
            ),
            *get_default_callbacks(progress_bar=True),
        ],
    )

    


    trainer.load_checkpoint(checkpoint_path, load_optimizer=False)
    trainer.evaluate(dataset=camera_ds,
     per_device_batch_size = 2, collate_fn=yolov7_collate_fn)





  