import sys
import os
import argparse
import pandas as pd
import numpy as np
import torchvision
import torch
from tqdm import tqdm

# function to run batched nms on predictions csv iterate through image_id's, create new csv with nms applied, convert predictions to tensor, pass them to function and after back to numpy array
def run_nms(predictions_csv_path, output_csv_path, confidence_threshold, nms_threshold):
    # read predictions csv
    predictions_df = pd.read_csv(predictions_csv_path, converters={'image_id': int})
    # get unique image_ids
    image_ids = predictions_df['image_id'].unique()
    # create empty list to store nms predictions
    nms_predictions = []
    # loop over image_ids with tqdm
    for image_id in tqdm(image_ids):
        # get predictions for image_id
        image_predictions = predictions_df[predictions_df['image_id'] == image_id]
        # convert to numpy array
        image_predictions = image_predictions.to_numpy()
        # convert to tensor
        image_predictions = torch.from_numpy(image_predictions)
        # run batched nms
        nms_preds = run_batched_nms(image_predictions, confidence_threshold, nms_threshold)
        # convert to numpy array
        nms_preds = nms_preds.numpy()
        # append nms_preds to nms_predictions
        nms_predictions.append(nms_preds)
    # convert nms_predictions to numpy array
    nms_predictions = np.concatenate(nms_predictions)
    # convert nms_predictions to dataframe

    nms_predictions_df = pd.DataFrame(nms_predictions, columns=['image_id', 'xmin', 'ymin', 'xmax', 'ymax', 'score','category_id'])
    # save nms_predictions_df to csv
    nms_predictions_df.to_csv(output_csv_path, index=False)




    # # read predictions csv
    # predictions_df = pd.read_csv(predictions_csv_path)
    # # get unique image_ids
    # image_ids = predictions_df['image_id'].unique()
    # # create empty list to store nms predictions
    # nms_predictions = []
    # # loop over image_ids
    # for image_id in image_ids:
    #     # get predictions for image_id
    #     image_predictions = predictions_df[predictions_df['image_id'] == image_id]
    #     # convert to numpy array
    #     image_predictions = image_predictions.to_numpy()

    #     # run batched nms
    #     nms_preds = run_batched_nms(image_predictions, confidence_threshold, nms_threshold)
    #     # append nms_preds to nms_predictions
    #     nms_predictions.append(nms_preds)
    # # convert nms_predictions to numpy array
    # nms_predictions = np.concatenate(nms_predictions)
    # # convert nms_predictions to dataframe
    # nms_predictions_df = pd.DataFrame(nms_predictions, columns=['image_id', 'xmin', 'ymin', 'xmax', 'ymax', 'score','category_id'])
    # # save nms_predictions_df to csv
    # nms_predictions_df.to_csv(output_csv_path, index=False)

# func run_batched_nms torchvision.ops.batched_nms, convert to tensor and back to numpy array
def run_batched_nms(predictions, confidence_threshold, nms_threshold):
    # get boxes, scores, labels
    boxes = predictions[:, 1:5]
    scores = predictions[:, 5]
    labels = predictions[:, 6]
    # run batched nms
    keep = torchvision.ops.batched_nms(boxes, scores, labels, nms_threshold)
    # get nms predictions
    nms_predictions = predictions[keep]
    # filter by confidence_threshold
    nms_predictions = nms_predictions[nms_predictions[:, 5] > confidence_threshold]
    # return nms_predictions
    return nms_predictions


def main(predictions_csv_path, output_csv_path, confidence_threshold, nms_threshold):
    run_nms(predictions_csv_path, output_csv_path, confidence_threshold, nms_threshold)

if __name__=='__main__':
    # parse arguments
    parser = argparse.ArgumentParser()
    parser.add_argument('-p', '--predictions_csv_path', type=str, help='path to predictions csv')
    parser.add_argument('-o', '--output_csv_path', type=str, help='path to output csv')
    parser.add_argument('-c', '--confidence_threshold', type=float, help='confidence threshold')
    parser.add_argument('-n', '--nms_threshold', type=float, help='nms threshold')
    args = parser.parse_args()


    # run batched nms on predictions csv
    main(args.predictions_csv_path, args.output_csv_path, args.confidence_threshold, args.nms_threshold)
    print('Your thresholds: \nconfidence_threshold: {} \nnms_threshold: {}'.format(args.confidence_threshold, args.nms_threshold))
