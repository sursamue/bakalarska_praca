\documentclass[conference]{IEEEtran}
\IEEEoverridecommandlockouts
% The preceding line is only needed to identify funding in the first footnote. If that is unneeded, please comment it out.


\ifCLASSOPTIONcompsoc
    \usepackage[caption=false, font=normalsize, labelfont=sf, textfont=sf]{subfig}
\else
\usepackage[caption=false, font=footnotesize]{subfig}
\fi

\usepackage{multirow}
\usepackage{todonotes}
\usepackage{float}
\usepackage[utf8]{inputenc}
\usepackage{rotating}
% \usepackage[slovak, shorthands=off]{babel}

\usepackage{csquotes}

\usepackage[T1]{fontenc}
% \usepackage{cite}
\usepackage{amsmath,amssymb,amsfonts}
\usepackage{algorithmic}
\usepackage{graphicx}
\usepackage{textcomp}
\usepackage{xcolor}

%\usepackage{csquotes}
\usepackage[style=ieee]{biblatex}
\addbibresource{report_yolo.bib}

\pagestyle{plain} % page numbering


\usepackage[unicode=true, bookmarks=true,bookmarksnumbered=true,
bookmarksopen=false, breaklinks=false,pdfborder={0 0 0},
pdfpagemode=UseNone,backref=false,colorlinks=true] {hyperref}




\begin{document}

\title{Robustness testing of CNN models for pigmented skin lesion classification   - technical report}

\author{\IEEEauthorblockN{Samuel Šúr}
}

\maketitle
\section{Introduction}
This report focuses on the robustness testing of our CNN models for pigmented skin lesion classification 
using various augmentation techniques. The use of augmentations during training
are important in order to simulate real world data and therefore improve the generalization of the model.
Similarly, we can use augmentations during testing to simulate real world data and therefore
test the robustness of the model. We will describe our approach and the results we achieved.

\section{Image classification}
The image classificators for  classification can be divided into two main categories:
the approaches using the traditional computer vision techniques and the approaches using deep learning.
The traditional approaches include k-Nearest Neighbors, Support Vector Machines, Naive Bayesian algorithm, etc.

The deep learning approaches are based on the convolutional neural networks (CNN).

% AlexNet
In 2012, A. Krizhevsky et al. \cite{krizhevsky_imagenet_2017} introduced the the AlexNet architecture,
which achieved the state-of-the-art results on the ImageNet dataset. 

% VGGNet
Visual Graphics Group (VGG) from Oxford University  introduced the VGGNet architecture
in 2014. One of the key contributions of VGGNet was the introduction of the concept of "deepness" in convolutional 
neural networks. The model consisted of up to 19 layers, which was much deeper than previous state-of-the-art models such as AlexNet. 
\cite{simonyan_very_2015}.

% GoogLeNet
The research group from Google, introduced GoogleNet also known as InceptionNet in 2014.
This architecture was designed to address the limitations of previous 
state-of-the-art models, such as VGGNet, which had large number of parameters and required a lot of computational power.
Another key contribution of InceptionNet was the introduction of the concept of "inception modules" which allowed the model to learn
both local and global features. \cite{szegedy_going_2014}.

% ResNet
ResNet or Residual Network was introduced by Microsoft Research in 2015. The main contribution 
of the ResNet architecture was introduction of the residual connections, which allows model 
to learn from the residual error between the input and the output of the layer. 
This architecture was designed to address the problem of vanishing gradients, which was a major problem in the previous
state-of-the-art models. \cite{he_deep_2015}.

% MobileNet family
In 2017 S. Howard et al. introduce MobileNet family of models \cite{howard_mobilenets_2017}, which are based on the concept of depthwise separable convolutions.
These convolutions are designed to reduce the number of parameters and computational power required by the model.
Therefore the MobileNet models are suitable for mobile devices. Instead of standard convolutional layers
the MobileNet models perform two types of convolutions: depthwise convolution and pointwise convolution. 
The depthwise convolution applies a single filter to each input channel, pointwise convolution combines the output of the depthwise convolution 
using 1x1 convolutional filters.

% EfficientNet family
In 2019 M. Tan and Q. V. Le introduce family of EfficientNet models \cite{tan_efficientnet_2020}, which are based on the
concept of compound scaling. The idea is to scale the width and depth of the network in a compound way, which allows to
achieve better results with less computational power. The family of EfficientNet models consists of 8 different models,
which differ in the scaling factor of the width and depth of the network.
The EfficientNet networks achieve the state-of-the-art results on the ImageNet dataset.







\section{Datasets of pigmented skin lesions}

\subsubsection{Dermofit Image Library}
Dataset called 
Dermofit Image Library \footnote{https://licensing.edinburgh-innovations.ed.ac.uk/product/dermofit-image-library} is a collection of
1300 colorful images of pigmented skin lesions. It contains 10 different types of skin lesions including Actinic Keratosis,
Basal Cell Carcinoma,
Melanocytic Nevus (mole),
Seborrhoeic Keratosis,
Squamous Cell Carcinoma,
Intraepithelial Carcinoma,
Pyogenic Granuloma,
Haemangioma,
Dermatofibroma,
Malignant Melanoma.

\subsubsection{PH2}Dataset PH2 \footnote{https://www.fc.up.pt/addi/ph2\%20database.html} consists of 200 images of pigmented skin lesions.
It contains 3 different types of skin lesions: common nevi, atypical nevi and melanoma.
\subsubsection{HAM10000} Dataset HAM10000 \footnote{https://challenge2018.isic-archive.com/} contains 10015 images of pigmented skin lesions.
It contains 7 different types of skin lesions: Actinic Keratosis, Basal Cell Carcinoma, Benign Keratosis, Dermatofibroma, Melanocytic Nevus, Melanoma, Vascular Lesion.
\subsubsection{ISIC2019}Challenge ISIC2019 \footnote{https://challenge2019.isic-archive.com/} was focused on skin lesion classification. 
They provide dataset made out from 3 different datasets: HAM10000 \cite{tschandl_ham10000_2018}, MSK \cite{codella_skin_2018} and BCN20000\cite{combalia_bcn20000_2019}.
The dataset contains 25 331 JPEG images from 8 different classes, same as in the HAM10000 but one more class - Squamous cell carcinoma.
\subsubsection{ISIC2020} Challenge ISIC2020 features a dataset of 33 126 images of pigmented skin lesions\cite{rotemberg_patient-centric_2021}. 
It features 5 classes - nevus, melanoma, seborrheic keratosis, lentigo and also unknown class. This dataset contains less classes than the previous ISIC challenges as the ISIC2020 challenge focused on the melanoma classification:
the goal was to classify the images into bening or malignant type of lesions.
\section{Our approach}
We trained three state-of-the-art CNN models for image classification. Two models were trained from scratch - Efficientnet-B0 and ResNet50 and one model using pretrained weights - EfficientNet-B7.
We do not use layer freezing, we train all layers of the models.
We use train, validation, test split in the ratio 60:20:20 using the preprocessed ISIC 2019 dataset\footnote{https://www.kaggle.com/datasets/cdeotte/jpeg-isic2019-512x512}.
It features the same images as in the ISIC 2019 dataset, but they are preprocessed and resized to 512x512 pixels (the center crop of the original image).
The dataset includes 8 classes, we present their names and abbreviations, which we use in the rest of the report:
Actinic Keratosis (AK), 
Basal Cell Carcinoma (BCC),
Benign Keratosis (BK),
 Dermatofibroma (DF),
  Melanocytic Nevus, Melanoma (MEL),
Vascular Lesion (VASC),
 Squamous Cell Carcinoma (SCC).
All of the models were trained on the train set. We use validation set for checking the performance and overfiting of the models.
Then we formed different ensembles of the models and evaluated them with the individual models on the test set.

We use number of augmentations during training to simulate real world data and therefore improve the generalization of the model.
These augmentations include image transposition, flip, shift, scale, rotation, brightness change, contrast change, hue change, saturation change, cutout, etc.
Although we use 512x512 images, we eventually resize them to 256x256. The training dataloader uses the weighted random sampler to balance the classes.
We use different batch sizes for each model, to use the GPU time efficiently.
We use the Adam optimizer with learning rate of $\approx 0.001$. We discovered that the learning rate of $\approx 0.001$ works well, 
therefore we did not use other learning rates. We follow the balanced accuracy metric on validation set during training. If the model does not improve its balanced accuracy for 10 epochs,
we stop the training. We save the model based on the best balanced accuracy on the validation dataset.

\section{Results}
In this section we are presenting the performance of the models on the test set without augmentations.
The training progress of the EfficientNet-B0 model is shown in ``Fig.~\ref{fig:efficientnet-b0}''. The training progress of the ResNet-50 model is shown in ``Fig.~\ref{fig:resnet-50}''.
The training of EfficientNet-B7 can be seen in ``Fig.~\ref{fig:efficientnet-b7}''.
% figure for efficientnet-b0
\begin{figure}[htbp]
    \centering
    \includegraphics[width=0.5\textwidth]{figs/graphs/efficientnet-b0_metrics.pdf}
    \caption{Training progress of the EfficientNet-B0 model, model evaluated on train set and validation set}
    \label{fig:efficientnet-b0}
    \end{figure}
    
    % figure for resnet-50
    \begin{figure}[htbp]
    \centering
    \includegraphics[width=0.5\textwidth]{figs/graphs/resnet50_metrics.pdf}
    \caption{Training progress of the ResNet-50 model, model evaluated on train set and validation set}
    \label{fig:resnet-50}
    \end{figure}
    
    % figure for efficientnet-b7
    \begin{figure}[htbp]
    \centering
    \includegraphics[width=0.5\textwidth]{figs/graphs/efficientnet-b7_metrics.pdf}
    \caption{Training progress of the EfficientNet-B7 model, model evaluated on train set validation set}
    \label{fig:efficientnet-b7}
    \end{figure}
    

We can see, that the training of all models is relatively
stable and the models do not overfit. Next we shall present the performance on the test set.

\subsection{Individual models} \label{sec:individual_models}
\subsubsection{ResNet50}
In ``Tab.~\ref{tab:resnet50}'' we can see the performance of the ResNet50 model on the test set without augmentations.
Model achieved accuracy of 0.63 and balanced accuracy of 0.64.



\begin{table}[htbp]
\centering
\caption{Performance of ResNet50 on test set}
\begin{tabular}{|l|l|l|l|l|}
\hline
\multicolumn{1}{|c||}{\textbf{Class}} & \multicolumn{1}{c|}{\textbf{Precision}} & \multicolumn{1}{c|}{\textbf{Recall}} & \multicolumn{1}{c|}{\textbf{F1-score}} & \multicolumn{1}{c|}{\textbf{Support}} \\ \hline
\multicolumn{1}{|c||}{AK} & \multicolumn{1}{c|}{0.27} & \multicolumn{1}{c|}{0.45} & \multicolumn{1}{c|}{0.33} & \multicolumn{1}{c|}{167} \\ \hline
\multicolumn{1}{|c||}{BCC} & \multicolumn{1}{c|}{0.69} & \multicolumn{1}{c|}{0.56} & \multicolumn{1}{c|}{0.62} & \multicolumn{1}{c|}{673} \\ \hline
\multicolumn{1}{|c||}{BKL} & \multicolumn{1}{c|}{0.35} & \multicolumn{1}{c|}{0.63} & \multicolumn{1}{c|}{0.45} & \multicolumn{1}{c|}{516} \\ \hline
\multicolumn{1}{|c||}{DF} & \multicolumn{1}{c|}{0.23} & \multicolumn{1}{c|}{0.89} & \multicolumn{1}{c|}{0.37} & \multicolumn{1}{c|}{45} \\ \hline
\multicolumn{1}{|c||}{MEL} & \multicolumn{1}{c|}{0.66} & \multicolumn{1}{c|}{0.40} & \multicolumn{1}{c|}{0.50} & \multicolumn{1}{c|}{910} \\ \hline
\multicolumn{1}{|c||}{NV} & \multicolumn{1}{c|}{0.87} & \multicolumn{1}{c|}{0.74} & \multicolumn{1}{c|}{0.80} & \multicolumn{1}{c|}{2568} \\ \hline
\multicolumn{1}{|c||}{SCC} & \multicolumn{1}{c|}{0.23} & \multicolumn{1}{c|}{0.57} & \multicolumn{1}{c|}{0.33} & \multicolumn{1}{c|}{128} \\ \hline
\multicolumn{1}{|c||}{VASC} & \multicolumn{1}{c|}{0.54} & \multicolumn{1}{c|}{0.90} & \multicolumn{1}{c|}{0.68} & \multicolumn{1}{c|}{60} \\ \hline
\hline
\multicolumn{1}{|c||}{accuracy} & \multicolumn{1}{c|}{} & \multicolumn{1}{c|}{} & \multicolumn{1}{c|}{0.63} & \multicolumn{1}{c|}{5067} \\ \hline
\multicolumn{1}{|c||}{macro avg} & \multicolumn{1}{c|}{0.48} & \multicolumn{1}{c|}{0.64} & \multicolumn{1}{c|}{0.51} & \multicolumn{1}{c|}{5067} \\ \hline
\multicolumn{1}{|c||}{weighted avg} & \multicolumn{1}{c|}{0.71} & \multicolumn{1}{c|}{0.63} & \multicolumn{1}{c|}{0.65} & \multicolumn{1}{c|}{5067} \\ \hline
\end{tabular}
\label{tab:resnet50}
\end{table}

\subsubsection{EfficientNet-B0}
In ``Tab.~\ref{tab:efficientnet-b0}'' we can see the performance of the EfficientNet-B0 model on the test set without augmentations.
Model achieved accuracy of 0.68 and balanced accuracy of 0.71.


\begin{table}[htbp]
\centering
\caption{Performance of EfficientNet-B0 on test set}
\begin{tabular}{|l|l|l|l|l|}
\hline
\multicolumn{1}{|c||}{\textbf{Class}} & \multicolumn{1}{c|}{\textbf{Precision}} & \multicolumn{1}{c|}{\textbf{Recall}} & \multicolumn{1}{c|}{\textbf{F1-score}} & \multicolumn{1}{c|}{\textbf{Support}} \\ \hline
\multicolumn{1}{|c||}{AK} & \multicolumn{1}{c|}{0.42} & \multicolumn{1}{c|}{0.59} & \multicolumn{1}{c|}{0.49} & \multicolumn{1}{c|}{167} \\ \hline
\multicolumn{1}{|c||}{BCC} & \multicolumn{1}{c|}{0.71} & \multicolumn{1}{c|}{0.72} & \multicolumn{1}{c|}{0.71} & \multicolumn{1}{c|}{673} \\ \hline
\multicolumn{1}{|c||}{BKL} & \multicolumn{1}{c|}{0.51} & \multicolumn{1}{c|}{0.60} & \multicolumn{1}{c|}{0.55} & \multicolumn{1}{c|}{516} \\ \hline
\multicolumn{1}{|c||}{DF} & \multicolumn{1}{c|}{0.56} & \multicolumn{1}{c|}{0.80} & \multicolumn{1}{c|}{0.66} & \multicolumn{1}{c|}{45} \\ \hline
\multicolumn{1}{|c||}{MEL} & \multicolumn{1}{c|}{0.49} & \multicolumn{1}{c|}{0.73} & \multicolumn{1}{c|}{0.59} & \multicolumn{1}{c|}{910} \\ \hline
\multicolumn{1}{|c||}{NV} & \multicolumn{1}{c|}{0.92} & \multicolumn{1}{c|}{0.66} & \multicolumn{1}{c|}{0.77} & \multicolumn{1}{c|}{2568} \\ \hline
\multicolumn{1}{|c||}{SCC} & \multicolumn{1}{c|}{0.39} & \multicolumn{1}{c|}{0.68} & \multicolumn{1}{c|}{0.49} & \multicolumn{1}{c|}{128} \\ \hline
\multicolumn{1}{|c||}{VASC} & \multicolumn{1}{c|}{0.90} & \multicolumn{1}{c|}{0.88} & \multicolumn{1}{c|}{0.89} & \multicolumn{1}{c|}{60} \\ \hline
\hline
\multicolumn{1}{|c||}{accuracy} & \multicolumn{1}{c|}{} & \multicolumn{1}{c|}{} & \multicolumn{1}{c|}{0.68} & \multicolumn{1}{c|}{5067} \\ \hline
\multicolumn{1}{|c||}{macro avg} & \multicolumn{1}{c|}{0.61} & \multicolumn{1}{c|}{0.71} & \multicolumn{1}{c|}{0.64} & \multicolumn{1}{c|}{5067} \\ \hline
\multicolumn{1}{|c||}{weighted avg} & \multicolumn{1}{c|}{0.74} & \multicolumn{1}{c|}{0.68} & \multicolumn{1}{c|}{0.69} & \multicolumn{1}{c|}{5067} \\ \hline
\end{tabular}
\label{tab:efficientnet-b0}
\end{table}

\subsubsection{EfficientNet-B7}
In ``Tab.~\ref{tab:efficientnet-b7}'' we can see the performance of the EfficientNet-B7 model on the test set without augmentations.
Model achieved accuracy of 0.73 and balanced accuracy of 0.74


\begin{table}[htbp]
\centering
\caption{Performacne of EfficientNet-B7 model on test dataset}
\begin{tabular}{|l|c|c|c|c|}
\hline
\multicolumn{1}{|c||}{\textbf{class}} & \multicolumn{1}{c|}{\textbf{precision}} & \multicolumn{1}{c|}{\textbf{recall}} & \multicolumn{1}{c|}{\textbf{f1-score}} & \multicolumn{1}{c|}{\textbf{support}} \\ \hline
\multicolumn{1}{|c||}{AK} & \multicolumn{1}{c|}{0.36} & \multicolumn{1}{c|}{0.71} & \multicolumn{1}{c|}{0.48} & \multicolumn{1}{c|}{167} \\ \hline
\multicolumn{1}{|c||}{BCC} & \multicolumn{1}{c|}{0.79} & \multicolumn{1}{c|}{0.70} & \multicolumn{1}{c|}{0.74} & \multicolumn{1}{c|}{673} \\ \hline
\multicolumn{1}{|c||}{BKL} & \multicolumn{1}{c|}{0.64} & \multicolumn{1}{c|}{0.57} & \multicolumn{1}{c|}{0.60} & \multicolumn{1}{c|}{516} \\ \hline
\multicolumn{1}{|c||}{DF} & \multicolumn{1}{c|}{0.33} & \multicolumn{1}{c|}{0.84} & \multicolumn{1}{c|}{0.47} & \multicolumn{1}{c|}{45} \\ \hline
\multicolumn{1}{|c||}{MEL} & \multicolumn{1}{c|}{0.61} & \multicolumn{1}{c|}{0.67} & \multicolumn{1}{c|}{0.64} & \multicolumn{1}{c|}{910} \\ \hline
\multicolumn{1}{|c||}{NV} & \multicolumn{1}{c|}{0.88} & \multicolumn{1}{c|}{0.79} & \multicolumn{1}{c|}{0.84} & \multicolumn{1}{c|}{2568} \\ \hline
\multicolumn{1}{|c||}{SCC} & \multicolumn{1}{c|}{0.55} & \multicolumn{1}{c|}{0.65} & \multicolumn{1}{c|}{0.59} & \multicolumn{1}{c|}{128} \\ \hline
\multicolumn{1}{|c||}{VASC} & \multicolumn{1}{c|}{0.49} & \multicolumn{1}{c|}{0.98} & \multicolumn{1}{c|}{0.65} & \multicolumn{1}{c|}{60} \\ \hline
\hline
\multicolumn{1}{|c||}{accuracy} & \multicolumn{1}{c|}{} & \multicolumn{1}{c|}{} & \multicolumn{1}{c|}{0.73} & \multicolumn{1}{c|}{5067} \\ \hline
\multicolumn{1}{|c||}{macro avg} & \multicolumn{1}{c|}{0.58} & \multicolumn{1}{c|}{0.74} & \multicolumn{1}{c|}{0.63} & \multicolumn{1}{c|}{5067} \\ \hline
\multicolumn{1}{|c||}{weighted avg} & \multicolumn{1}{c|}{0.76} & \multicolumn{1}{c|}{0.73} & \multicolumn{1}{c|}{0.74} & \multicolumn{1}{c|}{5067} \\ \hline
\end{tabular}
\label{tab:efficientnet-b7}
\end{table}

\subsection{Ensemble models}

The ensemble strategy is as follows.
Let's say we have an ensemble of $N$ CNN models. For a given input sample, let $x$ be the input feature vector, and let $y^*_i(x)$ be the predicted output of the $i$-th CNN model in the ensemble.
Then, the ensemble model's prediction $Y^*(x)$ can be written as the mean of the predicted outputs of all $N$ models, as follows:

    \begin{equation}
        Y^*(x) = \frac{1}{N} \sum_{i=1}^{N} y^*_i(x)
        \end{equation}
We use models from preveious section ``Sec.~\ref{sec:individual_models}'' 
to create ensemble models. We use the following combinations of models:
        \begin{itemize}
            \item ResNet50 + EfficientNet-B0 $\rightarrow$ Res50-Effb0
            \item ResNet50 + EfficientNet-B7 $\rightarrow$ Res50-Effb7
            \item EfficientNet-B0 + EfficientNet-B7 $\rightarrow$ Effb0-Effb7
            \item ResNet50 + EfficientNet-B0 + EfficientNet-B7 $\rightarrow$ Res50-Effb0-Effb7
        \end{itemize}
\subsubsection{Res50-Effb0}
In ``Tab.~\ref{tab:res50-effb0}'' we can see the performance of the Res50-Effb0 model on the test set without augmentations.
The model achieved accuracy of 0.70 and balanced accuracy of 0.72.


\begin{table}[htbp]
\centering
\caption{Classification report for Res50-Effb0 model}
\begin{tabular}{|c||c|c|c|c|}
\hline
\multicolumn{1}{|c||}{\textbf{Class}} & \multicolumn{1}{c|}{precision} & \multicolumn{1}{c|}{recall} & \multicolumn{1}{c|}{f1-score} & \multicolumn{1}{c|}{support} \\ \hline
\hline
\multicolumn{1}{|c||}{AK} & \multicolumn{1}{c|}{0.40} & \multicolumn{1}{c|}{0.57} & \multicolumn{1}{c|}{0.47} & \multicolumn{1}{c|}{167} \\ \hline
\multicolumn{1}{|c||}{BCC} & \multicolumn{1}{c|}{0.73} & \multicolumn{1}{c|}{0.71} & \multicolumn{1}{c|}{0.72} & \multicolumn{1}{c|}{673} \\ \hline
\multicolumn{1}{|c||}{BKL} & \multicolumn{1}{c|}{0.47} & \multicolumn{1}{c|}{0.64} & \multicolumn{1}{c|}{0.54} & \multicolumn{1}{c|}{516} \\ \hline
\multicolumn{1}{|c||}{DF} & \multicolumn{1}{c|}{0.50} & \multicolumn{1}{c|}{0.87} & \multicolumn{1}{c|}{0.63} & \multicolumn{1}{c|}{45} \\ \hline
\multicolumn{1}{|c||}{MEL} & \multicolumn{1}{c|}{0.60} & \multicolumn{1}{c|}{0.61} & \multicolumn{1}{c|}{0.61} & \multicolumn{1}{c|}{910} \\ \hline
\multicolumn{1}{|c||}{NV} & \multicolumn{1}{c|}{0.89} & \multicolumn{1}{c|}{0.75} & \multicolumn{1}{c|}{0.82} & \multicolumn{1}{c|}{2568} \\ \hline
\multicolumn{1}{|c||}{SCC} & \multicolumn{1}{c|}{0.37} & \multicolumn{1}{c|}{0.72} & \multicolumn{1}{c|}{0.49} & \multicolumn{1}{c|}{128} \\ \hline
\multicolumn{1}{|c||}{VASC} & \multicolumn{1}{c|}{0.79} & \multicolumn{1}{c|}{0.90} & \multicolumn{1}{c|}{0.84} & \multicolumn{1}{c|}{60} \\ \hline
\hline
\multicolumn{1}{|c||}{accuracy} & \multicolumn{1}{c|}{} & \multicolumn{1}{c|}{} & \multicolumn{1}{c|}{0.70} & \multicolumn{1}{c|}{5067} \\ \hline
\multicolumn{1}{|c||}{macro avg} & \multicolumn{1}{c|}{0.60} & \multicolumn{1}{c|}{0.72} & \multicolumn{1}{c|}{0.64} & \multicolumn{1}{c|}{5067} \\ \hline
\multicolumn{1}{|c||}{weighted avg} & \multicolumn{1}{c|}{0.74} & \multicolumn{1}{c|}{0.70} & \multicolumn{1}{c|}{0.72} & \multicolumn{1}{c|}{5067} \\ \hline
\end{tabular}
\label{tab:res50-effb0}
\end{table}

\subsubsection{Res50-Effb7}
In ``Tab.~\ref{tab:res50-effb7}'' we can see the performance of the Res50-Effb7 model on the test set without augmentations.
The model achieved accuracy of 0.74 and balanced accuracy of 0.77.


\begin{table}[htbp]
\centering
\caption{Performance of the Res50-Effb7 model on the test set}
\begin{tabular}{|l|c|c|c|c|}
\hline
\multicolumn{1}{|c||}{\textbf{class}} & \multicolumn{1}{c|}{\textbf{precision}} & \multicolumn{1}{c|}{\textbf{recall}} & \multicolumn{1}{c|}{\textbf{f1-score}} & \multicolumn{1}{c|}{\textbf{support}} \\ \hline
\hline
\multicolumn{1}{|c||}{AK} & \multicolumn{1}{c|}{0.40} & \multicolumn{1}{c|}{0.72} & \multicolumn{1}{c|}{0.51} & \multicolumn{1}{c|}{167} \\ \hline
\multicolumn{1}{|c||}{BCC} & \multicolumn{1}{c|}{0.79} & \multicolumn{1}{c|}{0.74} & \multicolumn{1}{c|}{0.77} & \multicolumn{1}{c|}{673} \\ \hline
\multicolumn{1}{|c||}{BKL} & \multicolumn{1}{c|}{0.57} & \multicolumn{1}{c|}{0.64} & \multicolumn{1}{c|}{0.60} & \multicolumn{1}{c|}{516} \\ \hline
\multicolumn{1}{|c||}{DF} & \multicolumn{1}{c|}{0.39} & \multicolumn{1}{c|}{0.93} & \multicolumn{1}{c|}{0.55} & \multicolumn{1}{c|}{45} \\ \hline
\multicolumn{1}{|c||}{MEL} & \multicolumn{1}{c|}{0.66} & \multicolumn{1}{c|}{0.61} & \multicolumn{1}{c|}{0.63} & \multicolumn{1}{c|}{910} \\ \hline
\multicolumn{1}{|c||}{NV} & \multicolumn{1}{c|}{0.89} & \multicolumn{1}{c|}{0.81} & \multicolumn{1}{c|}{0.85} & \multicolumn{1}{c|}{2568} \\ \hline
\multicolumn{1}{|c||}{SCC} & \multicolumn{1}{c|}{0.53} & \multicolumn{1}{c|}{0.70} & \multicolumn{1}{c|}{0.60} & \multicolumn{1}{c|}{128} \\ \hline
\multicolumn{1}{|c||}{VASC} & \multicolumn{1}{c|}{0.64} & \multicolumn{1}{c|}{0.97} & \multicolumn{1}{c|}{0.77} & \multicolumn{1}{c|}{60} \\ \hline
\hline
\multicolumn{1}{|c||}{accuracy} & \multicolumn{1}{c|}{} & \multicolumn{1}{c|}{} & \multicolumn{1}{c|}{0.74} & \multicolumn{1}{c|}{5067} \\ \hline
\multicolumn{1}{|c||}{macro avg} & \multicolumn{1}{c|}{0.61} & \multicolumn{1}{c|}{0.77} & \multicolumn{1}{c|}{0.66} & \multicolumn{1}{c|}{5067} \\ \hline
\multicolumn{1}{|c||}{weighted avg} & \multicolumn{1}{c|}{0.77} & \multicolumn{1}{c|}{0.74} & \multicolumn{1}{c|}{0.75} & \multicolumn{1}{c|}{5067} \\ \hline
\end{tabular}
\label{tab:res50-effb7}
\end{table}

\subsubsection{Effb0-Effb7}
In ``Tab.~\ref{tab:effb0-effb7}'' we can see the performance of the Effb0-Effb7 model on the test set.
The model achieved accuracy of 0.75 and balanced accuracy of 0.78.



\begin{table}[htbp]
\centering
\caption{Effb0-Effb7 model performance on the test set}
\begin{tabular}{|c||c|c|c|c|}
\hline
\multicolumn{1}{|c||}{\textbf{Class}} & \multicolumn{1}{c|}{\textbf{Precision}} & \multicolumn{1}{c|}{\textbf{Recall}} & \multicolumn{1}{c|}{\textbf{F1-score}} & \multicolumn{1}{c|}{\textbf{Support}} \\ \hline
\multicolumn{1}{|c||}{AK} & \multicolumn{1}{c|}{0.49} & \multicolumn{1}{c|}{0.69} & \multicolumn{1}{c|}{0.57} & \multicolumn{1}{c|}{167} \\ \hline
\multicolumn{1}{|c||}{BCC} & \multicolumn{1}{c|}{0.78} & \multicolumn{1}{c|}{0.79} & \multicolumn{1}{c|}{0.79} & \multicolumn{1}{c|}{673} \\ \hline
\multicolumn{1}{|c||}{BKL} & \multicolumn{1}{c|}{0.64} & \multicolumn{1}{c|}{0.63} & \multicolumn{1}{c|}{0.63} & \multicolumn{1}{c|}{516} \\ \hline
\multicolumn{1}{|c||}{DF} & \multicolumn{1}{c|}{0.55} & \multicolumn{1}{c|}{0.91} & \multicolumn{1}{c|}{0.68} & \multicolumn{1}{c|}{45} \\ \hline
\multicolumn{1}{|c||}{MEL} & \multicolumn{1}{c|}{0.59} & \multicolumn{1}{c|}{0.73} & \multicolumn{1}{c|}{0.65} & \multicolumn{1}{c|}{910} \\ \hline
\multicolumn{1}{|c||}{NV} & \multicolumn{1}{c|}{0.90} & \multicolumn{1}{c|}{0.77} & \multicolumn{1}{c|}{0.83} & \multicolumn{1}{c|}{2568} \\ \hline
\multicolumn{1}{|c||}{SCC} & \multicolumn{1}{c|}{0.59} & \multicolumn{1}{c|}{0.75} & \multicolumn{1}{c|}{0.66} & \multicolumn{1}{c|}{128} \\ \hline
\multicolumn{1}{|c||}{VASC} & \multicolumn{1}{c|}{0.75} & \multicolumn{1}{c|}{0.97} & \multicolumn{1}{c|}{0.85} & \multicolumn{1}{c|}{60} \\ \hline
\multicolumn{1}{|c||}{accuracy} & \multicolumn{1}{c|}{} & \multicolumn{1}{c|}{} & \multicolumn{1}{c|}{0.75} & \multicolumn{1}{c|}{5067} \\ \hline
\multicolumn{1}{|c||}{macro avg} & \multicolumn{1}{c|}{0.66} & \multicolumn{1}{c|}{0.78} & \multicolumn{1}{c|}{0.71} & \multicolumn{1}{c|}{5067} \\ \hline
\multicolumn{1}{|c||}{weighted avg} & \multicolumn{1}{c|}{0.77} & \multicolumn{1}{c|}{0.75} & \multicolumn{1}{c|}{0.76} & \multicolumn{1}{c|}{5067} \\ \hline
\end{tabular}
\label{tab:effb0-effb7}
\end{table}

\subsubsection{Res50-Effb0-Effb7}
Lastly, we made an ensemble from all individual models. The performance can be seen in ``Tab.~\ref{tab:ensemble_all}''.
This model achieved accuracy of 0.75 and balanced accuracy of 0.77.


\begin{table}[htbp]
\centering
\caption{Res50-Effb0-Effb7 model performance on the test set}
\begin{tabular}{|c|c|c|c|c|}
\hline
\multicolumn{1}{|c||}{Class} & \multicolumn{1}{c|}{precision} & \multicolumn{1}{c|}{recall} & \multicolumn{1}{c|}{f1-score} & \multicolumn{1}{c|}{support} \\ \hline
\multicolumn{1}{|c||}{AK} & \multicolumn{1}{c|}{0.46} & \multicolumn{1}{c|}{0.68} & \multicolumn{1}{c|}{0.55} & \multicolumn{1}{c|}{167} \\ \hline
\multicolumn{1}{|c||}{BCC} & \multicolumn{1}{c|}{0.77} & \multicolumn{1}{c|}{0.78} & \multicolumn{1}{c|}{0.78} & \multicolumn{1}{c|}{673} \\ \hline
\multicolumn{1}{|c||}{BKL} & \multicolumn{1}{c|}{0.57} & \multicolumn{1}{c|}{0.64} & \multicolumn{1}{c|}{0.61} & \multicolumn{1}{c|}{516} \\ \hline
\multicolumn{1}{|c||}{DF} & \multicolumn{1}{c|}{0.53} & \multicolumn{1}{c|}{0.91} & \multicolumn{1}{c|}{0.67} & \multicolumn{1}{c|}{45} \\ \hline
\multicolumn{1}{|c||}{MEL} & \multicolumn{1}{c|}{0.62} & \multicolumn{1}{c|}{0.66} & \multicolumn{1}{c|}{0.64} & \multicolumn{1}{c|}{910} \\ \hline
\multicolumn{1}{|c||}{NV} & \multicolumn{1}{c|}{0.89} & \multicolumn{1}{c|}{0.79} & \multicolumn{1}{c|}{0.84} & \multicolumn{1}{c|}{2568} \\ \hline
\multicolumn{1}{|c||}{SCC} & \multicolumn{1}{c|}{0.54} & \multicolumn{1}{c|}{0.76} & \multicolumn{1}{c|}{0.63} & \multicolumn{1}{c|}{128} \\ \hline
\multicolumn{1}{|c||}{VASC} & \multicolumn{1}{c|}{0.78} & \multicolumn{1}{c|}{0.95} & \multicolumn{1}{c|}{0.86} & \multicolumn{1}{c|}{60} \\ \hline
\hline
\multicolumn{1}{|c||}{accuracy} & \multicolumn{1}{c|}{} & \multicolumn{1}{c|}{} & \multicolumn{1}{c|}{0.75} & \multicolumn{1}{c|}{5067} \\ \hline
\multicolumn{1}{|c||}{macro avg} & \multicolumn{1}{c|}{0.65} & \multicolumn{1}{c|}{0.77} & \multicolumn{1}{c|}{0.70} & \multicolumn{1}{c|}{5067} \\ \hline
\multicolumn{1}{|c||}{weighted avg} & \multicolumn{1}{c|}{0.77} & \multicolumn{1}{c|}{0.75} & \multicolumn{1}{c|}{0.75} & \multicolumn{1}{c|}{5067} \\ \hline
\end{tabular}
\label{tab:ensemble_all}
\end{table}

\subsubsection{Conclusion on the performance of the models}
From the previous, we can form the table ``Tab.~\ref{tab:models}'' which 
ranks the models based on their performance.

% make table from previous list
\begin{table}[htbp]
\centering
\caption{Performance of the models}
\begin{tabular}{c|c}
\hline
\multicolumn{1}{|c||}{Model} & \multicolumn{1}{c|}{Balanced accuracy} \\ \hline
\multicolumn{1}{|c||}{Effb0-Effb7} & \multicolumn{1}{c|}{0.78} \\ \hline
\multicolumn{1}{|c||}{Res50-Effb0-Effb7} & \multicolumn{1}{c|}{0.77} \\ \hline
\multicolumn{1}{|c||}{Res50-Effb7} & \multicolumn{1}{c|}{0.77} \\ \hline
\multicolumn{1}{|c||}{EfficientNetB7} & \multicolumn{1}{c|}{0.74} \\ \hline
\multicolumn{1}{|c||}{Res50-Effb0} & \multicolumn{1}{c|}{0.72} \\ \hline
\multicolumn{1}{|c||}{EfficientNetB0} & \multicolumn{1}{c|}{0.71} \\ \hline
\multicolumn{1}{|c||}{ResNet} & \multicolumn{1}{c|}{0.64} \\ \hline
\end{tabular}
\label{tab:models}
\end{table}



The method of creating ensembles of the models shows better results than most of the individual models.
The best performing ensemble is ensemble created from models EfficientNetB0 and EfficientNetB7 - Effb0-Effb7, which 
achieves balanced accuracy of 0.78 compared to best individual model EfficientNetB7 with balanced accuracy of 0.74.

The downside of using ensembles is the increase in the inference time.

\subsection{Comparison with the state of the art} \label{sec:state_of_the_art}
Best solution in the ISIC 2019 challenge by N. Gessert et al.~\cite{gessert_skin_nodate}
trained various models and formed an ensemble from them. They report sensitivity of 72.5\% and AUC 95.4\%.
However, they trained their models also on unknown class which is not present in our dataset. 
Only model that is comparable to our models from this work is EfficientNetB0, which achieves sensitivity of 66.7\% and AUC 94.0\%.
This is the only model trained on 8 classes from their work.
Our EfficientNetB0 achieves sensitivity of 70.7\% and AUC 93.7. 

Second place in the ISIC 2019 by S. Zhou et al. \cite{zhou_multi-category_nodate} trained 
multiple models and formed an ensemble from them. This work presents results for 8 classes. Their best performing infividual model - SeResNext101 
achieves sensitivity 73.9 \%. Our best performing individual model EfficientNetB7 achieves same sensitivity of 73.9\%.
Their best performing ensemble formed from models EfficientNet-B2, EfficientNet-B4 and DenseNet121 
achieves sensitivity of 75.3\%. Our best performing ensemble Effb0-Effb7 is better with sensitivity of 77.9\%.


\section{Impact of augmentations applied to test dataset on the model performance}
While performance on the test set is important, it is also important to see how the 
model behaves on the data which is augmented, which simulates the real world data.
Therefore we run the models on whole test dataset with particular augmentation applied to it,
then we evaluate the performance and increase/decrease the augmentation strength and evaluate the performance again.
We describe the results of the experiments and discuss them.


\subsection{Image size} \label{sec:size}
We study the impact of the image size on our models. We simulated the change of image size by applying the resize augmentation to the test dataset
starting with the size 76x76 up to 256x256. We increment the size each time by 10 pixels. The impact on balanced accuracy on the test dataset is shown in figure ``Fig.~\ref{fig:size}''.
% figure for resolution
\begin{figure}[htbp]
\centering
\includegraphics[width=0.5\textwidth]{figs/graphs/size/bal_acc.pdf}
\caption{Impact of the image size (Image size x Image size) on the balanced accuracy of the models}
\label{fig:size}
\end{figure}
It may be seen, that size change has big impact on EfficientNetB7 model and smaller impact on the ResNet50 and EfficientNetB0 models.
As consequence, the ensemble models featuring EfficientNetB7 are more sensitive to the size change than the ensemble models without EfficientNetB7.
% We can see, that while the balanced accuracy of the EfficientNet-B0 is better than the 
% ResNet50 for higher resolutions, the ResNet50 is better for lower resolutions.
% The ensemble model, which we call Res50-Effb0 is better than both models for all resolutions.


\subsection{Resolution}
While resizing changes resolution of the image, it also changes the aspect ratio of the image.
In this section we study impact of resolution change without changing the aspect ratio of the image.
We accomplished this by applying downscaling followed by upscaling of the particular images in the test dataset.

The results of the augmentation may be seen in figure ``Fig.~\ref{fig:resolution}''. 
% We can see, that while the balanced accuracy of the models is stable from the from resolution of 0.2x original resolution.
% The resolution of 0.2x the original image is 51.2 x 51.2 pixels before upscaling. At this size, the models' performance studied in previous section \ref{sec:size} was low, and therefore not included in 
% the figure ``Fig.~\ref{fig:size}''. 

% THe balanced accuracy for size (which we studied in previous section \ref{sec:size}) ,at 51x51 
% is 0.161 for EfficientNet-B0, 0.178 for ResNet-50 and 0.215 for Res50-Effb0.
% The balanced accuracy of image that uses just downscaling and upscaling at downscaling factor of 0.2
% is 0.683 for EfficientNet-B0, 0.626 for ResNet-50 and 0.691 for Res50-Effb0. 
The results show that the models are robust to the resolution change, and the ensemble models tend to perform better than the individual models.
We can conclude that it is better to upscale your low resolution images, than to use the low resolution images directly.




% figure for resolution
\begin{figure}[htbp]
\centering
\includegraphics[width=0.5\textwidth]{figs/graphs/resolution/bal_acc.pdf}
\caption{Impact of the resolution change on the balanced accuracy of the models}
\label{fig:resolution}
\end{figure}


\subsection{Brightness}
In this section we study the impact of the brightness of the images on the model performance.
We simulated the change of brightness of the images by applying the brightness change augmentation to the test dataset
through the range of [-1, 1], where -1 is the smallest brightness, and 1 is maximum brightness, with step 0.05. The results in form of balanced accuracy on the test dataset are shown in figure ``Fig.~\ref{fig:brightness}''.

% figure for brightness
\begin{figure}[htbp]
\centering
\includegraphics[width=0.5\textwidth]{figs/graphs/brightness/bal_acc.pdf}
\caption{Impact of the brightness change on the balanced accuracy of the models}
\label{fig:brightness}
\end{figure}




\subsection{Contrast}
We simulated the change of contrast of the images by applying the contrast change augmentation to the test dataset.
We specified different values at which the contrast change is applied, in a range [-1, 1] (-1 minimum, 1 maximum) with a step of 0.05.
The results in form of balanced accuracy on the test dataset are shown in figure ``Fig.~\ref{fig:contrast}''.

The results show that, while the images with low contrast are harder to classify, the slight increase in contrast may improve the balanced accuracy.%improves the performance of the ResNet50 and Res50-Effb0 models.

% figure for contrast
\begin{figure}[htbp]
\centering
\includegraphics[width=0.5\textwidth]{figs/graphs/contrast/bal_acc.pdf}
\caption{Impact of the contrast change on the balanced accuracy of the models}
\label{fig:contrast}
\end{figure}


\subsection{Blur}
We simulated the blur of the images by applying the blur augmentation to the test dataset.
We specified different values at which the blur is applied, in a range [1, 25] with a step of 1.

The results in form of balanced accuracy on the test dataset are shown in figure ``Fig.~\ref{fig:blur}''.
It may be seen, that the blur has a significant impact on the performance of the models.
The worst performance is achieved by the EfficientNetB7 model, which has the best performance on 
test set, but the worst performance on the blurred test set after the value of 5.

% figure for blur
\begin{figure}[htbp]
\centering
\includegraphics[width=0.5\textwidth]{figs/graphs/blur/bal_acc.pdf}
\caption{Impact of the blur on the balanced accuracy of the models}
\label{fig:blur}
\end{figure}

\subsection{Color}
We simulated the change of color of the images by applying the RGB shift augmentation to the test dataset.
We tested with shift of 0 or $\pm 50$ applied to various channels. We tested all combinations of the shifts.
The results in form of balanced accuracy on the test dataset are shown in figure ``Fig.~\ref{fig:color}''.

We can see, that the EfficientNetB7 model's response to the color change is 
significantly better than the response of the other models. In some cases,
it is better than the response of the ensemble models. 
The highest balanced accuracy is achieved when we shift all channels the same amount in the same direction.
It is expected, as changing all channels the same amount in the same direction is equivalent to changing the brightness of the image.
The worst performance is achieved when 2 channels are shifted in the same direction, and the third channel is shifted in the opposite direction.


% figure for color shift spans on whole page, it is tilted 90 degrees
\begin{sidewaysfigure*}[htbp]
\centering
\includegraphics[width=1.\textwidth]{figs/graphs/color/bal_acc.pdf}
% rotate the caption 90 degrees
\caption{Impact of the color shift on the balanced accuracy of the models}
\label{fig:color}
\end{sidewaysfigure*}




\section{Conclusion}

We trained various models on the ISIC 2019 dataset and formed an ensemble from them.
The best performing individual model is EfficientNetB7, which achieves sensitivity of 73.9\%.
The best performing ensemble is formed from EfficientNetB0 to EfficientNetB7 and achieves sensitivity of 77.9\%.
These results are better than the results of the best two solutions in the ISIC 2019 challenge.
Although, ISIC2019 includes classification of Unknown class, which is not present in our dataset,
the best performing models report scores also without the Unknown class.
We also studied the robustness of the models to various augmentations applied to the test dataset.
Examples of these augmentations are shown in figure ``Fig.~\ref{fig:augmentations}''.
We found that the models are robust to the resolution change. 
The size of the images has significant impact on the performance of the models and 
it is suggested to upscale the low sized images. The best performance models achieve
with the size of 256x256 pixels.
slight increase in contrast may improve the balanced accuracy of some models.
The brightness change has significant impact on the performance of the models,
when the brightness is $\pm 50\%$ the balanced accuracy drops below 50\% for all models.
The models' response to blur change results in drop of the balanced accuracy for all models, starting from low values of blur.
During the experiments with color, we found, that models are connecting the colors with the classes,
it is expected as the color of the lesions is one of the most important features also used 
in the medical diagnosis.


We found that ensemble models are generally more robust to the augmentations than the individual models.

% figure for augmentations
\begin{figure}[htbp]
\centering
\includegraphics[width=0.5\textwidth]{figs/tmp/final_image.png}
\caption{Examples of augmentations applied to the test dataset. From left to right; Upper row:
original image, size change (100x100), resolution change(0.4, equivalent to $\sim $ 100x100), brightness ($+20\%$),
Lower row: 
contrast ($+20\%$), blur (10), color shift (+50, 0, +50)  }
\label{fig:augmentations}
\end{figure}




 


\newpage
\printbibliography

\end{document}