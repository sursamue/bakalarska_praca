# generate imports
import pandas as pd
import sys
import os
import argparse



# check if output folder has subfolders named 'groundtruths' and 'detections'
# if not, create them
def get_gt_det_folder(output_folder):
    if not os.path.exists(output_folder + 'groundtruths/'):
        os.makedirs(output_folder + 'groundtruths/')
    if not os.path.exists(output_folder + 'detections/'):
        os.makedirs(output_folder + 'detections/')
    # return gt folder and det folder
    return output_folder + 'groundtruths/', output_folder + 'detections/'

# chceck if output folders empty
def check_if_output_folder_empty(gt_folder, det_folder):
    is_empty_gt_folder = True
    is_empty_det_folder = True
    if len(os.listdir(gt_folder)) != 0:
        print('The ground truth folder is not empty.')
        is_empty_gt_folder = False
    if len(os.listdir(det_folder)) != 0:
        print('The detection folder is not empty.')
        is_empty_det_folder = False
    return is_empty_gt_folder, is_empty_det_folder
    
# function to delete all files in the otuput folders but ask the user first
def delete_all_files_in_output_folder(gt_folder, det_folder):
    # ask user if he wants to delete all files in the output folder

    answer = input('Do you want to delete all files in the output folder? (y/n)')
    if answer == 'y':
        # delete all files in the output folder
        for file in os.listdir(gt_folder):
            os.remove(gt_folder + file)
        for file in os.listdir(det_folder):
            os.remove(det_folder + file)
        print('All files have been deleted from the output folder.')
    else:
        print('No files have been deleted from the output folder.')
        print('Exiting the program.')
        sys.exit()

    print('The output folders are now empty.')


# function to add scan_id, slice_id to the prediction_df based on same image_id as in the ground truth df
def add_scan_id_slice_id(df, pred_df):
    for index, row in df.iterrows():
        pred_df.loc[pred_df.image_id == row['image_id'], 'scan_id'] = row['scan_id']
        pred_df.loc[pred_df.image_id == row['image_id'], 'slice_id'] = row['slice_id']
    return pred_df
    
def convert_gt_df_to_txt(df, path):
    for scan_id in df.scan_id.unique():
        for slice_id in df[df.scan_id == scan_id].slice_id.unique():
            if df[(df.scan_id == scan_id) & (df.slice_id == slice_id)].has_annotation.values[0]:
                file = open(path + scan_id + '_' + str(slice_id) + '.txt', 'w')
                for index, row in df[(df.scan_id == scan_id) & (df.slice_id == slice_id)].iterrows():
                    file.write('skin_lesion' + ' ' + str(row['xmin']) + ' ' + str(row['ymin']) + ' ' + str(row['xmax']) + ' ' + str(row['ymax'])+ '\n')
            else:
                file = open(path + scan_id + '_' + str(slice_id) + '.txt', 'w')
                file.close()

# convert_gt_df_to_txt but convert bbox coordinates to integers
def convert_gt_df_to_txt_int(df, path):
    for scan_id in df.scan_id.unique():
        for slice_id in df[df.scan_id == scan_id].slice_id.unique():
            if df[(df.scan_id == scan_id) & (df.slice_id == slice_id)].has_annotation.values[0]:
                file = open(path + scan_id + '_' + str(slice_id) + '.txt', 'w')
                for index, row in df[(df.scan_id == scan_id) & (df.slice_id == slice_id)].iterrows():
                    file.write('skin_lesion' + ' ' + str(int(row['xmin'])) + ' ' + str(int(row['ymin'])) + ' ' + str(int(row['xmax'])) + ' ' + str(int(row['ymax']))+ '\n')
            else:
                file = open(path + scan_id + '_' + str(slice_id) + '.txt', 'w')
                file.close()


#function to convert the prediction_df to txt format, but also create empty txt files for slices without predictions which are not part of the prediction_df but are part of the ground_truth_df. 
#beware of error when the slice is not part of the prediction_df but is part of the ground_truth_df
def convert_pred_df_to_txt(df_pred, df_gt, path):
    for index, row in df_gt.iterrows():
        if row['image_id'] in df_pred.image_id.unique():
            file = open(path + row['scan_id'] + '_' + str(int(row['slice_id'])) + '.txt', 'w')
            for index, row in df_pred[df_pred.image_id == row['image_id']].iterrows():
                file.write('skin_lesion' + ' ' + str(row['score']) + ' ' + str(row['xmin']) + ' ' + str(row['ymin']) + ' ' + str(row['xmax']) + ' ' + str(row['ymax'])+ '\n')
            file.close()
        else:
            file = open(path + row['scan_id'] + '_' + str(int(row['slice_id'])) + '.txt', 'w')
            file.close()

# convert_pred_df_to_txt but convert bbox coordinates to integers
def convert_pred_df_to_txt_int(df_pred, df_gt, path):
    for index, row in df_gt.iterrows():
        if row['image_id'] in df_pred.image_id.unique():
            file = open(path + row['scan_id'] + '_' + str(int(row['slice_id'])) + '.txt', 'w')
            for index, row in df_pred[df_pred.image_id == row['image_id']].iterrows():
                file.write('skin_lesion' + ' ' + str(row['score']) + ' ' + str(int(row['xmin'])) + ' ' + str(int(row['ymin'])) + ' ' + str(int(row['xmax'])) + ' ' + str(int(row['ymax']))+ '\n')
            file.close()
        else:
            file = open(path + row['scan_id'] + '_' + str(int(row['slice_id'])) + '.txt', 'w')
            file.close()
    

def main(gt, pred, out):

    # check if output folder has subfolders named 'groundtruths' and 'detections'
    # if not, create them
    gt_folder, det_folder = get_gt_det_folder(out)
    # check if the subfolders are empty
    is_empty_gt_folder, is_empty_det_folder = check_if_output_folder_empty(gt_folder, det_folder)
    # if the subfolders are not empty, run delete function
    if is_empty_gt_folder == False or is_empty_det_folder == False:
        delete_all_files_in_output_folder(gt_folder, det_folder)
    # read ground truth csv file
    gt_df = pd.read_csv(gt, converters = {'scan_id':str})
    # read prediction json file
    pred_df = pd.read_csv(pred)
    # add scan_id, slice_id to the prediction_df based on same image_id as in the ground truth df
    print('Modulating the prediction csv file to match the ground truth csv file.')
    pred_df = add_scan_id_slice_id(gt_df, pred_df)
    # convert the ground truth df to txt format
    print('Converting ground truth csv file to txt format.')
    # if args.int:
    # #     convert_gt_df_to_txt_int(gt_df, gt_folder)
    # else:
    convert_gt_df_to_txt(gt_df, gt_folder)
    # convert the prediction_df to txt format
    print('Converting prediction csv file to txt format.')
    # if args.int:
    #     convert_pred_df_to_txt_int(pred_df, gt_df, det_folder)
    # else:
    convert_pred_df_to_txt(pred_df, gt_df, det_folder)
    print('Done.')



# make the file to be callable from the command line
if __name__ == '__main__':

    # crate argparser with arguments as -gt, -pred, -out -h for help and optional -int for converting bbox coordinates to integers
    parser = argparse.ArgumentParser(description='Convert ground truth csv file and prediction csv file to txt files which can be used for evaluation.')
    parser.add_argument('-gt', '--ground_truth', help='The ground truth csv file.', required=True)
    parser.add_argument('-pred', '--prediction', help='The prediction csv file.', required=True)
    parser.add_argument('-out', '--output', help='The output path where the txt files will be saved.', required=True)
    # parser.add_argument('-int', '--int', help='Convert bbox coordinates to integers.', action='store_true')
    # parse arguments
    args = parser.parse_args()

    # run main function but int is optional
    main(args.ground_truth, args.prediction, args.output)