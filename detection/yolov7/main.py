

import pandas as pd
import numpy as np
import cv2
import os
import re
import ast
import sys


from func_to_script import script
from functools import partial


from yolov7 import create_yolov7_model
from yolov7.dataset import Yolov7Dataset, create_yolov7_transforms, yolov7_collate_fn, create_base_transforms
from yolov7.evaluation import CalculateMeanAveragePrecisionCallback
from yolov7.loss_factory import create_yolov7_loss
from yolov7.trainer import Yolov7Trainer, filter_eval_predictions
from yolov7.mosaic import MosaicMixupDataset, create_post_mosaic_transform
from yolov7.trainer import Yolov7Trainer, filter_eval_predictions
from yolov7.utils import SaveBatchesCallback, Yolov7ModelEma

import pickle

from pytorch_accelerated.callbacks import (
    EarlyStoppingCallback,
    SaveBestModelCallback,
    get_default_callbacks,
    ModelEmaCallback
)

from pytorch_accelerated.schedulers import CosineLrScheduler

from pathlib import Path

from PIL import Image
from torch.utils.data import Dataset


import albumentations as A
from albumentations.pytorch.transforms import ToTensorV2

import torch
import torchvision

from torchvision.models.detection.faster_rcnn import FastRCNNPredictor
from torchvision.models.detection import FasterRCNN

from torch.utils.data import DataLoader, Dataset
from torch.utils.data.sampler import SequentialSampler

from matplotlib import pyplot as plt
from matplotlib import patches


from tqdm import tqdm

from multiprocessing import Pool
from typing import List



sys.path.insert(0, os.path.abspath(os.path.join(os.getcwd(), '/ssd/temporary/sursamue/skin/skin3d')))
from skin3d.bodytex import BodyTexDataset

from skin3d.visualize import embed_annotatations
from skin3d.visualize import embed_box_borders



import dataset_wrapper as dw


def load_bodytex_dataset():
    """
    Loads BodyTexDataset and returns it.
    This function basically acts as a wrapper for the BodyTexDataset.
    """

    # File path of the bodytex CSV.
    bodytex_csv = '/datagrid/Medical/skin/skin3d/data/3dbodytex-1.1-highres/bodytex.csv'
    bodytex_df = pd.read_csv(bodytex_csv, converters={'scan_id': lambda x: str(x)})
    bodytex = BodyTexDataset(
        df=bodytex_df, 
        dir_annotate='/datagrid/Medical/skin/skin3d/data/3dbodytex-1.1-highres/annotations',
        dir_multi_annotate='/datagrid/Medical/skin/skin3d/data/3dbodytex-1.1-highres/multiple_annotators',
        dir_textures='/datagrid/Medical/skin/3dbodytex-1.1-highres'
        )
    return bodytex


# def save_im_with_bboxes(im, boxes, path):
#     """Function to save image with bboxes, it acts as a helper function"""
    
#     for _, row in enumerate(boxes):

#         color = [0,0,255]
#         pad = 2
#         x = int(row[0])
#         y = int(row[1])
#         width = int(row[2]-row[0])
#         height = int(row[3]-row[1])
#         embed_box_borders(im, x, y, width, height, color, pad)
#     im = Image.fromarray(im).convert('RGB')
#     im.save(path, 'PNG')

def get_image_size(image):
    """Function to get image size in h x w format"""
    # this is just a helper func
    w, h = image.size
    return {"image_height": h, "image_width": w}


def save_im_with_bboxes(im, boxes, path):
    """Function to save image with bboxes, it acts as a helper function"""
    
    for _, row in enumerate(boxes):

        color = [0,0,255]
        pad = 2
        x = int(row[0])
        y = int(row[1])
        width = int(row[2]-row[0])
        height = int(row[3]-row[1])
        embed_box_borders(im, x, y, width, height, color, pad)
    im = Image.fromarray(im).convert('RGB')
    im.save(path, 'PNG')


def calculate_slice_bboxes(
    image_height: int = 4096,
    image_width: int = 4096,
    slice_height: int = 800,
    slice_width: int = 800,
    overlap_height_ratio: float = 0.2,
    overlap_width_ratio: float = 0.2,
) -> List[List[int]]:
    """
    Given the height and width of an image, calculates how to divide the image into
    overlapping slices according to the height and width provided. These slices are returned
    as bounding boxes in xyxy format.

    :param image_height: Height of the original image.
    :param image_width: Width of the original image.
    :param slice_height: Height of each slice
    :param slice_width: Width of each slice
    :param overlap_height_ratio: Fractional overlap in height of each slice (e.g. an overlap of 0.2 for a slice of size 100 yields an overlap of 20 pixels)
    :param overlap_width_ratio: Fractional overlap in width of each slice (e.g. an overlap of 0.2 for a slice of size 100 yields an overlap of 20 pixels)
    :return: a list of bounding boxes in xyxy format
    """

    slice_bboxes = []
    y_max = y_min = 0
    y_overlap = int(overlap_height_ratio * slice_height)
    x_overlap = int(overlap_width_ratio * slice_width)
    while y_max < image_height:
        x_min = x_max = 0
        y_max = y_min + slice_height
        while x_max < image_width:
            x_max = x_min + slice_width
            if y_max > image_height or x_max > image_width:
                xmax = min(image_width, x_max)
                ymax = min(image_height, y_max)
                xmin = max(0, xmax - slice_width)
                ymin = max(0, ymax - slice_height)
                slice_bboxes.append([xmin, ymin, xmax, ymax])
            else:
                slice_bboxes.append([x_min, y_min, x_max, y_max])
            x_min = x_max - x_overlap
        y_min = y_max - y_overlap
    return slice_bboxes




def save_image_from_tensor(image, path):
    """
    Function to save image which is in tensor format.
    """
    image = image.permute(1, 2, 0).numpy()*255
    image = image.astype(np.uint8)
    image = Image.fromarray(image).convert("RGB")
    image.save(path, "PNG")

def save_im_with_bboxes_from_tensor(im, boxes, path):
    """
    Function to save image with bboxes, from tensor format.
    """

    im = im.permute(1, 2, 0).numpy()*255
    im = im.astype(np.uint8)
    for _, row in enumerate(boxes):

        color = [0,0,255]
        pad = 2
        x, y, w, h = yolo_to_xywh(row)
        x = int(x)
        y = int(y)
        width = int(w)
        height = int(h)
        embed_box_borders(im, x, y, width, height, color, pad)
    im = Image.fromarray(im).convert('RGB')
    im.save(path, 'PNG')

# function to convert yolo bbox format to xywh format
def yolo_to_xywh(yolo_bbox):
    x = yolo_bbox[0]
    y = yolo_bbox[1]
    w = yolo_bbox[2]
    h = yolo_bbox[3]
    x = x - w/2
    y = y - h/2
    return [x,y,w,h]



class BodyTexPreprocessed(Dataset):
    """ 
    Dataset class for BodyTexPreprocessed dataset.
    """
    def __init__(self, dataframe, dataframe_expanded, transforms=None, partition = 'train', annotator = None):
        self.dataframe = dataframe
        self.dataframe_expanded = dataframe_expanded
        self.slice_height = 800
        self.slice_width = 800
        self.transforms = transforms
        self.partition = partition
        self.annotator = annotator
        self.bbox_min_area=0.1,
        self.bbox_min_visibility=0.1,
        if self.annotator is None:
            self.dir_textures = os.path.join('/ssd/temporary/sursamue/skin/3dbodytex-1.1-highres_preprocessed/', self.partition)
        else:
            self.dir_textures = os.path.join('/ssd/temporary/sursamue/skin/3dbodytex-1.1-highres_preprocessed/', self.partition, self.annotator)
        self.image_idx_to_image_id = {
            idx: int(image_id)
            for idx, image_id in enumerate(self.dataframe.image_id)
        }
        self.image_id_to_image_idx = {
            v: k for k, v, in self.image_idx_to_image_id.items()
        }
    def __len__(self):
        """Return the number of images in the dataset."""
        return len(self.dataframe)
    

    def texture_filepath(self,
            scan_id, slice_id) -> str:
        """ Return the filepath to the texture image for the given `scan_id`, 'slice_id'.

            Args:
                scan_id (string): A string representing scan identifier
                    (i.e., `self.df.scan_id`).
                slice_id (int): A string representing slice identifier
                    (i.e., `self.df.slice_id`).
        
            Returns:
                string: Filepath to the texture image.
        """

        name = (scan_id +'_'+ str(slice_id) + '.png')
        return os.path.join(self.dir_textures, name)

    def texture_image(self, scan_id, slice_id) -> Image:
        """Return the texture image corresponding to `scan_id."""
        img_path = self.texture_filepath(scan_id=scan_id, slice_id = slice_id)
        img = Image.open(img_path).convert('RGB')
        return img

    def __getitem__(self, idx):
        # create same __gettime__ fucntion as in bodytexslices dataset
        image_id = self.image_idx_to_image_id[idx]
        image_id = idx

  
        row = self.dataframe.iloc[idx]
        row_expanded = self.dataframe_expanded.loc[(self.dataframe_expanded.scan_id == row.scan_id) &  (self.dataframe_expanded.slice_id == row.slice_id)]

        image = np.asarray(self.texture_image(row.scan_id, row.slice_id), dtype=np.uint8).copy()
        if row.has_annotation == True:
            annotations = row_expanded[["xmin", "ymin", "xmax", "ymax"]].values
            bboxes = annotations
            class_labels = row_expanded['class_id'].values
            #class_labels= np.zeros(len(bboxes)) # HACK: set all class labels to 0
        else:
            bboxes = np.array([])
            class_labels= np.array([])
  
        # transforms = []


        # if self.transforms:
        #     transforms.extend(self.transforms)
        
        # image, bboxes, class_labels = self._apply_transforms(
        #     transforms, image, bboxes, class_labels
        # )

        image_hw = image.shape[:2]

        return image, bboxes, class_labels, image_id, image_hw

def get_train_transforms():
    return (#A.HorizontalFlip(p=0.5),
    A.RandomSizedBBoxSafeCrop(width=800, height=800, erosion_rate=0.2, p=1),
            A.Flip(always_apply=True), # Either Horizontal, Vertical or both flips
        A.OneOf([  # One of blur or adding gauss noise
        A.Blur(blur_limit=3, p=0.50),  # Adds blur to image
        A.GaussNoise(var_limit=5.0 / 255.0, p=0.50)  # Adds Gauss noise to image
    ], p=0.8),
    # rgb shift
    A.OneOf([
        A.RGBShift(r_shift_limit=20, g_shift_limit=20, b_shift_limit=20, p=0.5),
        A.RandomBrightnessContrast(brightness_limit=0.2, contrast_limit=0.2, p=0.5),
    ], p=0.8),
    # random cutout but must include the object
    
    )


@script
def main(
    # data_path: str = DATA_PATH,
    image_size: int = 800,
    pretrained: bool = True,
    num_epochs: int = 300,
    batch_size: int = 2,
):



    path_train_dataset_slices = '/ssd/temporary/sursamue/skin/3dbodytex-1.1-highres_preprocessed/train/train_dataset_slices.csv' # '/mnt/datagrid/Medical/skin/3dbodytex-1.1-highres_preprocessed/train/train_dataset_slices.csv'
    path_train_dataset_slices_expanded = '/ssd/temporary/sursamue/skin/3dbodytex-1.1-highres_preprocessed/train/train_dataset_slices_expanded.csv'

    path_valid_dataset_slices = '/ssd/temporary/sursamue/skin/3dbodytex-1.1-highres_preprocessed/valid/valid_dataset_slices.csv'
    path_valid_dataset_slices_expanded = '/ssd/temporary/sursamue/skin/3dbodytex-1.1-highres_preprocessed/valid/valid_dataset_slices_expanded.csv'

    path_test_dataset_slices_A1 = '/ssd/temporary/sursamue/skin/3dbodytex-1.1-highres_preprocessed/test/A1/test_dataset_slices.csv'
    path_test_dataset_slices_A1_expanded = '/ssd/temporary/sursamue/skin/3dbodytex-1.1-highres_preprocessed/test/A1/test_dataset_slices_expanded.csv'

    path_test_dataset_slices_A2 = '/ssd/temporary/sursamue/skin/3dbodytex-1.1-highres_preprocessed/test/A2/test_dataset_slices.csv'
    path_test_dataset_slices_A2_expanded = '/ssd/temporary/sursamue/skin/3dbodytex-1.1-highres_preprocessed/test/A2/test_dataset_slices_expanded.csv'

    path_test_dataset_slices_A3 = '/ssd/temporary/sursamue/skin/3dbodytex-1.1-highres_preprocessed/test/A3/test_dataset_slices.csv'
    path_test_dataset_slices_A3_expanded = '/ssd/temporary/sursamue/skin/3dbodytex-1.1-highres_preprocessed/test/A3/test_dataset_slices_expanded.csv'

    train_dataset_slices = pd.read_csv(path_train_dataset_slices, converters={'scan_id': str})
    valid_dataset_slices = pd.read_csv(path_valid_dataset_slices, converters={'scan_id': str})
    test_dataset_slices_A1 = pd.read_csv(path_test_dataset_slices_A1, converters={'scan_id': str})
    test_dataset_slices_A2 = pd.read_csv(path_test_dataset_slices_A2, converters={'scan_id': str})
    test_dataset_slices_A3 = pd.read_csv(path_test_dataset_slices_A3, converters={'scan_id': str})

    train_dataset_slices_expanded = pd.read_csv(path_train_dataset_slices_expanded,  converters={'scan_id': str})
    valid_dataset_slices_expanded = pd.read_csv(path_valid_dataset_slices_expanded,  converters={'scan_id': str})
    test_dataset_slices_A1_expanded = pd.read_csv(path_test_dataset_slices_A1_expanded,  converters={'scan_id': str})
    test_dataset_slices_A2_expanded = pd.read_csv(path_test_dataset_slices_A2_expanded,  converters={'scan_id': str})
    test_dataset_slices_A3_expanded = pd.read_csv(path_test_dataset_slices_A3_expanded,  converters={'scan_id': str})

    #cretae image id column by merging "scan_id" and "slice_id" as string
    train_dataset_slices_expanded['image_id'] = train_dataset_slices_expanded['scan_id'].astype(str) + '0000' + train_dataset_slices_expanded['slice_id'].astype(str)
    valid_dataset_slices_expanded['image_id'] = valid_dataset_slices_expanded['scan_id'].astype(str) + '0000' + valid_dataset_slices_expanded['slice_id'].astype(str)
    test_dataset_slices_A1_expanded['image_id'] = test_dataset_slices_A1_expanded['scan_id'].astype(str) + '0000' + test_dataset_slices_A1_expanded['slice_id'].astype(str)
    test_dataset_slices_A2_expanded['image_id'] = test_dataset_slices_A2_expanded['scan_id'].astype(str) + '0000' + test_dataset_slices_A2_expanded['slice_id'].astype(str)
    test_dataset_slices_A3_expanded['image_id'] = test_dataset_slices_A3_expanded['scan_id'].astype(str) + '0000' + test_dataset_slices_A3_expanded['slice_id'].astype(str)

    train_dataset_slices['image_id'] = train_dataset_slices['scan_id'].astype(str) + '0000' + train_dataset_slices['slice_id'].astype(str)
    valid_dataset_slices['image_id'] = valid_dataset_slices['scan_id'].astype(str) + '0000' + valid_dataset_slices['slice_id'].astype(str)
    test_dataset_slices_A1['image_id'] = test_dataset_slices_A1['scan_id'].astype(str) + '0000' + test_dataset_slices_A1['slice_id'].astype(str)
    test_dataset_slices_A2['image_id'] = test_dataset_slices_A2['scan_id'].astype(str) + '0000' + test_dataset_slices_A2['slice_id'].astype(str)
    test_dataset_slices_A3['image_id'] = test_dataset_slices_A3['scan_id'].astype(str) + '0000' + test_dataset_slices_A3['slice_id'].astype(str)


    train_dataset_slices['image_id'] = train_dataset_slices['image_id'].apply(np.int64)
    valid_dataset_slices['image_id'] = valid_dataset_slices['image_id'].apply(np.int64)
    test_dataset_slices_A1['image_id'] = test_dataset_slices_A1['image_id'].apply(np.int64)
    test_dataset_slices_A2['image_id'] = test_dataset_slices_A2['image_id'].apply(np.int64)
    test_dataset_slices_A3['image_id'] = test_dataset_slices_A3['image_id'].apply(np.int64)

    train_dataset_slices_expanded['image_id'] = train_dataset_slices_expanded['image_id'].apply(np.int64)
    valid_dataset_slices_expanded['image_id'] = valid_dataset_slices_expanded['image_id'].apply(np.int64)
    test_dataset_slices_A1_expanded['image_id'] = test_dataset_slices_A1_expanded['image_id'].apply(np.int64)
    test_dataset_slices_A2_expanded['image_id'] = test_dataset_slices_A2_expanded['image_id'].apply(np.int64)
    test_dataset_slices_A3_expanded['image_id'] = test_dataset_slices_A3_expanded['image_id'].apply(np.int64)



    train_dataset_slices_expanded.loc[train_dataset_slices_expanded['class_id'] == 1., ['class_id']] = 0
    valid_dataset_slices_expanded.loc[valid_dataset_slices_expanded['class_id'] == 1., ['class_id']] = 0
    test_dataset_slices_A1_expanded.loc[test_dataset_slices_A1_expanded['class_id'] == 1., ['class_id']] = 0
    test_dataset_slices_A2_expanded.loc[test_dataset_slices_A2_expanded['class_id'] == 1., ['class_id']] = 0
    test_dataset_slices_A3_expanded.loc[test_dataset_slices_A3_expanded['class_id'] == 1., ['class_id']] = 0


    # train_dataset_slices_expanded[['x', 'y', 'x2', 'y2']] = train_dataset_slices_expanded[['x', 'y', 'x2', 'y2']].apply(np.int64)
    # valid_dataset_slices_expanded[['x', 'y', 'x2', 'y2']] = valid_dataset_slices_expanded[['x', 'y', 'x2', 'y2']].apply(np.int64)
    # test_dataset_slices_expanded[['x', 'y', 'x2', 'y2']] = test_dataset_slices_expanded[['x', 'y', 'x2', 'y2']].apply(np.int64)
    num_classes = 1





    valid_dataset_slices_expanded.drop(columns=['xmin', 'ymin', 'xmax', 'ymax'], inplace=True)
    train_dataset_slices_expanded.drop(columns=['xmin', 'ymin', 'xmax', 'ymax'], inplace=True)
    test_dataset_slices_A1_expanded.drop(columns=['xmin', 'ymin', 'xmax', 'ymax'], inplace=True)
    test_dataset_slices_A2_expanded.drop(columns=['xmin', 'ymin', 'xmax', 'ymax'], inplace=True)
    test_dataset_slices_A3_expanded.drop(columns=['xmin', 'ymin', 'xmax', 'ymax'], inplace=True)

    train_dataset_slices_expanded.rename(columns={"x": "xmin", "y": "ymin", "x2": "xmax", "y2": "ymax"}, inplace=True)
    valid_dataset_slices_expanded.rename(columns={"x": "xmin", "y": "ymin", "x2": "xmax", "y2": "ymax"}, inplace=True)
    test_dataset_slices_A1_expanded.rename(columns={"x": "xmin", "y": "ymin", "x2": "xmax", "y2": "ymax"}, inplace=True)
    test_dataset_slices_A2_expanded.rename(columns={"x": "xmin", "y": "ymin", "x2": "xmax", "y2": "ymax"}, inplace=True)
    test_dataset_slices_A3_expanded.rename(columns={"x": "xmin", "y": "ymin", "x2": "xmax", "y2": "ymax"}, inplace=True)


    
    # print(len(train_dataset_slices_expanded.image_id.unique()))
    # print(len(valid_dataset_slices_expanded.image_id.unique()))
    # print(len(train_dataset_slices.image_id.unique()))
    # print(len(valid_dataset_slices.image_id.unique()))
    # print(len(train_dataset_slices))
    # print(len(valid_dataset_slices))
    

    # input()
    train_dataset_slices['image_id'] = range(0, len(train_dataset_slices))
    valid_dataset_slices['image_id'] = range(0, len(valid_dataset_slices))
    test_dataset_slices_A1['image_id'] = range(0, len(test_dataset_slices_A1))
    test_dataset_slices_A2['image_id'] = range(0, len(test_dataset_slices_A2))
    test_dataset_slices_A3['image_id'] = range(0, len(test_dataset_slices_A3))
    
    # create image_id for train_dataset_slices_expanded and valid_dataset_slices_expanded. it wiil be unique integer

    train_dataset_slices_expanded['image_id_new'] = pd.factorize(train_dataset_slices_expanded['image_id'])[0]
    valid_dataset_slices_expanded['image_id_new'] = pd.factorize(valid_dataset_slices_expanded['image_id'])[0]
    test_dataset_slices_A1_expanded['image_id_new'] = pd.factorize(test_dataset_slices_A1_expanded['image_id'])[0]
    test_dataset_slices_A2_expanded['image_id_new'] = pd.factorize(test_dataset_slices_A2_expanded['image_id'])[0]
    test_dataset_slices_A3_expanded['image_id_new'] = pd.factorize(test_dataset_slices_A3_expanded['image_id'])[0]

    train_dataset_slices_expanded.drop(columns=['image_id'], inplace=True)
    valid_dataset_slices_expanded.drop(columns=['image_id'], inplace=True)
    test_dataset_slices_A1_expanded.drop(columns=['image_id'], inplace=True)
    test_dataset_slices_A2_expanded.drop(columns=['image_id'], inplace=True)
    test_dataset_slices_A3_expanded.drop(columns=['image_id'], inplace=True)

    train_dataset_slices_expanded.rename(columns={"image_id_new": "image_id"}, inplace=True)
    valid_dataset_slices_expanded.rename(columns={"image_id_new": "image_id"}, inplace=True)
    test_dataset_slices_A1_expanded.rename(columns={"image_id_new": "image_id"}, inplace=True)
    test_dataset_slices_A2_expanded.rename(columns={"image_id_new": "image_id"}, inplace=True)
    test_dataset_slices_A3_expanded.rename(columns={"image_id_new": "image_id"}, inplace=True)

    valid_dataset_slices.to_csv('./stats/valid_dataset_slices_gt.csv', index=False)
    valid_dataset_slices_expanded.to_csv('./stats/valid_dataset_slices_expanded_gt.csv', index=False)
    test_dataset_slices_A1.to_csv('./stats/test_dataset_slices_A1_gt.csv', index=False)
    test_dataset_slices_A1_expanded.to_csv('./stats/test_dataset_slices_A1_expanded_gt.csv', index=False)
    test_dataset_slices_A2.to_csv('./stats/test_dataset_slices_A2_gt.csv', index=False)
    test_dataset_slices_A2_expanded.to_csv('./stats/test_dataset_slices_A2_expanded_gt.csv', index=False)
    test_dataset_slices_A3.to_csv('./stats/test_dataset_slices_A3_gt.csv', index=False)
    test_dataset_slices_A3_expanded.to_csv('./stats/test_dataset_slices_A3_expanded_gt.csv', index=False)



    # train_dataset_slices = train_dataset_slices.loc[train_dataset_slices['image_id']<10] 
    # valid_dataset_slices = valid_dataset_slices.loc[valid_dataset_slices['image_id'] <10]#50000000
    # train_dataset_slices_expanded = train_dataset_slices_expanded.loc[train_dataset_slices_expanded['image_id'] < 10]
    # valid_dataset_slices_expanded = valid_dataset_slices_expanded.loc[valid_dataset_slices_expanded['image_id'] <10]#39300048



    train_dataset_prep = BodyTexPreprocessed(dataframe = train_dataset_slices, dataframe_expanded = train_dataset_slices_expanded, partition='train')
    valid_dataset_prep = BodyTexPreprocessed(dataframe = valid_dataset_slices, dataframe_expanded = valid_dataset_slices_expanded, partition='valid')
    test_dataset_prep_A1 = BodyTexPreprocessed(dataframe = test_dataset_slices_A1, dataframe_expanded = test_dataset_slices_A1_expanded, partition='test', annotator = 'A1')
    test_dataset_prep_A2 = BodyTexPreprocessed(dataframe = test_dataset_slices_A2, dataframe_expanded = test_dataset_slices_A2_expanded, partition='test', annotator = 'A2')
    test_dataset_prep_A3 = BodyTexPreprocessed(dataframe = test_dataset_slices_A3, dataframe_expanded = test_dataset_slices_A3_expanded, partition='test', annotator = 'A3')






    
    train_ds = Yolov7Dataset(
        train_dataset_prep,
        create_yolov7_transforms(training=True, image_size=(image_size, image_size),
        training_transforms=           
        ( #A.HorizontalFlip(p=0.5),
        A.Flip(p=0.5),
            A.RandomSizedBBoxSafeCrop(width=800, height=800, erosion_rate=0.2, p=1),
            A.OneOf([  # One of blur or adding gauss noise
            A.Blur(blur_limit=3, p=0.50),  # Adds blur to image
            A.GaussNoise(var_limit=5.0 / 255.0, p=0.50)  # Adds Gauss noise to image
            ], p=0.8),
            # rgb shift
            A.OneOf([
                A.RGBShift(r_shift_limit=20, g_shift_limit=20, b_shift_limit=20, p=0.5),
                A.RandomBrightnessContrast(brightness_limit=0.2, contrast_limit=0.2, p=0.5),
            ], p=0.8),
        ),
        ),

        
    )
    valid_ds = Yolov7Dataset(
        valid_dataset_prep,
        create_yolov7_transforms(training=False, image_size=(image_size, image_size)),
    )
    test_ds_A1 = Yolov7Dataset(
        test_dataset_prep_A1,
        create_yolov7_transforms(training=False, image_size=(image_size, image_size)),
    )
    test_ds_A2 = Yolov7Dataset(
        test_dataset_prep_A2,
        create_yolov7_transforms(training=False, image_size=(image_size, image_size)),
    )
    test_ds_A3 = Yolov7Dataset(
        test_dataset_prep_A3,
        create_yolov7_transforms(training=False, image_size=(image_size, image_size)),
    )



    # Create model, loss function and optimizer
    model = create_yolov7_model(
        architecture="yolov7", num_classes=num_classes, pretrained=pretrained
    )


    loss_func = create_yolov7_loss(model, image_size=image_size)

    # optimizer = torch.optim.SGD(
    #     model.parameters(), lr=0.001, momentum=0.9, nesterov=True
    # )
    optimizer = torch.optim.Adam(model.parameters(), lr=0.0001, weight_decay=0.0005)
    # #########################testing_anchor boxes####################

    # model = create_yolov7_model('yolov7', pretrained=False)
    # # print(model.detection_head.anchor_grid)
    # current_anchors = model.detection_head.anchor_grid.clone().cpu().view(-1, 2); current_anchors

    # train_annotations_df = train_dataset_slices_expanded.query('has_annotation == True').copy()
    # train_annotations_df['h'] = train_annotations_df['ymax'] -  train_annotations_df['ymin']
    # train_annotations_df['w'] = train_annotations_df['xmax'] -  train_annotations_df['xmin']

    # raw_gt_wh = train_annotations_df[['w', 'h']].values
    # # add image_width and image_height columns to the train_annotations_df. Assiggn 800x800
    # train_annotations_df['image_w'] = [800 for i in range(len(train_annotations_df))]
    # train_annotations_df['image_h'] = [800 for i in range(len(train_annotations_df))]

    # print(train_annotations_df.head(5))

    # image_sizes = train_annotations_df[['image_w', 'image_h']].values
    # from yolov7.anchors import calculate_resized_gt_wh
    # print(raw_gt_wh.shape)
    # gt_wh = calculate_resized_gt_wh(raw_gt_wh, image_sizes, target_image_size=800); gt_wh[:5]

    # from yolov7.anchors import calculate_best_possible_recall, LOSS_ANCHOR_MULTIPLE_THRESHOLD
    # print(calculate_best_possible_recall(current_anchors, gt_wh))


    
    # Create trainer and train
    trainer = Yolov7Trainer( 
        model=model,
        optimizer=optimizer,
        loss_func=loss_func,
        # filter_eval_predictions_fn=partial(
        #     filter_eval_predictions, confidence_threshold=0.01, nms_threshold=0.01
        # ),
        callbacks=[
            CalculateMeanAveragePrecisionCallback.create_from_targets_df(
                targets_df=test_dataset_slices_A1_expanded.query("has_annotation == True")[
                    ["image_id", "xmin", "ymin", "xmax", "ymax", "class_id"]
                ],# just ids with bboxes                            
                image_ids=set(test_dataset_slices_A1.image_id.unique()), # all img ids
                iou_threshold=0.5,
                save_predictions_output_dir_path="./stats",
                verbose = False
            ),
            SaveBestModelCallback(watch_metric="map", greater_is_better=True),
            EarlyStoppingCallback(
                early_stopping_patience=100,
                watch_metric="map",
                greater_is_better=True,
                early_stopping_threshold=0.0001,
            ),
            *get_default_callbacks(progress_bar=True),
        ],
    )





    # trainer.train(
    #     num_epochs=num_epochs,
    #     train_dataset=train_ds,
    #     eval_dataset=valid_ds,
    #     per_device_batch_size=batch_size,
    #     create_scheduler_fn=CosineLrScheduler.create_scheduler_fn(
    #         num_warmup_epochs=5,
    #         num_cooldown_epochs=5,
    #         k_decay=2,
    #     ),
    #     collate_fn=yolov7_collate_fn,
    # )

    # checkpoint_path = "best_model.pt"
    checkpoint_path = "/home.stud/sursamue/temporary/03_09/best_model.pt"
    trainer.load_checkpoint(checkpoint_path, load_optimizer=False)
    # trainer.evaluate(dataset=valid_ds, per_device_batch_size = 16, collate_fn=yolov7_collate_fn)
    trainer.evaluate(dataset=test_ds_A1, per_device_batch_size = 4, collate_fn=yolov7_collate_fn)
    # trainer.evaluate(dataset=test_ds_A2, per_device_batch_size = 4, collate_fn=yolov7_collate_fn)
    # trainer.evaluate(dataset=test_ds_A3, per_device_batch_size = 16, collate_fn=yolov7_collate_fn)


    # # load model
    # model = create_yolov7_model('yolov7', num_classes=1)
    # checkpoint_path = "best_model.pt" #/home.stud/sursamue/temporary/03_09/
    # model.load_state_dict(torch.load(checkpoint_path, map_location="cpu")["model_state_dict"])
    # model.eval()
    # image = cv2.imread("../IMG_20230310_183856_243.jpg")

    # print(image.shape)

    # image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
    # original_image_size = image.shape[:2]
    # image = cv2.resize(image, (800, 800))
    # print(image.shape)

    # image = image.astype(np.float32) / 255.0
    # image = image.transpose(2, 0, 1)
    # image_tensor = torch.from_numpy(image).float()
    
    
    
    # from yolov7.models.yolo import scale_bboxes_to_original_image_size
    




    # with torch.no_grad():
    #     model_outputs = model(image_tensor[None])
    #     preds = model.postprocess(model_outputs, conf_thres=0., multiple_labels_per_box=False)
    #     nms_predictions = filter_eval_predictions(preds, confidence_threshold=0.05, nms_threshold=0.01)






    # boxes = nms_predictions[0][:, :4]
    # scores = nms_predictions[0][:, 4]
    # # scores to numpy
    # scores = scores.numpy()
    # # convert to list
    # scores = scores.tolist()
    # print(boxes)
    # class_ids = nms_predictions[0][:, -1]


    # # scale preds to original image size
    # boxes = scale_bboxes_to_original_image_size(
    #     boxes,
    #     (800, 800),
    #     (original_image_size[1], original_image_size[0]),
    #     is_padded=False,
    # )

    # print(boxes)

    # # loage image fromarray
    # #reade image
    # image = cv2.imread("../IMG_20230310_183856_243.jpg")
    # image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)



    # print(image.shape)
    # # convert to rgb

    # # convert tensor boxes to list of boxes
    # boxes = boxes.tolist()
    # # convert to int
    # boxes = [list(map(int, box)) for box in boxes]

    # # enumerateboxes and scores
    # for box, score in zip(boxes, scores):

    #     color = [0,0,255]
    #     pad = 2
    #     x = int(box[0])
    #     y = int(box[1])
    #     width = int(box[2]-box[0])
    #     height = int(box[3]-box[1])
    #     embed_box_borders(image, x, y, width, height, color, pad)
    #     # draw score but bigger and convert it to percentage and write only 0  decimals and convert to int
    #     cv2.putText(image, str(int(score*100))+"%", (x, y-5), cv2.FONT_HERSHEY_SIMPLEX, 1, color, 2, cv2.LINE_AA)
    # image = Image.fromarray(image).convert('RGB')
    # image.save("test1.jpg")



if __name__ == "__main__":

    main()



